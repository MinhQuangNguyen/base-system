const Route = require('@core/Route');
const ExtendMiddleware = require("@app/Middlewares/ExtendMiddleware");
const AuthApiMiddleware = require('@app/Middlewares/AuthApiMiddleware')
const {permission, permissionResource, permissionMethod} = require('@app/Middlewares/PermissionMiddleware')
const nhfinderConfig = require('@config/nhfinder.js')
const nhfinderRoutes = require('@ngochipx/nhfinder/nodejs/routes')(nhfinderConfig)

Route.group(() => {
  Route.post("/login", "UserController.login").name('login')

  Route.use('/nhfinder', nhfinderRoutes)

  Route.group(() => {
    // Route.put("/users/updatePassword/:id", "UserController.updatePassword").name('users.updatePassword').middleware([
    //   permission({ 'admin.users': 'U'}),
    // ])
    // Route.get("/categories/getTreeData","CategoryController.getTreeData").name('categorys.getTreeData')
    // Route.get("/serviceSchedules/getByProductId", "ServiceScheduleController.getByProductId").name('serviceSchedules.getByProductId')
    
    // Route.get("/permissions/getByGroupId/:groupId", "UserPermissonController.getByGroupId").name('userpermission.getByGroupId')
    // Route.put("/permissions/update", "UserGroupPermissionController.update").name('user_group_permission.update')

    // Route.resource("/users", "UserController").name('users')
    // Route.resource("/serviceSchedules", "ServiceScheduleController").name('serviceSchedules')
    // Route.resource("/userGroups", "UserGroupController").name('userGroups')
    // Route.resource("/categories", "CategoryController").name('categories')

    // Route.resource("/products", "ProductController").name('products')
    // Route.get("/products/search", "ProductController.search").name('products.search')
    // Route.post("/products/schedule", "ProductController.schedule").name('products.schedule')
    // Route.put("/products/updateQuantity", "ProductController.updateQuantity").name('products.updateQuantity')
    // Route.put("/products/import", "ProductController.import").name('products.import')
    // Route.put("/products/export", "ProductController.export").name('products.export')
    // Route.get("/products/getProducts", "ProductController.getProducts").name('products.getProducts')
    // Route.get("/products/sProducts", "ProductController.sProducts").name('products.sProducts')

    // Route.resource("/customers", "CustomerController").name('customers')
    // Route.get("/customers/search", "CustomerController.search").name('customers.search')
    // Route.get("/customers/:id/histories", "CustomerController.histories").name('customers.histories')
    // Route.get("/users/search", "UserController.search").name('users.search')

    // Route.get("/customers/booking", "CustomerController.getBooking").name('customers.getBooking')
    // Route.post("/customers/booking", "CustomerController.addBooking").name('customers.addBooking')
    // Route.put("/customers/booking/:id", "CustomerController.updateBooking").name('customers.updateBooking')

    // Route.resource("/orders", "OrderController").name('orders')
    // Route.get("/orders/new", "OrderController.listOrder").name('orders.listOrder')
    // Route.post("/orders/new", "OrderController.newOrder").name('orders.newOrder')
    // Route.put("/orders/new/:id", "OrderController.editOrder").name('orders.editOrder')
    // Route.delete("/orders/new/:id", "OrderController.cancelOrder").name('orders.cancelOrder')
    // Route.get("/orders/new/:id", "OrderController.viewOrder").name('orders.viewOrder')

    // Route.get("/revenues", "RevenueController.index").name('revenues.index')
    // Route.get("/revenues/date/:date", "RevenueController.getByDate").name('revenues.getByDate')
    // Route.get("/revenues/month/:month", "RevenueController.getByMonth").name('revenues.getByMonth')
    // Route.get("/revenues/year/:year", "RevenueController.getByYear").name('revenues.getByYear')
    // Route.post("/revenues/:month", "RevenueController.closeMonth").name('revenues.closeMonth')

    // // Route.get("/salaries", "SalaryController.index").name('salaries.index')
    // // Route.post("/salaries", "SalaryController.store").name('salaries.store')
    // Route.get("/salaries/:month", "SalaryController.detail").name('salaries.getByMonth')
    // Route.put("/salaries/:month", "SalaryController.update").name('salaries.updateByMonth')
    // Route.put("/salaries/detail/:id", "SalaryController.updateDetail").name('salaries.updateDetail')

    // Route.get("/reports/revenue/:type/:value", "ReportController.revenue").name('report.revenue')
    // Route.get("/reports/order/:type/:value", "ReportController.order").name('report.order')
    // Route.get("/reports/customer/:type/:value", "ReportController.customer").name('report.customer')
    // Route.get("/reports/getYear", "ReportController.getYear").name('report.getYear')

    // Route.post("/reports/sms", "ReportController.sms").name('report.sms')

    // Route.resource("/salaries", "SalaryController").name('salaries')

    // Route.get("/salaries/init/:month", "SalaryController.init").name('salaries.init')

    // Route.get("/schedules", "CustomerScheduleController.index").name('schedules.index')
    // Route.get("/schedules/:date", "CustomerScheduleController.getByDate").name('schedules.getByDate')
    // Route.get("/schedules/:from/:to", "CustomerScheduleController.getByDateRange").name('schedules.getByDateRange')
    // Route.put("/schedules/:id", "CustomerScheduleController.update").name('schedules.update')

    // Route.post("/sms/user", "SmsController.sendUser").name('sms.sendUser')
    // Route.post("/sms/group", "SmsController.sendGroup").name('sms.sendGroup')

    // Route.resource("/users", "UserController").name('users').middleware([
    //   permissionResource(['admin.users']),
    // ])
    // Route.resource("/customers", "CustomerController").name('customers').middleware([
    //   permissionResource(['admin.customers']),
    // ])
    // Route.resource("/serviceSchedules", "ServiceScheduleController").name('serviceSchedules').middleware([
    //   permissionResource(['admin.serviceSchedules']),
    // ])
    // Route.resource("/userGroups", "UserGroupController").name('userGroups').middleware([
    //   permissionResource(['admin.userGroups']),
    // ])
    // Route.resource("/categories", "CategoryController").name('categories').middleware([
    //   permissionResource(['admin.categories']),
    // ])
    // Route.get("/categories/getByProductType", "CategoryController.getByProductType").name('categories.getByProductType')

    // Route.resource("/products", "ProductController").name('products').middleware([
    //   permissionResource(['admin.products']),
    // ])
    // Route.resource("/customers", "CustomerController").name('customers').middleware([
    //   permissionResource(['admin.customers']),
    // ])
    // Route.resource("/permission_categories", "PermissionCategoryController").name('permission_categories')

  }).middleware([AuthApiMiddleware])
}).middleware([ExtendMiddleware]).name('api')