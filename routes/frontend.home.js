const Route = require('@core/Route');
const express = require('express')
const path = require('path')
Route.router.use('/lang', express.static(path.join(__ROOTDIR__, 'lang')))
Route.group(() => {
    const name = 'frontend.landing'
    Route.get(`/`, `pages/landing`).name(`${name}.index`)
    Route.get(`/Login`, `pages/landing/Login`).name(`${name}.Login`)
    Route.get(`/Signup`, `pages/landing/Signup`).name(`${name}.Signup`)
    Route.get(`/chart`, `pages/landing/chart`).name(`${name}.chart`)
    Route.get(`/ListForm1`, `pages/landing/ListForm1`).name(`${name}.ListForm1`)
    Route.get(`/ListForm2`, `pages/landing/ListForm2`).name(`${name}.ListForm2`)
    Route.get(`/ListTable1`, `pages/landing/ListTable1`).name(`${name}.ListTable1`)
    Route.get(`/ListTable2`, `pages/landing/ListTable2`).name(`${name}.ListTable2`)
    Route.get(`/ListTable3`, `pages/landing/ListTable3`).name(`${name}.ListTable3`)
    Route.get(`/ListTable4`, `pages/landing/ListTable4`).name(`${name}.ListTable4`)
    Route.get(`/ListTable5`, `pages/landing/ListTable5`).name(`${name}.ListTable5`)
    Route.get(`/ListTable6`, `pages/landing/ListTable6`).name(`${name}.ListTable6`)
    Route.get(`/ListComment`, `pages/landing/ListComment`).name(`${name}.ListComment`)
    Route.get(`/ListModal`, `pages/landing/ListModal`).name(`${name}.ListModal`)
    Route.get(`/CalendareMqn`, `pages/landing/CalendareMqn`).name(`${name}.CalendareMqn`)
    Route.get(`/NotificationMqn`, `pages/landing/NotificationMqn`).name(`${name}.NotificationMqn`)
    Route.get(`/Profile`, `pages/landing/Profile`).name(`${name}.Profile`)
    Route.get(`/EditProfile`, `pages/landing/EditProfile`).name(`${name}.EditProfile`)
    Route.get(`/ComponentLibrary`, `pages/landing/ComponentLibrary`).name(`${name}.ComponentLibrary`)
    Route.get(`/PluginCamp`, `pages/landing/PluginCamp`).name(`${name}.PluginCamp`)
    Route.get(`/CreateCamp`, `pages/landing/CreateCamp`).name(`${name}.CreateCamp`)
    // Route.get(`/bai-viet/:slug/:id`, `pages/home/postDetail`).name(`frontend.home.post`)
    // Route.get(`/chuyen-muc-san-pham/:slug/:id`, `pages/home/productCategory`).name(`frontend.home.product_category`)
    // Route.get(`/san-pham/:slug/:id`, `pages/home/productDetail`).name(`frontend.home.product`)
    // Route.get(`/tim-kiem-chi-nhanh`, `pages/home/salonFinder`).name(`frontend.home.salon_finder`)
    // Route.get(`/lien-he`, `pages/home/contact`).name(`frontend.home.contact`)
    // Route.get(`/trang/:slug/:id`, `pages/home/page`).name(`frontend.home.page`)
    // Route.get("/not-found", `pages/home/notFound`).name(`frontend.home.notFound`)
})

