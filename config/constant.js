
const productUpdateTypes = {
    'update': 0,
    'import': 1,
    'export': 2
}

const productTypes = {
    'service': 0,
    'product': 1
}

const productStatus = {
    'hide': 0,
    'show': 1
}

const orderStatus = {
    'open': 0,
    'close': 1
}

const discountTypes = {
    'amount': 1,
    'percent': 0
}

const salaryStatus = {
    'open': 0,
    'close': 1
}

const scheduleStatus = {
    'open': 0,
    'close': 1
}

const bookingStatus = {
    'open': 0,
    'close': 1
}

const chartTypes = {
    'day': 'day',
    'month': 'month',
    'year': 'year'
}

module.exports = {
    productUpdateTypes,
    productTypes,
    productStatus,
    orderStatus,
    discountTypes,
    salaryStatus,
    scheduleStatus,
    bookingStatus,
    chartTypes
}