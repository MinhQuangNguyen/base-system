const publicRuntimeConfig = require('./publicRuntime')

module.exports = {
  ROOTDIR: __ROOTDIR__,
  UPLOAD_PATH: __ROOTDIR__ + '/static/uploads/',
  PUBLIC_PATH: '/uploads',
  DOMAIN: publicRuntimeConfig.API_HOST,
  ALLOW_EXTENSIONS: ['.jpg','.jpeg', '.png', '.bmp', '.gif','.txt'],
  API_KEY: "123456"
};
