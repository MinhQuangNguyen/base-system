import NHFinder from '@ngochipx/nhfinder/reactjs'
import React from 'react'

class CKEditorComponent extends React.Component {
  constructor(props) {
    super(props);
    this.initEditor();
    this.refNHFinder;
  }

  getContent = () => {
    return this.editor.getData();
  }

  setRefNHFinder = (ref) => {
    this.refNHFinder = ref;
    this.setState({reload: 1})
  }

  initEditor = async () => {
    if (process.browser) {
      this.CKEditor = require("@ckeditor/ckeditor5-react");
      this.InlineEditor = require("@ngochipx/nhfinder/build-inline");
    }
  }

  render() {
    const { imageManagementOptions, defaultContent, config, ...otherProps } = this.props
    if (!this.CKEditor) return "";
    return (
      <React.Fragment>
        {this.refNHFinder && <this.CKEditor editor={this.InlineEditor}
          data={defaultContent || "<b>Click here to insert content ...</b>"}
          onInit={editor => {
            this.editor = editor
          }}
          config={{
            imageManagement: {
              component: this.refNHFinder,
              options: {
                apiUrl: "http://localhost:3333/api/v1/nhfinder",
                apiKey: "123456"
              }
            },
            ...config
          }}
          {...otherProps}
        />}
        <NHFinder  ref={this.setRefNHFinder}  />
      </React.Fragment>
    );
  }
}

export default CKEditorComponent
