import MultiImage from './MultiImage'
import SingleImage from './SingleImage'

export {
    MultiImage,
    SingleImage
}