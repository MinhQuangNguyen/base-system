import { BaseComponent } from 'themes/components'
import PropTypes from 'prop-types';
import { DeleteOutlined } from '@ant-design/icons';
import { Input, Button, Row, Col } from 'antd'
const { Search } = Input;
import NHFinder from '@ngochipx/nhfinder/reactjs'
const getConfig = require('next/config').default
const { publicRuntimeConfig } = getConfig();

class MultiImage extends BaseComponent{
  constructor(props) {
    super(props);
    this.state = { reload: false };
    this.list = this.props.defaultValue;
    this.selectedIndex = -1;
  }

  showNHFinder = (index) => {
    this.NHFinderRef.show();
    this.selectedIndex = index;
  }

  reload() {
    this.setState({ reload: !this.state.reload })
  }

  onChange = () => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.list);
    }
  }

  onChooseImages = (imgs = []) => {
    imgs = imgs.map((img = {}) => ({
      url: img.src,
      name: img.caption || img.src,
    }))
    this.list = this.list.concat(imgs)
    this.reload();
    this.onChange();
  }

  onRemove = (img) => {
    if (img) {
      this.list.splice(img.uid, 1);
      this.reload();
      this.onChange();
    }
  }

  getOptionsNHFinder() {
    return {
      apiKey: publicRuntimeConfig.API_KEY, //API KEY
      directorySelected: "/", //Default selected folder
      apiUrl: publicRuntimeConfig.API_HOST + "/api/v1/nhfinder",
      onChooseImages: this.onChooseImages
    }
  }

  addRow = () => {
    this.list.push({})
    this.reload();
  }

  onChangeInput = (e, index) => {
    if (!e) return false;
    const { value } = e.target;
    this.list[index].url = value;
    this.reload();
    this.onChange();
  }

  onChooseImages = (imgs = []) => {
    let img = imgs[0] || {};
    if (this.selectedIndex === -1) return false;
    this.list[this.selectedIndex].url = (img.src || "").replace(/\\/g, "/");
    this.selectedIndex = -1;
    this.reload();
    this.onChange();
  }

  deleteItem = (index) => {
    this.list.splice(index, 1);
    this.reload();
    this.onChange();
  }

  render() {
    const { t } = this.props;
    let { ItemProps = {}, label } = this.props;
    let { labelCol = {}, wrapperCol = {} } = ItemProps;
    return (
      <Row gutter={[8, 8]}>
        <Col span={labelCol.span || 12} style={{ textAlign: "right" }}>
          {label}:
                </Col>
        <Col span={wrapperCol.span || 12}>
          <Button onClick={this.addRow}>Thêm ảnh</Button>
          {
            this.list.map((img = {}, index) => {
              return (
                <div key={index}><br />
                  <Row>
                    <Col span={20}>
                      <Search
                        value={img.url}
                        onChange={(e) => this.onChangeInput(e, index)}
                        placeholder={t("placeholder.linkImg")}
                        enterButton={t("chooseImg")}
                        onSearch={() => this.showNHFinder(index)}
                      />
                    </Col>
                    <Col span={4}>
                      <Button
                        onClick = {() => this.deleteItem(index)}
                        style={{ display: "block", margin: "auto" }}
                        type="primary"
                        shape="circle"
                        icon={<DeleteOutlined />} />
                    </Col>
                  </Row>
                </div>
              );
            })
          }
        </Col>



        <NHFinder
          ref={ref => this.NHFinderRef = ref}
          options={this.getOptionsNHFinder()}
        />
      </Row>
    );
  }
}

MultiImage.propTypes = {
  defaultValue: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string,
    name: PropTypes.string,
  })),
  label: PropTypes.string
}

MultiImage.defaultProps = {
  defaultValue: [],
  label: 'image'
}
export default BaseComponent.export({

})(MultiImage);