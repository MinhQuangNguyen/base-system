import { BaseComponent } from 'themes/components'
import PropTypes from 'prop-types';
import { Row, Col } from 'antd'
import NHFinder from '@ngochipx/nhfinder/reactjs'
import { Input } from 'antd';
const { Search } = Input;
const getConfig = require('next/config').default
const { publicRuntimeConfig } = getConfig();
class SingleImage extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = { reload: false };
    this.value = this.props.defaultValue;
  }

  showNHFinder = () => {
    this.NHFinderRef.show();
  }

  reload() {
    this.setState({ reload: !this.state.reload })
  }

  onChange = () => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.value);
    }
  }

  onChooseImages = (imgs = []) => {
    let img = imgs[0] || {};
    this.value = (img.src || "").replace(/\\/g, "/");
    this.reload();
    this.onChange();
  }

  onRemove = (img) => {
    if (img) {
      this.value = '';
      this.reload();
      this.onChange();
    }
  }

  getOptionsNHFinder() {
    return {
      apiKey: publicRuntimeConfig.API_KEY, //API KEY
      directorySelected: "/", //Default selected folder
      apiUrl: publicRuntimeConfig.API_HOST + "/api/v1/nhfinder",
      onChooseImages: this.onChooseImages
    }
  }

  onChangeInput = (e) => {
    if(!e) return false;
    const {value} = e.target;
    this.value = value;
    this.reload();
    this.onChange();
  }

  render() {
    const { t } = this.props;
    let { ItemProps = {}, label } = this.props;
    let { labelCol = {}, wrapperCol = {} } = ItemProps;

    return (
      <Row gutter={[8, 8]} align="middle">
        <Col span={labelCol.span || 12} style={{ textAlign: "right" }}>
          {label}:
        </Col>
        <Col span={wrapperCol.span || 12}>
          <Search
            value = {this.value}
            onChange = {this.onChangeInput}
            placeholder={t("placeholder.linkImg")}
            enterButton={t("chooseImg")}
            onSearch={() => this.showNHFinder()}
          />

        </Col>
        <NHFinder
          ref={ref => this.NHFinderRef = ref}
          options={this.getOptionsNHFinder()}
        />
      </Row>


    )
  }
}

SingleImage.propTypes = {
  defaultValue: PropTypes.string,
  label: PropTypes.string
}

SingleImage.defaultProps = {
  defaultValue: '',
  label: 'image'
}
export default BaseComponent.export({

})(SingleImage);