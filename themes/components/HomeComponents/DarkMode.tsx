
import { ChangeEventHandler } from "react";
import { Tooltip } from 'antd';
import useBaseHooks from "themes/hooks/BaseHooks";





const setDark = () => {
  localStorage.setItem("theme", "dark");
  document.documentElement.setAttribute("data-theme", "dark");

};

const setLight = () => {
  localStorage.setItem("theme", "light");
  document.documentElement.setAttribute("data-theme", "light");
};

// Lưu localStorage 

// const storedTheme = localStorage.getItem("theme");

// const prefersDark =
//   window.matchMedia &&
//   window.matchMedia("(prefers-color-scheme: dark)").matches;

// const defaultDark =
//   storedTheme === "dark" || (storedTheme === null && prefersDark);

// if (defaultDark) {
//   setDark();
// }

// End lưu localStorage

const toggleTheme: ChangeEventHandler<HTMLInputElement> = (e) => {
  if (e.target.checked) {
    setDark();
  } else {
    setLight();
  }
};

const DarkMode = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const textDarkMode = <span>{t("Input.Label.SwitchMode")}</span>;
  return <>
    <div className="toggle-theme-wrapper">
      {/* <label className="toggle-theme" htmlFor="checkbox">
                <input
                type="checkbox"
                id="checkbox"
                onChange={toggleTheme}
                // defaultChecked={defaultDark}
                />
                <div className="slider round"></div>
            </label> */}

      <input
        type="checkbox"
        className="checkbox"
        id="checkbox"
        onChange={toggleTheme}
      // defaultChecked={defaultDark}
      />
      <Tooltip placement="top" title={textDarkMode}>
        <label htmlFor="checkbox" className="checkbox-label">

          <i className="fas fa-sun"></i>
          <i className="fas fa-moon"></i>
          <span className="ball"></span>
        </label>
      </Tooltip>
    </div>
  </>
}

export default DarkMode