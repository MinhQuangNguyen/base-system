import React, { Component } from "react";
import { Layout, Menu, Badge, Affix } from "antd";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@ant-design/icons";
import Link from "next/link";
import { route as makeUrl } from "themes/route";
import ItemMenu from "./ItemMenu";

const { Sider } = Layout;

class MenuMain extends Component {
  state = {
    collapsed: true,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <>
        <div className="item-menumain-slide-left">
          <Affix offsetTop={0}>
            <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
              <div className="item-logo-ufo">
                <Link {...makeUrl("frontend.landing.index")}>
                  <div className="logo"></div>
                </Link>
                <div className="UfoMqn">
                  {React.createElement(
                    this.state.collapsed
                      ? ArrowRightOutlined
                      : ArrowLeftOutlined,
                    {
                      className: "trigger",
                      onClick: this.toggle,
                    }
                  )}
                </div>
              </div>
              <ItemMenu />
            </Sider>
          </Affix>
        </div>
      </>
    );
  }
}

export default MenuMain;
