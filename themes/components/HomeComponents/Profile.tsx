import { Menu, Dropdown } from "antd";
import { UserOutlined, LogoutOutlined } from "@ant-design/icons";
import useBaseHooks from "themes/hooks/BaseHooks";

const Profile = (props: any) => {
  const { t, redirect } = useBaseHooks({ lang: ["common"] });

  const menu = (
    <Menu>
      <Menu.Item>
        <a href="/Profile">
          <UserOutlined /> {t("Button.PersonalInformation")}
        </a>
      </Menu.Item>
      <Menu.Item>
        <a href="/Login">
          <LogoutOutlined /> {t("Button.LogOut")}
        </a>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Dropdown overlay={menu} placement="bottomRight" trigger={["click"]}>
        <span className="avatar-item">
          <img src="/static/img/avata-mqn.png" alt="icon-avata" />
        </span>
      </Dropdown>
    </>
  );
};

export default Profile;
