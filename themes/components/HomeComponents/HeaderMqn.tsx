import React, { useState } from "react";
import {
  Row,
  Col,
  Badge,
  Affix,
  Drawer,
  List,
  Avatar,
  Tooltip,
  Tag,
  Select,
} from "antd";
import useBaseHooks from "themes/hooks/BaseHooks";
import { i18n } from "themes/modules/I18n";
import moment from "moment";
import SearchMqn from "./SearchMqn";
import MenuMobile from "./MenuMobile";
import DarkMode from "./DarkMode";
import Profile from "./Profile";
import {LaptopOutlined } from "@ant-design/icons";
const { Option } = Select;

const MenuMain = (props: any) => {
  const { t, redirect } = useBaseHooks({ lang: ["common"] });

  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const data = [
    {
      key: "1",
      title: "Admin",
      description: (
        <>
          <p>Hôm nay bạn có hẹn đi ký hơp đồng 2 tỉ</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata1.png" />
        </>
      ),
    },
    {
      key: "2",
      title: "Admin",
      description: (
        <>
          <p>Hôm nay bạn có hẹn đi ăn trưa với em yêu!</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata1.png" />
        </>
      ),
    },
    {
      key: "3",
      title: "Hoàng Đức Anh",
      description: (
        <>
          <p>Bình luận chiến dịch Vincom Smart City</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata2.png" />
        </>
      ),
    },
    {
      key: "4",
      title: "Admin",
      description: (
        <>
          <p>Hệ thống sẽ cập nhập version 2.1.2.6</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata1.png" />
        </>
      ),
    },
    {
      key: "5",
      title: "Admin",
      description: (
        <>
          <p>Hôm nay bạn có hẹn đi ký hơp đồng 2 tỉ</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata1.png" />
        </>
      ),
    },
    {
      key: "6",
      title: "Admin",
      description: (
        <>
          <p>Hôm nay bạn có hẹn đi ăn trưa với em yêu!</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata1.png" />
        </>
      ),
    },
    {
      key: "7",
      title: "Hoàng Đức Anh",
      description: (
        <>
          <p>Bình luận chiến dịch Vincom Smart City</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata2.png" />
        </>
      ),
    },
    {
      key: "8",
      title: "Admin",
      description: (
        <>
          <p>Hệ thống sẽ cập nhập version 2.1.2.6</p>
          <span>
            <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
              <span>{moment().format("DD-MM-YYYY")}</span>
            </Tooltip>
          </span>
        </>
      ),
      avatar: (
        <>
          <Badge status="processing" /> <Avatar src="/static/img/avata1.png" />
        </>
      ),
    },
  ];

  return (
    <>
      <Affix offsetTop={0}>
        <div className="HeaderMqn">
          <Row justify="space-between">
            <Col xs={1} xl={8} xxl={15}>
              <div className="item-menu-mobile">
                <MenuMobile />
              </div>
              <div className="title-page">{t("title.DashbroadOverview")}</div>
                <Tag icon={<LaptopOutlined />} color="processing">
                  Version: 1.0.2
                </Tag>
              {/* <div className="date-time-system">{moment().format("LLLL")}</div> */}
            </Col>

            <Col xs={4} xl={8} xxl={5}>
              <SearchMqn />
            </Col>

            <Col xs={5} xl={2} xxl={1}>
              <div className="flag-icon-lang">
                {/* <Button onClick={() => {i18n.changeLanguage('vi'); redirect("localhost:3000/?lang=vi");}}><img src="/static/img/vi-flag.png" alt="lang-flag" /></Button> */}
                <Select defaultValue={i18n.language} key="vi" bordered={false}>
                  <Option key="vi" value="vi">
                    <img
                      src="/static/img/vi-flag.png"
                      className="langActive"
                      alt="hinhanh"
                      onClick={() => {
                        i18n.changeLanguage("vi");
                        redirect("localhost:3000/?lang=vi");
                      }}
                    />
                  </Option>
                  <Option key="en" value="en">
                    <img
                      src="/static/img/en-flag.png"
                      className="langActive"
                      alt="hinhanh"
                      onClick={() => {
                        i18n.changeLanguage("en");
                        redirect("localhost:3000/?lang=en");
                      }}
                    />
                  </Option>
                </Select>
              </div>
            </Col>

            <Col xs={6} xl={2} xxl={1}>
              <div className="notification-main" onClick={showDrawer}>
                <Badge color="#FC3149" dot>
                  <img src="/static/img/notification.svg" alt="notification" />
                </Badge>
              </div>
              <Drawer
                title={t("title.GeneralMessage")}
                placement="right"
                onClose={onClose}
                visible={visible}
              >
                <List
                  itemLayout="horizontal"
                  dataSource={data}
                  renderItem={(item) => (
                    <List.Item>
                      <List.Item.Meta
                        avatar={item.avatar}
                        title={<p>{item.title}</p>}
                        description={item.description}
                      />
                    </List.Item>
                  )}
                />
              </Drawer>
            </Col>

            <Col xs={4} xl={2} xxl={1}>
              <Profile />
            </Col>

            <Col xs={4} xl={2} xxl={1}>
              <DarkMode />
            </Col>
          </Row>
        </div>
      </Affix>
    </>
  );
};

export default MenuMain;
