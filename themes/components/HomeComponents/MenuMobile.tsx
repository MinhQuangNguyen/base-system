import React, { useState } from "react";
import {
  Button,
  Drawer,
} from "antd";
import useBaseHooks from "themes/hooks/BaseHooks";
import ItemMenu from "./ItemMenu"




const MenuMobile = (props: any) => {
  const { t, redirect } = useBaseHooks({ lang: ["common"] });

  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={showDrawer}>
        Menu
      </Button>
        <Drawer  className="item-draw-menu-mobile" title={t("title.DashbroadOverview")} placement="left" onClose={onClose} visible={visible}>
          <ItemMenu />
        </Drawer>
    </>
  );
};

export default MenuMobile;
