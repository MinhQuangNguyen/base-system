import { Menu, Badge } from "antd";
import useBaseHooks from "themes/hooks/BaseHooks";
import Link from "next/link";
import { route as makeUrl } from "themes/route";

const SubMenu = Menu.SubMenu;

const DashbroadSvg = () => (
  <>
    <Link {...makeUrl("frontend.landing.index")}>
      <svg
        viewBox="0 0 24 24"
        focusable="false"
        data-icon="index"
        className="icon"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden="true"
      >
        <path d="M12 18V15" />
        <path d="M20.64 19.2401C20.4 20.6501 19.03 21.8101 17.6 21.8101H6.40002C4.96002 21.8101 3.60002 20.6601 3.36002 19.2401L2.03002 11.2801C1.86002 10.3001 2.36002 8.99009 3.14002 8.37009L10.07 2.82009C11.13 1.97009 12.86 1.97009 13.93 2.83009L20.86 8.37009C21.63 8.99009 22.13 10.3001 21.97 11.2801L21.35 15.0001" />
      </svg>
    </Link>
  </>
);


const ChartSvg = () => (
  <>
    <Link {...makeUrl("frontend.landing.chart")}>
      <svg
        viewBox="0 0 24 24"
        className="icon"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M9 22H21" />
        <path d="M2 22H6" />
        <path d="M3 12.9999V17.9999C3 18.5499 3.45 18.9999 4 18.9999H5.59998C6.14998 18.9999 6.59998 18.5499 6.59998 17.9999V9.37988C6.59998 8.82988 6.14998 8.37988 5.59998 8.37988H4C3.45 8.37988 3 8.82988 3 9.37988" />
        <path d="M12.8 5.18994H11.2C10.65 5.18994 10.2 5.63994 10.2 6.18994V17.9999C10.2 18.5499 10.65 18.9999 11.2 18.9999H12.8C13.35 18.9999 13.8 18.5499 13.8 17.9999V6.18994C13.8 5.63994 13.35 5.18994 12.8 5.18994Z" />
        <path d="M21 3C21 2.45 20.55 2 20 2H18.4C17.85 2 17.4 2.45 17.4 3V18C17.4 18.55 17.85 19 18.4 19H20C20.55 19 21 18.55 21 18V7.13" />
      </svg>
    </Link>
  </>
);

const TableSvg = () => (
  <svg viewBox="0 0 24 24" className="icon" xmlns="http://www.w3.org/2000/svg">
    <path d="M22 10H2" />
    <path d="M12 10V22" />
    <path d="M3.96 3.32C2.6 4.43 2 6.27 2 9V15C2 20 4 22 9 22H15C20 22 22 20 22 15V9C22 4 20 2 15 2H9" />
  </svg>
);

const FromSvg = () => (
  <svg viewBox="0 0 24 24" className="icon" xmlns="http://www.w3.org/2000/svg">
    <path d="M22 13V15C22 20 20 22 15 22H9C4 22 2 20 2 15V13.48" />
    <path d="M11 2H9C4 2 2 4 2 9" />
    <path d="M19.93 9.01001L20.98 7.96001C22.34 6.60001 22.98 5.02001 20.98 3.02001C18.98 1.02001 17.4 1.66001 16.04 3.02001L8.16 10.9C7.86 11.2 7.56 11.79 7.5 12.22L7.07 15.23C6.91 16.32 7.68 17.08 8.77 16.93L11.78 16.5C12.2 16.44 12.79 16.14 13.1 15.84L16.28 12.66L17.01 11.93" />
    <path d="M14.91 4.1499C15.58 6.5399 17.45 8.4099 19.85 9.0899" />
  </svg>
);

const CommentSvg = () => (
  <>
    <Link {...makeUrl("frontend.landing.ListComment")}>
      <svg
        viewBox="0 0 24 24"
        className="icon"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M2 7C2 3 3 2 7 2H8.5C10 2 10.33 2.44001 10.9 3.20001L12.4 5.20001C12.78 5.70001 13 6 14 6H17C21 6 22 7 22 11V13" />
        <path d="M9 22H7C3 22 2 21 2 17V12.03" />
        <path d="M13.76 18.3199C11.41 18.4899 11.41 21.8899 13.76 22.0599H19.32C19.99 22.0599 20.65 21.8099 21.14 21.3599C22.79 19.9199 21.91 17.0399 19.74 16.7699C18.96 12.0799 12.18 13.8599 13.78 18.3299" />
      </svg>
    </Link>
  </>
);

const ModalntSvg = () => (
  <>
    <Link {...makeUrl("frontend.landing.ListModal")}>
      <svg
        viewBox="0 0 24 24"
        className="icon"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M19.83 15.45C21.69 13.59 21.69 10.57 19.83 8.70005L15.3 4.17005C14.35 3.22005 13.04 2.71005 11.7 2.78005L6.69998 3.02005C4.69998 3.11005 3.10998 4.70005 3.00998 6.69005L2.76998 11.69C2.70998 13.03 3.20998 14.34 4.15998 15.29L8.68997 19.82C10.55 21.68 13.57 21.68 15.44 19.82L16.96 18.3" />
        <path d="M7 9.5C7 10.88 8.12 12 9.5 12C10.88 12 12 10.88 12 9.5C12 8.12 10.88 7 9.5 7" />
      </svg>
    </Link>
  </>
);

const IcontSvg = () => (
  <Link {...makeUrl("frontend.landing.PluginCamp")}>
    <svg
      viewBox="0 0 24 24"
      className="icon"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M3.67004 10.95V14.47C3.67004 15.45 4.13004 16.38 4.92004 16.97L10.13 20.87C11.24 21.7 12.77 21.7 13.88 20.87L19.09 16.97C19.88 16.38 20.34 15.45 20.34 14.47V2.5H3.68002V6.03999" />
      <path d="M2 2.5H22" />
      <path d="M8 8H16" />
      <path d="M8 13H12" />
      <path d="M15 13H16" />
    </svg>
  </Link>
);

const NotiSvg = () => (
  <>
    <Link {...makeUrl("frontend.landing.NotificationMqn")}>
      <svg
        viewBox="0 0 24 24"
        className="icon"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M3.09 6.73003C3.09 5.91003 3.71001 4.98004 4.48001 4.67004L10.05 2.39007C11.3 1.88007 12.71 1.88007 13.96 2.39007L19.53 4.67004C20.29 4.98004 20.92 5.91003 20.92 6.73003V11.12C20.92 16.01 17.37 20.59 12.52 21.93C12.19 22.02 11.83 22.02 11.5 21.93C6.65 20.59 3.10001 16.01 3.10001 11.12" />
        <path d="M12 12.5C13.1046 12.5 14 11.6046 14 10.5C14 9.39543 13.1046 8.5 12 8.5C10.8954 8.5 10 9.39543 10 10.5C10 11.6046 10.8954 12.5 12 12.5Z" />
        <path d="M12 12.5V15.5" />
      </svg>
    </Link>
  </>
);

const CalendarSvg = () => (
  <>
    <Link {...makeUrl("frontend.landing.CalendareMqn")}>
      <svg
        viewBox="0 0 24 24"
        className="icon"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M8 2V5" />
        <path d="M16 2V5" />
        <path d="M3.5 9.08984H20.5" />
        <path d="M3 13.01V8.5C3 5.5 4.5 3.5 8 3.5H16C19.5 3.5 21 5.5 21 8.5V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17" />
        <path d="M11.9955 13.7H12.0045" />
        <path d="M8.29431 13.7H8.30329" />
        <path d="M8.29431 16.7H8.30329" />
      </svg>
    </Link>
  </>
);

const SquareSvg = () => (
  <Link {...makeUrl("frontend.landing.ComponentLibrary")}>
    <svg
      viewBox="0 0 24 24"
      className="icon"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M2 14V15C2 20 4 22 9 22H15C20 22 22 20 22 15V9C22 4 20 2 15 2H9C4 2 2 4 2 9" />
      <path d="M6.70001 9.26001L12 12.33L17.26 9.28001" />
      <path d="M12 17.7701V12.3201" />
      <path d="M10.76 6.2901L7.56 8.0701C6.84 8.4701 6.23999 9.4801 6.23999 10.3101V13.7001C6.23999 14.5301 6.83 15.5401 7.56 15.9401L10.76 17.7201C11.44 18.1001 12.56 18.1001 13.25 17.7201L16.45 15.9401C17.17 15.5401 17.77 14.5301 17.77 13.7001V10.3001C17.77 9.4701 17.18 8.4601 16.45 8.0601L13.25 6.2801C12.56 5.9001 11.44 5.9001 10.76 6.2901Z" />
    </svg>
  </Link>
);

const ItemMenu = (props: any) => {
  const { t } = useBaseHooks({ lang: ["menu"] });

  return (
    <>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1" icon={<DashbroadSvg />}>
          <Link {...makeUrl("frontend.landing.index")}>
            {t("frontend.home.dash")}
          </Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<ChartSvg />}>
          <Link {...makeUrl("frontend.landing.chart")}>
            {t("frontend.home.chartMqn")}
          </Link>
        </Menu.Item>
        <SubMenu
          key="sub1"
          icon={<TableSvg />}
          title={t("frontend.home.tableMqn.tableMainMqn")}
        >
          <Menu.Item key="5">
            <Link {...makeUrl("frontend.landing.ListTable1")}>
              {t("frontend.home.tableMqn.tableCheckboxMqn")}
            </Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link {...makeUrl("frontend.landing.ListTable2")}>
              {t("frontend.home.tableMqn.tableChartMqn")}
            </Link>
          </Menu.Item>
          <Menu.Item key="7">
            <Link {...makeUrl("frontend.landing.ListTable3")}>
              {t("frontend.home.tableMqn.tableNestedMqn")}
            </Link>
          </Menu.Item>
          <Menu.Item key="8">
            <Link {...makeUrl("frontend.landing.ListTable4")}>
              {t("frontend.home.tableMqn.TabeChangeNestedMqn")}
            </Link>
          </Menu.Item>
          <Menu.Item key="9">
            <Link {...makeUrl("frontend.landing.ListTable5")}>
              {t("frontend.home.tableMqn.TabeSearchMqn")}
            </Link>
          </Menu.Item>
          <Menu.Item key="10">
            <Link {...makeUrl("frontend.landing.ListTable6")}>
              {t("frontend.home.tableMqn.TabeDeleteAdd")}
            </Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu
          key="sub2"
          icon={<FromSvg />}
          title={t("frontend.home.formMqn.formMqnMain")}
        >
          <Menu.Item key="10">
            <Link {...makeUrl("frontend.landing.ListForm1")}>
              {t("frontend.home.formMqn.formNotIcon")}
            </Link>
          </Menu.Item>
          <Menu.Item key="11">
            <Link {...makeUrl("frontend.landing.ListForm2")}>
              {t("frontend.home.formMqn.formIcon")}
            </Link>
          </Menu.Item>
          <SubMenu key="sub3" title={t("frontend.home.formMqn.subFormMqn")}>
            <Menu.Item key="12">
              {t("frontend.home.formMqn.formMqn3")}
            </Menu.Item>
            <Menu.Item key="13">
              {t("frontend.home.formMqn.formMqn4")}
            </Menu.Item>
          </SubMenu>
        </SubMenu>
        <Menu.Item key="14" icon={<CommentSvg />}>
          <Link {...makeUrl("frontend.landing.ListComment")}>
            {t("frontend.home.commentMqn")}
          </Link>
        </Menu.Item>
        <Menu.Item key="15" icon={<ModalntSvg />}>
          <Link {...makeUrl("frontend.landing.ListModal")}>
            {t("frontend.home.modalMqn")}
          </Link>
        </Menu.Item>
        <Menu.Item key="16" icon={<IcontSvg />}>
          <Link {...makeUrl("frontend.landing.PluginCamp")}>
            <span className="text-bage">{t("frontend.home.pluginMqn")}</span>
          </Link>
          <Badge key="1" status="processing" className="badgeMenu" />
        </Menu.Item>
        <Menu.Item key="17" icon={<NotiSvg />}>
          <Link {...makeUrl("frontend.landing.NotificationMqn")}>
            {t("frontend.home.notiMqn")}
          </Link>
        </Menu.Item>
        <Menu.Item key="18" icon={<CalendarSvg />}>
          <Link {...makeUrl("frontend.landing.CalendareMqn")}>
            {t("frontend.home.calenMqn")}
          </Link>
        </Menu.Item>
        <Menu.Item key="19" icon={<SquareSvg />}>
          <Link {...makeUrl("frontend.landing.ComponentLibrary")}>
            {t("frontend.home.componentMqn")}
          </Link>
        </Menu.Item>
      </Menu>
    </>
  );
};

export default ItemMenu;
