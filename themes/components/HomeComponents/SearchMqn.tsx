import { Input, AutoComplete } from "antd";
import { DollarOutlined } from "@ant-design/icons";
import { route as makeUrl } from "themes/route";
import useBaseHooks from "themes/hooks/BaseHooks";

const SearchMqn = (props: any) => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const renderTitle = (title: string) => (
    <span>
      {title}
      <p style={{ float: "right" }}>{t("Button.SeeMore")}</p>
    </span>
  );

  const renderItem = (title: string, count: number) => ({
    value: title,
    label: (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        {title}
        <span className="coinSearch">
          <DollarOutlined /> ${count}
        </span>
      </div>
    ),
  });

  const options = [
    {
      label: renderTitle(t("Input.Budget")),
      options: [
        renderItem(t("Input.TotalDailyBudget"), 1503),
        renderItem(t("Input.TotalBudgetForTheWeek"), 2550),
        renderItem(t("Input.TotalMonthlyBudget"), 8850),
      ],
    },
    {
      label: renderTitle(t("Input.Targets")),
      options: [
        renderItem(t("Input.AdvertisingCosts"), 1243),
        renderItem(t("Input.TotalCampaignCost"), 2250),
      ],
    },
    {
      label: renderTitle(t("Input.Data")),
      options: [renderItem(t("Input.TotalCostForData"), 3550)],
    },
  ];
  return (
    <>
      {/* <div className="input-search-mqn">
        <Search placeholder="Search" />
      </div> */}
      <div className="input-search-mqn">
        <AutoComplete dropdownMatchSelectWidth={500} options={options}>
          <Input.Search size="large" placeholder={t("Input.Search")} />
        </AutoComplete>
      </div>
    </>
  );
};

export default SearchMqn;
