import React from 'react';
import _ from 'lodash'

const debug = require("debug")("mq:form:Context")
const StoreContext = React.createContext();
class StoreProvider extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      store: {}
    }
  }

  setStore = async (newValue) => {
    this.setState({
      store: {
        ...this.state.store,
        ...newValue
      }
    })
  }

  get store() {
    return this.state.store;
  }

  render() {
    return (
      <StoreContext.Provider
        value={{
          store: this.store,
          setStore: this.setStore
        }}
      >
        {this.props.children}
      </StoreContext.Provider>
    );
  }
}
class RenderComponent extends React.Component{
  shouldComponentUpdate(nextProps, nextState){
    return Object.keys(nextProps.store).length > 0
    && nextProps.cProps == this.props.nextProps.cProps
    && JSON.stringify(nextProps.store) != JSON.stringify(this.props.store)
  }
  render(){
    const {setStore, store, cProps} = this.props
    return React.cloneElement(this.props.children, {
      ...cProps,
      setStore,
      store: store
    })
  }
}
class StoreConsumer extends React.Component {
  render() {
    const { children, cProps, selector } = this.props;
    let Component = this.props.component
    return (
      <StoreContext.Consumer>
        {({ store, setStore } = {}) =>{
            let subStore = {};
            if(!Array.isArray(selector)){
              subStore = {}
            }
            else if(selector[0] === '*'){
              subStore = store
            }
            else {
              for(let key of selector){
                _.set(subStore, key, store[key])
              }
            }
            return(
            <RenderComponent cProps={cProps} setStore={setStore} store={subStore}>
              <Component />
            </RenderComponent>
            )
        }
        }
      </StoreContext.Consumer>
    );
  }
}

const connect = (Component, selector = {}) => (props) => {
  const { ...ohterProps } = props
  return (
    <StoreConsumer component={Component} cProps={props} selector={selector}>

    </StoreConsumer>
  )
}

export {
  StoreProvider,
  StoreConsumer,
  connect
}
