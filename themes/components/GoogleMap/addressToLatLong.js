import Geocode from "react-geocode";
const getConfig = require('next/config').default
const { publicRuntimeConfig } = getConfig();
const {GOOGLE_MAP_API_KEY} = publicRuntimeConfig;

Geocode.setApiKey(GOOGLE_MAP_API_KEY);
Geocode.setLanguage("vi");
Geocode.setRegion("vi");
let addressToLatLong = async function (address) {
    if (!address) return false;
    try {
        let data = await Geocode.fromAddress(address);
        if (data && data.status === 'OK') {
            const {
                results
            } = data;
            const {
                lat,
                lng
            } = results[0].geometry.location;
            return {
                lat,
                lng
            };
        }
        return false;
    } catch {
        return false;
    }
}

export default addressToLatLong;