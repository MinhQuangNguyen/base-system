
const { compose, withProps } = require("recompose");
const getConfig = require('next/config').default
const { publicRuntimeConfig } = getConfig();
const {GOOGLE_MAP_API_KEY} = publicRuntimeConfig;
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  DirectionsRenderer,
} = require("react-google-maps");

const MapWithADirectionsRenderer = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAP_API_KEY}&&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div className="google-map-container" />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props => {
  const { data = {}, directions, isMarkerShown } = props;
  let { lat, lng } = data;
  lat = lat ? Number(lat) : 21.022736;
  lng = lng ? Number(lng) : 105.8019441;

  const onClick = function (e){
    if(typeof props.onClick === "function"){
      props.onClick(e);
    }
  }

  return (
    <GoogleMap
      onClick={onClick}
      defaultZoom={13}
      center={new google.maps.LatLng(lat, lng)}
    >
      {isMarkerShown && <Marker
        position={{ lat, lng }}
      />}

      {directions && <DirectionsRenderer directions={directions} />}
    </GoogleMap>
  )
}
);

export default MapWithADirectionsRenderer;