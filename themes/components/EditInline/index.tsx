import { useState } from 'react'
import { Typography, Input } from 'antd';
import {EditTwoTone} from '@ant-design/icons'

const EditInline = ({value, onChange, onComplete, editElement, valuePropName = "value", ...otherProps}: {value: any, onChange?: Function,onComplete?: Function, editElement?: any, valuePropName?: string}) => {
    let [editing, setEditing] = useState(false)
    let [_value, setValue] = useState(value)
    let Component = editElement ? editElement : Input
    let ref: any;
    const onChangeFunc = (e: any) => {
        let value = e;
        if (e.target && e.target.value) value = e.target.value
        setValue(value);
        if(onChange) return onChange(_value)
    }
    const onSubmit = () => {
        setEditing(false)
        if(onComplete) onComplete(_value)
    }

    let props = {
        onChange: onChangeFunc,
        onBlur: onSubmit,
        onPressEnter: onSubmit,
        [valuePropName]: _value
    }

    if (editing) {
        return <Component {...props} ref={(_ref:any) => ref = _ref} />
    }
    return <Typography.Text>{_value} <EditTwoTone onClick={() => setEditing(true)} /></Typography.Text>
}

export default EditInline