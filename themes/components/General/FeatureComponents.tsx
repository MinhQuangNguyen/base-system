import React, { useState } from 'react';
import { Menu, Row, Col, Button, Form, Input, Select } from 'antd';

const FeatureComponents = (props: any) =>{

    return <>
        <div id="feature" className="container">
            <Row>
              <Col md={24} lg={24} xl={24}>
                <div className="titleFeature">
                  <h1>tính năng của salon</h1>
                  <h2>phần mềm quản lý kinh doanh thế hệ mới</h2>
                </div>
              </Col>
            </Row>
            
            <div className="contentFeature">
                <Row>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (5).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>Giao tiếp an toàn</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12}xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (6).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>tự động hóa tài liệu</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (7).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>báo cáo cấp quản lý</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (8).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>tự động hóa quy trình và quy trình làm việc</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (9).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>giữ liên lạc với khách hàng của bạn</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (10).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>nhập thời gian và thanh toán</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (11).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>luôn cập nhật với các trường hợp của bạn</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                  <Col md={24} lg={12} xl={12}>
                    <div className="contentItemFeature">
                        <img src="static/img/download (12).svg" alt=""/>
                        <div className="itemContentFeature">
                              <h1>mẫu cho bất kỳ khu vực thực hành</h1>
                              <p>Hợp tác an toàn và liên hệ từ mọi nơi.</p>
                          </div>
                      </div>
                  </Col>
                </Row>
              </div>
          </div>
    </>
}

export default FeatureComponents