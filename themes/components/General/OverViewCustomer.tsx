import React, { useState } from 'react';
import { Menu, Row, Col, Button, Form, Input, Select } from 'antd';
import CountUp from 'react-countup';
const FeatureComponents = (props: any) =>{

    return <>
       <div id="reviewMain" >
            <Row className="container">
              <Col sm={24} md={24} lg={24} xl={24}>
                <div className="titleTextTop">
                  <h1>đánh giá tổng quan</h1>
                  <h2>sự hài lòng của khách hàng là động lực của chúng tôi</h2>
                </div>
              </Col>
              <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                <div className="backborder">
                  <div className="backPro">
                    <div className="contentTextReview">
                        <h1><CountUp start={0} end={5500} duration={5}/>+</h1>
                        <h2>Nhân viên</h2>
                      </div>
                  </div>
                </div>
              </Col>
              <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                  <div className="backborder">
                  <div className="backPro">
                    <div className="contentTextReview">
                        <h1><CountUp start={0} end={300}/>+</h1>
                        <h2>Nhân viên</h2>
                      </div>
                  </div>
                </div>
              </Col>
              <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="backborder">
                  <div className="backPro">
                    <div className="contentTextReview">
                        <h1><CountUp start={0} end={180}/>+</h1>
                        <h2>Nhân viên</h2>
                      </div>
                  </div>
                </div>
              </Col>
              <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="backborder">
                  <div className="backPro">
                    <div className="contentTextReview">
                        <h1><CountUp start={0} end={2000}/>+</h1>
                        <h2>Nhân viên</h2>
                      </div>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
    </>
}

export default FeatureComponents