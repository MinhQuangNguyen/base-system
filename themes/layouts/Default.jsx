import React, { Suspense } from 'react';
import Head from 'next/head';
// import style from 'themes/scss/default.scss'
class Main extends React.Component {
  render() {
    return (<React.Fragment>
      <Head>
        <title>{this.props.title || ""}</title>
      </Head>
      <div id="root">
        <main>
          {/* <div className="toolbar"/> */}
          <div className="content">
            {this.props.children}
          </div>
        </main>
      </div>
    </React.Fragment>
    );
  }
}


export default Main;