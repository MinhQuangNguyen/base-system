import React, { Suspense } from "react";
import Head from "next/head";
import MenuMain from "../components/HomeComponents/MenuMain";
import HeaderMqn from "../components/HomeComponents/HeaderMqn";
import SupportRobot from "../components/HomeComponents/SupportRobot";
import Footer from "../components/HomeComponents/Footer";
import { Layout } from "antd";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import to from "await-to-js";

// import style from 'themes/scss/default.scss'
const HomeLayout = (props: any) => {
  const { metaData = {}, data = {} } = props;
  return (
    <React.Fragment>
      <Head>
        <title>{metaData.title || data.siteTitle || []}</title>
        <meta
          property="og:url"
          content={`${data.siteUrl}${metaData.url || ""}`}
        />
        <meta property="og:type" content={metaData.type || "website"} />
        <meta property="og:title" content={metaData.title || data.siteTitle} />
        <meta
          property="og:description"
          content={metaData.description || data.siteDescription}
        />
        <meta property="og:image" content={metaData.image || data.logo} />
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"></meta>
      </Head>
      <div id="home" className="App">
        <main>
          <Layout>
            <MenuMain />
              <div className="site-layout">
                <HeaderMqn />
                {props.children}
                <SupportRobot/>
              </div>
            <Footer />
          </Layout>
        </main>
      </div>
    </React.Fragment>
  );
};

export default HomeLayout;
