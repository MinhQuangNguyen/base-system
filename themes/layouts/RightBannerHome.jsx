import { BaseComponent } from 'themes/components'
import HomeLayout from 'themes/layouts/Home'
import Breadcrumb from 'themes/components/HomeComponents/Breadcrumb'
import RightSider from 'themes/components/HomeComponents/RightSider'
import { Row, Col } from 'antd';
import { Layout } from 'antd';
const { Content } = Layout;


class PostCategory extends BaseComponent {
  render() {
    let { data = {}, contentClassName } = this.props;
    let {breadcrumb = [], categoryName, metaData = {}} = data;
    return (
      <HomeLayout data = {data} metaData = {metaData}>
        <Breadcrumb categoryName={categoryName} breadcrumb = {breadcrumb} />
        <Content className={`home-container ${contentClassName}`}>
          <Row gutter={[16, 16]} type="flex" justify="center">
            <Col xs={24} lg={18} xl={14}>
              {this.props.children}
            </Col>
            <Col xs={24} lg={6} xl={6}><RightSider data = {data}/></Col>
          </Row>
        </Content>
      </HomeLayout>
    )
  }
}

export default PostCategory;