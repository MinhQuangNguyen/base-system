import React, { Fragment, createRef, useEffect } from 'react';
import { Menu, Layout, Avatar, Button, Input } from 'antd';
import { MenuUnfoldOutlined, MenuFoldOutlined, PoweroffOutlined, SettingFilled, TeamOutlined, MoneyCollectOutlined, RightOutlined, BellOutlined } from '@ant-design/icons'
import ConfirmDialog from 'themes/components/Dialogs/ConfirmDialog';
import useBaseHook from 'themes/hooks/BaseHooks'
import Cookies from 'universal-cookie'

const { Header } = Layout;
const { SubMenu } = Menu
const {Search} = Input

const AdminHeader = (props: any) => {
  const { getCookies, redirect, t, getData, getAuth } = useBaseHook()
  const auth = getAuth();
  const cookieObject = getCookies()
  const confirmRef = createRef<ConfirmDialog>()
  const handleClickMenu = (e: any) => { }
  const redirectLogin = () => {
    const cookies = new Cookies(cookieObject)
    cookies.remove('token')
    cookies.remove('user')
    redirect('frontend.admin.login')
  }
  useEffect(() => {
    console.log("vao.....")
    if (!cookieObject.token || !cookieObject.user) {
     // redirect('frontend.admin.login')
    }
  }, [])

  const renderConfirmDialog = () => {
    return (
      <ConfirmDialog
        ref={confirmRef}
        onSubmit={redirectLogin}
        title={t('signout')}
        content={t('message.signoutConfirm')}
      />
    )
  }

  const renderRightContent = () => {
    return <div>
      <Button icon={<PoweroffOutlined />} type="link" className="header-btn" onClick={() => { if (confirmRef.current) confirmRef.current.show() }}></Button>
      <Button icon={<SettingFilled spin />} type="link" className="header-btn"></Button>
      

    </div>
  }

  const { collapsed, onCollapseChange } = props
  const menuIconProps = {
    className: "trigger",
    onClick: () => onCollapseChange(!collapsed)
  }
  let headerClass = "header"
  if (collapsed) headerClass += ' collapsed'

  return <React.Fragment>
    <Header
      className={headerClass}>
      {collapsed ? <MenuUnfoldOutlined style={{ color: "#fff" }} {...menuIconProps} /> : <MenuFoldOutlined style={{ color: "#fff" }} {...menuIconProps} />}
      <Button icon={<TeamOutlined />} type="link" className="header-btn"></Button>
      <Button icon={<MoneyCollectOutlined />} type="link" className="header-btn"></Button>
      <Button icon={<BellOutlined />} type="link" className="header-btn"></Button>
      <Search
      placeholder="Tìm kiếm..."
      onSearch={value => console.log(value)}
      className="header-search"
    />
      <div className='rightContainer'>{renderRightContent()}</div>

    </Header>
    {renderConfirmDialog()}
  </React.Fragment>
}

export default AdminHeader