import React from 'react';
import { Row, Col, Avatar, Divider } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import useBaseHook from 'themes/hooks/BaseHooks'

const Profile = () => {
    const { getCookies, redirect, t, getData, getAuth } = useBaseHook()
    const auth = getAuth();
    return <div className="sidebar-profile">
        <Row>
            <Col xs={24}>
                <div className="profile-avatar">
                    <Avatar size={80} icon={<UserOutlined />} src="https://thememakker.com/templates/oreo/html/assets/images/profile_av.jpg" className="avatar" />
                </div>
            </Col>
        </Row>
        <Row>
            <Col xs={24}>
                <div className="profile-name">Đỗ Duy Ngọc</div>
                <div className="profile-address">12 Khuất Duy Tiến, Thanh Xuân, Hà Nội</div>
            </Col>
        </Row>
        <Row>
            <Col xs={8}>
                <div className="profile-info">
                    <p className="value">324</p>
                    <p className="label">Đơn hàng</p>

                </div>
            </Col>
            <Col xs={8}>
                <div className="profile-info">
                    <p className="value">85.6M</p>
                    <p className="label">Doanh Thu</p>

                </div>
            </Col>
            <Col xs={8}>
                <div className="profile-info">
                    <p className="value">852</p>
                    <p className="label">Khách hàng</p>
                </div>
            </Col>
        </Row>
        <Row>
            <Col xs={22} offset={1}>
                <Divider />
            </Col>
        </Row>


    </div>
}

export default Profile