// declare module 'react-slick';

interface Company{
    id: number,
    name: string,
    address: string
    info: string,
    status: number
}

interface ServiceSchedule {
    id: number,
    productId: number,
    duration: number,
    content: string
}

interface UserGroup {
    id: number,
    name: string,
    description: string
}

interface User {
    id: number,
    fullname: string,
    username: string,
    email: string,
    phone: number,
    groupId: number
}

interface Product{
    id: number,
    companyId: number,
    name: string,
    code: string,
    categoryId: number,
    description: string,
    quantity: number,
    price: number,
    status: number,
    limit: number,
    type:number
}

interface ProductCategory{
    id: number,
    companyId: number,
    name: string,
    description: string,
    status: number
}

interface Customer{
    id: number,
    companyId: number,
    fullname: string,
    phone: string,
    address: string,
    note: string,
    dob: Date,
    email: string
}

interface Order{
    id: number,
    companyId: number,
    customerId: number,
    status: number,
    date: Date
    amount: number,
    discount: number
}

interface OrderDetail{
    id: number,
    orderId: number,
    productId: number,
    quantity: number,
    price: number,
    discount: number,
    quantity: number,
    userId: number,
}

interface Permission{
    id?: number,
    name: string,
    description: string,
    key: string,
    avalibleValue: number,
    value?: number
}

interface PermissionCategories{
    id: number,
    name: string,
    description: string
    permissions: Permission[]
}
