import BaseApi from './BaseService'

class Revenue extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'salaries';
  }

  async getByMonth(data: any){
    let url = this.route(`api.${this.name}.getByMonth`, data).href
    return await this.Request.get(url)
  }

  async updateDetail(data: any){
    const url = this.route(`api.${this.name}.updateDetail`, data).href
    return await this.Request.put(url, data)
  }
}

export default Revenue