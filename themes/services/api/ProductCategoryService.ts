import BaseApi from './BaseService'

class ProductCategory extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'categories'; //kế thừa CRUD từ thằng cha
  }
  async getByProductType(data:any) {
    const url = this.route(`api.${this.name}.getByProductType`).href
    return await this.Request.get(url,data)
  }
}

export default ProductCategory