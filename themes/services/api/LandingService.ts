import BaseApi from './BaseService'

class Customer extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'frontend.landing'; //kế thừa CRUD từ thằng cha
  }
  async contact(data: any){
    const url = this.route(`api.${this.name}.search`, data).href
    return await this.Request.get(url, data)
  }
}

export default Customer