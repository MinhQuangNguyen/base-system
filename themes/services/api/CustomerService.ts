import BaseApi from './BaseService'

class Customer extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'customers'; //kế thừa CRUD từ thằng cha
  }
  async search(data: any){
    const url = this.route(`api.${this.name}.search`, data).href
    return await this.Request.get(url, data)
  }
  async histories(data: any){
    const url = this.route(`api.${this.name}.histories`, data).href
    return await this.Request.get(url, data)
  }
  async booking(data: any) {
    const url = this.route(`api.${this.name}.booking`,data).href
    return await this.Request.get(url,data)
  }
  async updateBooking(data: any) {
    const url = this.route(`api.${this.name}.updateBooking`,data).href
    return await this.Request.put(url,data)
  }
}

export default Customer