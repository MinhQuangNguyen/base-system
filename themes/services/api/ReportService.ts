import BaseApi from './BaseService'

class Report extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'report'; //kế thừa CRUD từ thằng cha
  }
  async revenue(data: any){
    const url = this.route(`api.${this.name}.revenue`, data).href
    return await this.Request.get(url, data)
  }
  async order(data: any){
    const url = this.route(`api.${this.name}.order`, data).href
    return await this.Request.get(url, data)
  }
  async customer(data: any){
    const url = this.route(`api.${this.name}.customer`, data).href
    return await this.Request.get(url, data)
  }
}

export default Report