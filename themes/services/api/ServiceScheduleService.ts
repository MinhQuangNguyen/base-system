import BaseApi from "./BaseService";

class ServiceSchedule extends BaseApi {
  constructor(context?: Context) {
    super(context);
    this.name = "serviceSchedules"; //kế thừa CRUD từ thằng cha
  }

  async getByProductId(data: any) {
    const url = this.route(`api.${this.name}.getByProductId`, data).href;
    return await this.Request.get(url, data);
  }
}

export default ServiceSchedule;
