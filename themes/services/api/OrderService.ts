import BaseApi from './BaseService'

class Order extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'orders'; //kế thừa CRUD từ thằng cha
  } 
    // list order
  async listOrder(data: any){
    const url = this.route(`api.${this.name}.listOrder`, data).href
    return await this.Request.get(url, data)
  }
  // chi tiết order
  async viewOrder(data: any){
    const url = this.route(`api.${this.name}.viewOrder`, data).href
    return await this.Request.get(url, data)
  }
  // lưu tạm
  async tempStore(data: any) {
    const url = this.route(`api.${this.name}.newOrder`, data).href
    return await this.Request.post(url, data)
  }
  //update order tạm
    async tempUpdate(data: any){
    const url = this.route(`api.${this.name}.editOrder`, data).href
    return await this.Request.put(url, data)
  }
  // hủy order
  async cancelOrder(data: any){
    const url = this.route(`api.${this.name}.cancelOrder`, data).href
    return await this.Request.delete(url, data)
  }
}

export default Order