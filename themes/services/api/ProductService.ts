import BaseApi from './BaseService'

class Product extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'products'; //kế thừa CRUD từ thằng cha
  }

  async search(data: any){
    const url = this.route(`api.${this.name}.search`, data).href
    return await this.Request.get(url, data)
  }

  async updateQuantity({id, newQuantity}: {id: number, newQuantity: number}) {
    const url = this.route(`api.${this.name}.updateQuantity`).href
    return await this.Request.put(url, { id: id, newQuantity: newQuantity })
  }
  async getProducts(data:any) {
    const url = this.route(`api.${this.name}.getProducts`).href
    return await this.Request.get(url,data)
  }
  async sProducts(data:any) {
    const url = this.route(`api.${this.name}.sProducts`).href
    return await this.Request.get(url,data)
  }

  async import(data: any) {
    const url = this.route(`api.${this.name}.import`).href
    return await this.Request.put(url,data)
  }
  async export(data: any) {
    const url = this.route(`api.${this.name}.export`).href
    return await this.Request.put(url,data)
  }
}

export default Product