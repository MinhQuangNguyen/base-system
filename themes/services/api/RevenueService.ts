import BaseApi from './BaseService'

class Revenue extends BaseApi {
  constructor(context?: Context){
    super(context)
    this.name = 'revenues';
  }

  async closeMonth(data:any) {
    const url = this.route(`api.${this.name}.closeMonth`, data).href
    return await this.Request.post(url, data)
  }

  async getByDate(data:any) {
    const url = this.route(`api.${this.name}.getByDate`, data).href
    return await this.Request.get(url)
  }
  async getByMonth(data:any) {
    const url = this.route(`api.${this.name}.getByMonth`, data).href
    return await this.Request.get(url)
  }
  async getByYear(data:any) {
    const url = this.route(`api.${this.name}.getByYear`, data).href
    return await this.Request.get(url)
  }
  async getByDateRange(data:any) {
    const url = this.route(`api.${this.name}.getByDateRange`, data).href
  }
}

export default Revenue