import {useState, useEffect} from 'react'
import _ from 'lodash'
let events: any = []
const OrderHook = () => {
    let defaultState: {
        orderDetails: any[],
        customer: any,
        info: any
    } = { orderDetails: [], info: {}, customer: {} }

    const [state, setState] = useState(defaultState)
    useEffect(() => {
        return () => {
            events = events.filter((e: Function) => e != setState)
        }
    }, [])
    
    if (!events.includes(setState)) {
        events.push(setState)
    }
    const setStateFunc: Function = (newData: any) => {
        console.log("fsdfs", JSON.stringify(newData))
        return events.map((event: any) => event({ ...state,...newData }))
    }
    return [state, setStateFunc] as const
}

export default OrderHook