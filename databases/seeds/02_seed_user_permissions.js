
exports.seed = function (knex, Promise) {

    let permissions = [
        {
            id: 1,
            key: "root",
            name: "Quyền Root",
            description: "Root",
            avalibleValue: 15
        },
        {
            id: 2,
            key: "admin.userGroups",
            name: "Quản lý nhóm tài khoản",
            description: "Cho phép tạo mới hoặc chỉnh sửa các nhóm tài khoản nhân viên.",
            avalibleValue: 15
        },
        {
            id: 3,
            key: "admin.users",
            name: "Quản lý tài khoản",
            description: "Cho phép tạo mới hoặc chỉnh sửa các tài khoản nhân viên.",
            avalibleValue: 15
        },
        {
            id: 4,
            key: "admin.products",
            name: "Quản lý sản phẩm",
            description: "Cho phép tạo mới hoặc chỉnh sửa danh sách sản phẩm.",
            avalibleValue: 15
        },
        {
            id: 5,
            key: "admin.categories",
            name: "Quản lý chuyên mục sản phẩm",
            description: "Cho phép tạo mới hoặc chỉnh sửa danh sách sản phẩm.",
            avalibleValue: 15
        }
    ]

    // Deletes ALL existing entries
    return knex('user_permissions').del()
        .then(async () => {
            // Inserts seed entries
            await knex('user_permissions').insert(permissions);
            await knex.raw('select setval(\'user_permissions_id_seq\', max(id)) from user_permissions');
        });
};