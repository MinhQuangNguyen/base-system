
exports.seed = function (knex, Promise) {

    let initObj = {
        name: 'salon',
        address: 'X',
        // groupId: 1,

    };
    let arr = [];
    for (let index = 1; index < 10; index++) {
        let obj = { ...initObj };
        obj.id = index;
        obj.name = obj.name + index;
        obj.address = obj.address + index;
        arr.push(obj)
    }

    // Deletes ALL existing entries
    return knex('companies').del()
        .then(async () => {
            // Inserts seed entries
            await knex('companies').insert(arr);
            await knex.raw('select setval(\'companies_id_seq\', max(id)) from companies');
        });
};