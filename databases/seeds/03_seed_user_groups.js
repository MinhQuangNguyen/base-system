
exports.seed = function (knex, Promise) {

    let initObj = {
        name: 'Chủ Cửa hàng ',
        description: 'Nhóm mặc định của chủ cửa hàng',

    };
    let arr = [];
    for (let index = 1; index < 10; index++) {
        let obj = { ...initObj };
        obj.id = index;
        obj.name = obj.name + index;
        obj.companyId = index;
        arr.push(obj)
    }

    // Deletes ALL existing entries
    return knex('user_groups').del()
        .then(async () => {
            // Inserts seed entries
            await knex('user_groups').insert(arr);
            await knex.raw('select setval(\'user_groups_id_seq\', max(id)) from user_groups');
        });
};