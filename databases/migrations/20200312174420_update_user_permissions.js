
exports.up = function(knex) {
    return knex.schema.table('user_permissions', function (table) {
        table.integer('sort').notNullable().defaultTo(0);
        table.integer('categoryId').nullable().index().references('id').inTable('permission_categories')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    })
};

exports.down = function(knex) {
    return knex.schema.table('user_permissions', function (table) {
        table.dropColumn('sort');
        table.dropColumn('categoryId');
    })
};
