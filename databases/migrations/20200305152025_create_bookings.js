
exports.up = function(knex) {
    return knex.schema.createTable('bookings', function (table) {
        table.increments();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('customerId').nullable();
        table.timestamp('datetime').notNullable();
        table.text('note').nullable();
        table.integer('status').defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('bookings');
};
