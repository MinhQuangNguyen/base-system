exports.up = function (knex) {
    return knex.schema.table('service_schedules', function (table) {
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    })
};

exports.down = function (knex) {
    return knex.schema.table('service_schedules', function (table) {
        table.dropColumn('createdAt');
        table.dropColumn('updatedAt');
        table.dropColumn('updatedBy');
    })
};