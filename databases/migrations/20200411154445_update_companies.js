
exports.up = function(knex) {
  return knex.schema.table('companies', function (table) {
    table.string('zaloPhone').nullable();
    table.string('zaloGroup').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('companies', function (table) {
    table.dropColumn('zaloPhone');
    table.dropColumn('zaloGroup')
  })
};
