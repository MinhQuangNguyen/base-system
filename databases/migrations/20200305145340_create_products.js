
exports.up = function(knex) {
    return knex.schema.createTable('products', function (table) {
        table.increments();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.string('name').nullable();
        table.string('code').nullable();
        table.integer('categoryId').nullable().index().references('id').inTable('categories')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.text('description').nullable();
        table.integer('quantity').defaultTo(0);
        table.decimal('price', 16, 2).defaultTo(0);
        table.decimal('discount', 16, 2).defaultTo(0);
        table.decimal('userPay', 16, 2).defaultTo(0);
        table.integer('status').defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('products');
};
