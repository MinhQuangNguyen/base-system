
exports.up = function (knex) {
  return knex.schema.table('revenues', function (table) {
    table.string('month').notNullable();
    table.integer('orders').nullable().defaultTo(0);
  })
};

exports.down = function (knex) {
  return knex.schema.table('revenues', function (table) {
    table.dropColumn('month');
    table.dropColumn('orders');
  })
};