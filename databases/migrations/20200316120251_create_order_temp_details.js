exports.up = function (knex) {
    return knex.schema.createTable('order_temp_details', function (table) {
        table.increments();
        table.integer('orderTempId').notNullable().index().references('id').inTable('order_temps')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('productId').nullable().index().references('id').inTable('products')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.integer('quantity').defaultTo(0);
        table.decimal('price', 16, 2).defaultTo(0);
        table.decimal('discount', 16, 2).defaultTo(0);
        table.integer('userId').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable('order_temp_details');
};