
exports.up = function (knex) {
  return knex.schema.table('salary_details', function (table) {
    table.decimal('salaryByDate', 16, 2).defaultTo(0);
  })
};

exports.down = function (knex) {
  return knex.schema.table('salary_details', function (table) {
    table.dropColumn('salaryByDate');
  })
};