
exports.up = function(knex) {
    return knex.schema.createTable('packs', function(table) {
        table.increments();
        table.string('name').nullable();
        table.text('description').nullable();
        table.decimal('price', 16, 2).defaultTo(0);
        table.decimal('discount', 16, 2).defaultTo(0);
        table.integer('status').defaultTo(0);
        table.integer('timeLimit').defaultTo(0).comment("Thoi han su dung");
        table.integer('userLimit').defaultTo(0).comment("So nguoi dung toi da");
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('packs');
};
