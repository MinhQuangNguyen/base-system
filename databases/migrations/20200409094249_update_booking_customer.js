
exports.up = function(knex) {
  return knex.schema.table('bookings', function (table) {
    table.string('name').nullable();
    table.string('phone').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('bookings', function (table) {
    table.dropColumn('name');
    table.dropColumn('phone');
  })
};
