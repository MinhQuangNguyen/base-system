
exports.up = function(knex) {
    return knex.schema.createTable('customers', function (table) {
        table.increments();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.string('fullname').notNullable();
        table.string('phone').notNullable();
        table.string('address').nullable();
        table.text('note').nullable();
        table.timestamp('dob').nullable();
        table.string('email').nullable();
        table.timestamp('created').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');

    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('customers');
};
