exports.up = function (knex) {
    return knex.schema.createTable('revenues', function (table) {
        table.increments();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('products').nullable();
        table.integer('services').nullable();
        table.decimal('revenue', 16, 2).defaultTo(0);
        table.decimal('payment', 16, 2).defaultTo(0);
        table.decimal('profit', 16, 2).defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.integer('createdBy').nullable().index().references('id').inTable('users')
        .onUpdate('CASCADE')
        .onDelete('SET NULL');
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable('revenues');
};