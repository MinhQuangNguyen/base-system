
exports.up = function(knex) {
    return knex.schema.createTable('product_histories', function (table) {
        table.increments();
        table.integer('productId').notNullable().index().references('id').inTable('products')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('quantity').defaultTo(0);
        table.decimal('price', 16, 2).defaultTo(0);
        table.integer('newQuantity').defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('product_histories');
};
