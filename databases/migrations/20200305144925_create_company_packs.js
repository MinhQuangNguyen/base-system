
exports.up = function(knex) {
    return knex.schema.createTable('company_packs', function (table) {
        table.increments();
        table.integer('companyId').nullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('packId').nullable().index().references('id').inTable('packs')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.decimal('price', 16, 2).nullable();
        table.decimal('discount', 16, 2).nullable();
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('finishedAt').nullable();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('company_packs');
};
