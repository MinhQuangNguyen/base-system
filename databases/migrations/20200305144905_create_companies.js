
exports.up = function(knex) {
    return knex.schema.createTable('companies', function (table) {
        table.increments();
        table.string('name').notNullable();
        table.string('address').nullable();
        table.text('info').nullable();
        table.string('phone').nullable();
        table.integer('status').defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('companies');
};
