
exports.up = function(knex) {
    return knex.schema.createTable('permission_categories', function (table) {
        table.increments();
        table.string('name').notNullable();
        table.text('description').nullable();
        table.integer('sort').notNullable().defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('permission_categories');
};
