exports.up = function (knex) {
    return knex.schema.createTable('salaries', function (table) {
        table.increments();
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable();
        table.integer('status').defaultTo(0);
        table.text('note').nullable();
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable('salaries');
};