
exports.up = function(knex) {
    return knex.schema.createTable('salary_details', function (table) {
        table.increments();
        table.integer('salaryId').notNullable().index().references('id').inTable('salaries')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('userId').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.string('username').notNullable();
        table.string('groupname').nullable();
        table.decimal('salary', 16, 2).defaultTo(0).comment('Lương cứng');
        table.decimal('extra', 16, 2).defaultTo(0).comment('Lương doanh số');
        table.decimal('fine', 16, 2).defaultTo(0).comment('Giảm trừ lương');
        table.decimal('bonus', 16, 2).defaultTo(0).comment('Tiền thưởng');
        table.decimal('total', 16, 2).defaultTo(0).comment('Tổng nhận');
        table.text('note').nullable();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('salary_details');
};
