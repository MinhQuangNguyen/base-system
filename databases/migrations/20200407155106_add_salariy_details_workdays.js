
exports.up = function(knex) {
  return knex.schema.table('salary_details', function (table) {
      table.decimal('workdays').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('salary_details', function (table) {
      table.dropColumn('workdays');
  })
};
