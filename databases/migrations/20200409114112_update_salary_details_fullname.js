
exports.up = function(knex) {
  return knex.schema.table('salary_details', function (table) {
    table.string('fullname').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('salary_details', function (table) {
    table.dropColumn('fullname');
  })
};
