
exports.up = function(knex) {
  return knex.schema.table('customers', function (table) {
    table.string('avatar').nullable();
    table.string('cover').nullable();
    table.integer('gender').nullable();
    table.string('sdob').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('customers', function (table) {
    table.dropColumn('avatar');
    table.dropColumn('cover');
    table.dropColumn('gender');
    table.dropColumn('sdob');
  })
};
