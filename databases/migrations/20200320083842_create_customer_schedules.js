exports.up = function (knex) {
    return knex.schema.createTable('customer_schedules', function (table) {
        table.increments();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('customerId').notNullable().index().references('id').inTable('customers')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('scheduleId').notNullable().index().references('id').inTable('service_schedules')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('status').defaultTo(0).comment("0: chưa nhắc, 1: đã nhắc");
        table.timestamp('orderDate').notNullable();
        table.timestamp('scheduleDate').notNullable();
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').nullable();
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
        .onUpdate('CASCADE')
        .onDelete('SET NULL');
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable('customer_schedules');
};