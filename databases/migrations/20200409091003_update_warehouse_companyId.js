
exports.up = function (knex) {
  return knex.schema.table('warehouse_bills', function (table) {
    table.integer('companyId').notNullable().index().references('id').inTable('companies')
      .onUpdate('CASCADE')
      .onDelete('CASCADE');
  })
};

exports.down = function (knex) {
  return knex.schema.table('warehouse_bills', function (table) {
    table.dropColumn('companyId');
  })
};
