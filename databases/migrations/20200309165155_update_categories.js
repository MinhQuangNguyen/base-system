
exports.up = function (knex) {
    return knex.schema.table('categories', function (table) {
        table.integer('parentId');
    })
};

exports.down = function (knex) {
    return knex.schema.table('categories', function (table) {
        table.dropColumn('parentId');
    })
};
