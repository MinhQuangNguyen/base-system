exports.up = function(knex) {
    return knex.schema.table('products', function (table) {
        table.integer('type').comment("0: dịch vụ; 1: sản phẩm");
        table.integer('limit').comment("Số lượng sản phẩm tối thiểu,lượng hàng tồn,....");
        table.decimal('importPrice', 16, 2).defaultTo(0);
    })
};

exports.down = function(knex) {
    return knex.schema.table('products', function (table) {
        table.dropColumn('type').comment("0: dịch vụ; 1: sản phẩm");
        table.dropColumn('limit');
        table.dropColumn('importPrice');
    })
};
