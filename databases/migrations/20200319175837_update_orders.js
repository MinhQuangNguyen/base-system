exports.up = function(knex) {
    return knex.schema.table('orders', function (table) {
        table.decimal('amount', 16, 2).defaultTo(0);
        table.integer('services').defaultTo(0).comment("Số lượng dịch vụ");
        table.integer('products').defaultTo(0).comment("Số lượng sản phẩm");
    })
};

exports.down = function(knex) {
    return knex.schema.table('orders', function (table) {
        table.dropColumn('amount');
        table.dropColumn('services');
        table.dropColumn('products');
    })
};
