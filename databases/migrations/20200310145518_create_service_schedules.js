exports.up = function (knex) {
    return knex.schema.createTable('service_schedules', function (table) {
        table.increments();
        table.integer('duration').notNullable();
        table.string('content').defaultTo(0);
        table.integer('productId').notNullable().index().references('id').inTable('products')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable('service_schedules');
};