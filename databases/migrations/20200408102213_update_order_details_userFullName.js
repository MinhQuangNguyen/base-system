
exports.up = function (knex) {
  return knex.schema.table('order_details', function (table) {
      table.string('userFullName').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('order_details', function (table) {
      table.dropColumn('userFullName');
  })
};
