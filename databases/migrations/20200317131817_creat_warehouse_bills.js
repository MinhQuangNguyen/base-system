exports.up = function(knex) {
    return knex.schema.createTable('warehouse_bills', function(table) {
        table.increments();
        table.integer('type').nullable().comment("1: import; 2: export");
        table.string('code').nullable();
        table.timestamp('date').nullable();
        table.string('note').nullable();
        table.decimal('total', 16, 2).defaultTo(0);
        table.decimal('cash', 16, 2).defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('warehouse_bills');
};
