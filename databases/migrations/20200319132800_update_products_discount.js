exports.up = function(knex) {
    return knex.schema.table('products', function (table) {
        table.integer('discountType').comment("0: km tiền; 1: km phần trăm");
    })
};

exports.down = function(knex) {
    return knex.schema.table('products', function (table) {
        table.dropColumn('discountType').comment("0: km tiền; 1: km phần trăm");
    })
};
