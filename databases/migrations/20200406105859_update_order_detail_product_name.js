exports.up = function(knex) {
  return knex.schema.table('order_details', function (table) {
      table.string('productName').nullable();
  })
};

exports.down = function(knex) {
  return knex.schema.table('order_details', function (table) {
      table.dropColumn('productName');
  })
};
