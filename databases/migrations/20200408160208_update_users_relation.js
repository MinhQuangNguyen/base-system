
exports.up = function (knex) {
  return knex.schema.table('users', function (table) {
    table.foreign('groupId').references('id').inTable('user_groups')
      .onUpdate('CASCADE')
      .onDelete('SET NULL');
  })
};

exports.down = function (knex) {
  return knex.schema.table('users', function (table) {
    table.dropForeign('groupId');
  })
};
