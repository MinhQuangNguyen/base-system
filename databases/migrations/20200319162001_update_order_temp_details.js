exports.up = function(knex) {
    return knex.schema.table('order_temp_details', function (table) {
        table.integer('productType').nullable();
        table.integer('discountType').nullable();
        table.decimal('userPay', 16, 2).nullable();
    })
};

exports.down = function(knex) {
    return knex.schema.table('order_temp_details', function (table) {
        table.dropColumn('productType');
        table.dropColumn('discountType');
        table.dropColumn('userPay')
    })
};
