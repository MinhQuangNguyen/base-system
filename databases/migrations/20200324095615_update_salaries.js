
exports.up = function(knex) {
    return knex.schema.table('salaries', function (table) {
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.string('month').notNullable();
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.table('salaries', function (table) {
        table.dropColumn('companyId');
        table.dropColumn('month');
        table.dropColumn('updatedAt');
    })
};
