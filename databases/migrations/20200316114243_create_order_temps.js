exports.up = function (knex) {
    return knex.schema.createTable('order_temps', function (table) {
        table.increments();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('customerId').nullable();
        table.timestamp('datetime').nullable();
        table.integer('totalQuantity').defaultTo(0);
        table.decimal('totalAmount', 16, 2).defaultTo(0);
        table.decimal('totalDiscount', 16, 2).defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};
exports.down = function (knex) {
    return knex.schema.dropTable('order_temps');
};