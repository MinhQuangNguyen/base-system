
exports.up = function (knex) {
  return knex.schema.table('salaries', function (table) {
    table.integer('monthWorkdays').notNullable().defaultTo(26);
  })
};

exports.down = function (knex) {
  return knex.schema.table('salaries', function (table) {
    table.dropColumn('monthWorkdays');
  })
};