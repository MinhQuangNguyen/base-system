
exports.up = function(knex) {
    return knex.schema.table('orders', function (table) {
        table.decimal('total', 16, 2).defaultTo(0).comment("Tổng tiền");
        table.decimal('paid', 16, 2).defaultTo(0).comment("Khách trả");
        table.decimal('back', 16, 2).defaultTo(0).comment("Trả lại khách");
    })
};

exports.down = function(knex) {
    return knex.schema.table('orders', function (table) {
        table.dropColumn('total')
        table.dropColumn('paid');
        table.dropColumn('back');
    })
};
