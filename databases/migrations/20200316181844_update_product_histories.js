exports.up = function(knex) {
    return knex.schema.table('product_histories', function (table) {
        table.integer('type').comment("0: update; 1: import; 2 export");
        table.integer('billId').nullable();
    })
};

exports.down = function(knex) {
    return knex.schema.table('product_histories', function (table) {
        table.dropColumn('type').comment("0: update; 1: import; 2 export");
        table.dropColumn('billId');
    })
};
