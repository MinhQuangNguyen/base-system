
exports.up = function(knex) {
    return knex.schema.table('orders', function (table) {
        table.string('code').nullable();
        table.text('note').nullable();
        table.integer('discountType').defaultTo(0);
        table.decimal('discount', 16, 2).defaultTo(0);
        table.dropColumn('datetime');
        table.dropColumn('totalDiscount');
        table.dropColumn('totalAmount');
        table.dropColumn('totalQuantity');
    })
};

exports.down = function(knex) {
    return knex.schema.table('orders', function (table) {
        table.dropColumn('code')
        table.dropColumn('note');
        table.dropColumn('discountType');
        table.dropColumn('discount');
    })
};
