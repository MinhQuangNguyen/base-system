
exports.up = function (knex) {
    return knex.schema.createTable('users', function (table) {
        table.increments();
        table.string('username').notNullable();
        table.string('password').notNullable();
        table.string('token').notNullable();
        table.string('fullname').nullable();
        table.string('email').nullable();
        table.string('phone').nullable();
        table.string('address').nullable();
        table.integer('groupId').nullable();
        table.integer('companyId').notNullable().index().references('id').inTable('companies')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('status').defaultTo(0).comment("0 => deactive, 1 => active");
        table.decimal('salary', 16, 2).defaultTo(0);
        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.integer('updatedBy').nullable().index().references('id').inTable('users')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('users');
};
