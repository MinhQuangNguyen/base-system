var positions = [
  {
    id: 1,
    name: "Dưới menu trang chủ, 25:9",
    position: "topBanner",
    ratio: Number(25/9),
    key: "topBanner"
  },
  {
    id: 2,
    name: "Bên sườn phải, 3:4",
    position: "rightBanner",
    ratio: Number(3/4),
    key: "rightBanner"
  }
];

exports.positions = positions;
