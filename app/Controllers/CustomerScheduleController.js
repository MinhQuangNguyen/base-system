const BaseController = require("./BaseController");
const CustomerScheduleRepository = require("@app/Repositories/CustomerScheduleRepository");
const CustomerRepository = require("@app/Repositories/CustomerRepository");
const ServiceScheduleRepository = require("@app/Repositories/ServiceScheduleRepository");
const UserRepository = require("@app/Repositories/UserRepository");
const ApiException = require("@app/Exceptions/ApiException");
const moment = require('moment');
const {scheduleStatus} = require("@config/constant")

class CustomerScheduleController extends BaseController {
    constructor() {
        super();
        this.Repository = new CustomerScheduleRepository();
        this.CustomerRepository = new CustomerRepository();
        this.ServiceScheduleRepository = new ServiceScheduleRepository();
        this.UserRepository = new UserRepository();
    }

    async index() {
        let auth = this.request.auth;
        let data = this.request.all()
    
        let select = [
            'customer_schedules.*',
            'customers.fullname as name',
            'customers.phone as phone',
            'service_schedules.content as content',
            'products.name as productName'
        ]
        this.Repository.setCompanyId(auth.companyId);
        let schedules = this.Repository.query()
            .select(select)
            .leftJoin('customers', {
                'customer_schedules.customerId': 'customers.id'
            })
            .leftJoin('service_schedules', {
                'customer_schedules.scheduleId': 'service_schedules.id'
            })
            .leftJoin('products', {
                'products.id': 'service_schedules.productId'
            })
            .getForGridTable(data);
        return schedules;
    }
  
    async update() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        let data = {};
        data.status = scheduleStatus.close
        data.updatedAt = new Date();
        data.updatedBy = auth.id;

        return await this.Repository.query(auth.companyId).updateOne(inputs.id, data)
    }

    async getByDate() {
        let auth = this.request.auth;
        let inputs = this.request.all();
        
        const partern = '^(([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]))$';
        let match = inputs.date.match(partern);
        if (!match) {
            throw new ApiException(7003, "Date is not valid");
        }
        let fromDate = moment(inputs.date).startOf('day');
        let toDate = fromDate.clone().add(1, 'days');

        this.Repository.setCompanyId(auth.companyId);
        let schedules = this.Repository.getList(fromDate, toDate);

        return schedules;
    }

    async getByDateRange() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const partern = '^([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])?$';
        let match1 = inputs.from.match(partern);
        let match2 = inputs.to.match(partern);
        if (!match1 || !match2) {
            throw new ApiException(7003, "Date is not valid");
        }

        let fromDate = moment(inputs.from);
        let toDate = moment(inputs.to).endOf('day');

        this.Repository.setCompanyId(auth.companyId);
        let schedules = this.Repository.getList(fromDate, toDate);

        return schedules;
    }
}

module.exports = CustomerScheduleController