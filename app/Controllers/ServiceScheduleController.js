const BaseController = require("./BaseController")
const ServiceScheduleRepository = require("@app/Repositories/ServiceScheduleRepository")
const ProductRepository = require("@app/Repositories/ProductRepository")
const ApiException = require("@app/Exceptions/ApiException");
const Auth = require('@core/Auth');
const authConfig = require('@config/auth')
const _ = require('lodash')

class ServiceScheduleController extends BaseController {
  constructor() {
    super()
    this.Repository = new ServiceScheduleRepository();
    this.ProductRepository = new ProductRepository();
  }
  
  async getByProductId() {
    let params = this.request.all()
    let id = params.id
    if (!id) throw new ApiException(9996, "ID is required!");
    const auth = this.request.auth || {};
    let result = await this.ProductRepository.query(auth.companyId).getById(id);
    if (!result) {
      throw new ApiException(7002, 'Data not found');
    }
    let schedules = await this.Repository.query().getByCondition({
      productId: result.id
    });
    return schedules
  }

  async store() {
    let user = this.request.auth;
    let params = this.request.all();
    const allowFields = {
        productId: "number!",
        duration: "number!",
        content: "string",
    };
    let data = this.validate(params, allowFields, {
        removeNotAllow: true
    });
   
    data.updatedBy = user.id;
    let result = await this.Repository.query().insertOne(data);
    return result;
  }

  async update() {
    let user = this.request.auth;
    let params = this.request.all();
    const allowFields = {
        id: "number!",
        duration: "number!",
        content: "string",
    };
    let data = this.validate(params, allowFields, {
        removeNotAllow: true
    });
   
    data.updatedBy = user.id;
    let result = await this.Repository.query().updateOne(data.id, data);
    return result;
  }
}

module.exports = ServiceScheduleController