const BaseController = require("./BaseController");
const UserPermissionRepository = require("@app/Repositories/UserPermissionRepository");
const UserGroupRepository = require("@app/Repositories/UserGroupRepository");
const ApiException = require("@app/Exceptions/ApiException");

class UserPermissionController extends BaseController {
    constructor() {
        super()
        this.Repository = new UserPermissionRepository();
        this.UserGroupRepository = new UserGroupRepository();
    }

    async getByGroupId(){
        let inputs = this.request.all();
        if (!inputs.groupId){
            throw new ApiException(7002, "GroupId is required");
        }
        return await this.Repository.getByGroupId(inputs.groupId);
    }
}

module.exports = UserPermissionController