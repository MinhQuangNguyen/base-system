const BaseController = require("./BaseController");
const ProductRepository = require("@app/Repositories/ProductRepository");
const ProductHistoryRepository = require("@app/Repositories/ProductHistoryRepository");
const CategoryRepository = require("@app/Repositories/CategoryRepository");
const ServiceScheduleRepository = require("@app/Repositories/ServiceScheduleRepository");
const ApiException = require("@app/Exceptions/ApiException");
const WareHouseBillsRepository = require("@app/Repositories/WareHouseBillsRepository");
const _ = require('lodash');
const { productUpdateTypes, productTypes, productStatus } = require("@config/constant")

class ProductController extends BaseController {
    constructor() {
        super();
        this.Repository = new ProductRepository();
        this.ProductHistoryRepository = new ProductHistoryRepository();
        this.CategoryRepository = new CategoryRepository();
        this.ServiceScheduleRepository = new ServiceScheduleRepository();
        this.WareHouseBillsRepository = new WareHouseBillsRepository();
    }

    async index() {
        const data = this.request.all()
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId)
        const select = ['products.*', 'categories.name as categoryName'];
        let result = await this.Repository.query()
            .select(select)
            .leftJoin('categories', {
                'products.categoryId': 'categories.id'
            })
            .getForGridTable(data);
        return result;
    }

    /**
     * Lấy danh sách sản phẩm được hiện
     */
    async sProducts() {
        const data = this.request.all()
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId)
        const select = ['products.*', 'categories.name as categoryName'];
        let result = await this.Repository.query()
            .select(select)
            .where({
                'products.status': productStatus.show
            })
            .leftJoin('categories', {
                'products.categoryId': 'categories.id'
            })
            .getForGridTable(data);
        return result;
    }

    async store() {
        let user = this.request.auth;
        this.Repository.setCompanyId(user.companyId);
        let params = this.request.all();
        const allowFields = {
            name: "string!",
            code: "string",
            categoryId: "number",
            description: "string",
            quantity: "number",
            price: "number",
            discount: "number",
            discountType: "number",
            userPay: "number",
            status: "number",
            limit: "number",
            type:"number"
        };
        let data = this.validate(params, allowFields, {
            removeNotAllow: true
        });
        if (data['categoryId']) {
            let checkCategory = await this.CategoryRepository.query(user.companyId).getById(data['categoryId']);
            if (!checkCategory) {
                throw new ApiException(7004, "Categegory does not exists!")
            }
        }
        let checkExistName = await this.Repository.getByName(data['name']);
        let checkExistCode = await this.Repository.getByCode(data['code']);
        if (checkExistName || checkExistCode) {
            throw new ApiException(7002, "Product already exists!")
        }
        data.companyId = user.companyId;
        data.updatedBy = user.id;
        let result = await this.Repository.query().insertOne(data);
        return result;
    }

    async update() {
        let auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
        let inputs = this.request.all();
        const allowFields = {
            name: "string!",
            code: "string",
            categoryId: "number",
            description: "string",
            quantity: "number",
            price: "number",
            discount: "number",
            discountType: "number",
            userPay: "number",
            status: "number",
            limit: "number",
            type:"number"
        };
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        })
          
        let exist = await this.Repository.getById(inputs.id)
      
        if (!exist) {
            throw new ApiException(7002, "Product does not exist!")
        }
        
        if (data['categoryId']) {
            let checkCategory = await this.CategoryRepository.query(auth.companyId).getById(data['categoryId']);
            if (!checkCategory) {
                throw new ApiException(7004, "Categegory does not exists!")
            }
        }
        // let existUserName = await this.Repository.getByName(data['name']);
        // let existUserCode = await this.Repository.getByCode(data['code']);
        //   if (existUserName && existUserName.id != inputs.id && existUserCode.id) {
        //     throw new ApiException(7001, "Fullname already exists!");
        //   }

        if (data.name) {
            let checkDuplicate = await this.Repository.query().andWhere({ 'name': data.name }).andWhereNot('id', inputs.id);
            if (!_.isEmpty(checkDuplicate)) {
                throw new ApiException(7002, "Product name is exist!")
            }
        }
        if (data.code) {
            let checkDuplicate = await this.Repository.query().andWhere({ 'code': data.code }).andWhereNot('id', inputs.id);
            if (!_.isEmpty(checkDuplicate)) {
                throw new ApiException(7002, "Product code is exist!")
            }
        }

        let result = await this.Repository.query().updateOne(inputs.id, data);
        return result;
    }

    async search() {
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
    
        const inputs = this.request.all();
        const allowFields = {
          keyword: "string!"
        }
        
        const data = this.validate(inputs, allowFields)
    
        const select = ['products.*'];
        let result = await this.Repository.query()
            .select(select)
            .where('status', productStatus.show)
            .where(function (query) {
                query.where('name', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
                .orWhere('code', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
            })
            .limit(12)
        return result;
    }

    async updateQuantity(){
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
        this.ProductHistoryRepository.setCompanyId(auth.companyId);
        const inputs = this.request.all();
        const allowFields = {
            id:"number",
            newQuantity:"number"
        }
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        })
        let id = data.id
        let newQuantity = data.newQuantity
        let type = productUpdateTypes.update
        let price = 0
        return await this.Repository.updateQuantity({id,newQuantity,type,price})
    }
    async getProducts() {
        const data = this.request.all()
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId)
        const select = ['products.*', 'categories.name as categoryName'];
        let result = await this.Repository.query()
            .select(select)
            .where({
                'products.type': productTypes.product
            })
            .leftJoin('categories', {
                'products.categoryId': 'categories.id'
            })
            .getForGridTable(data);
        return result;
    }
    async import(){
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
        this.ProductHistoryRepository.setCompanyId(auth.companyId);
        this.WareHouseBillsRepository.setCompanyId(auth.companyId);
        const inputs = this.request.all();
        const allowFields = {
            code:"string!",
            datetime: "date!",
            note: "string",
            data: "object!",
            value: {
                total: "number!",
                cash: "number!"
            }
        }
        let values = this.validate(inputs, allowFields, {
            removeNotAllow: false
        })
        let { code, datetime, note ,data} = values
        let { total, cash } = values.value
        let bill = await this.WareHouseBillsRepository.getByCode(values.code)
        if (bill) {
            throw new ApiException(7004, "Bill exist!")
        }
        let importBill = await this.WareHouseBillsRepository.query().insertOne({
            type: productUpdateTypes.import,
            code: code,
            date: datetime,
            note: note,
            total: total,
            cash: cash,
            companyId: auth.companyId,
        }); 
        for (let product of data) {
            let id = product.id
            let newQuantity = product.selectedQuantity
            let type = productUpdateTypes.import
            let price = product.selectedPrice
            await this.Repository.updateQuantity({id,newQuantity,type,price})
        }
        if (importBill) {
            return {message:"update success"}
        }
    }

    async export(){
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
        this.ProductHistoryRepository.setCompanyId(auth.companyId);
        this.WareHouseBillsRepository.setCompanyId(auth.companyId);
        const inputs = this.request.all();
        const allowFields = {
            code:"string!",
            datetime: "date!",
            note: "string",
            data: "object!",
            value: {
                total: "number!",
                cash: "number!"
            }
        }
        let values = this.validate(inputs, allowFields, {
            removeNotAllow: false
        })
        let { code, datetime, note ,data} = values
        let { total, cash } = values.value
        let bill = await this.WareHouseBillsRepository.getByCode(values.code)
        if (bill) {
            throw new ApiException(7004, "Bill exist!")
        }
        let exportBill = await this.WareHouseBillsRepository.query().insertOne({
            type: productUpdateTypes.export,
            code: code,
            date: datetime,
            note: note,
            total: total,
            cash: cash,
            companyId: auth.companyId,
        }); 
        for (let product of data) {
            let id = product.id
            let newQuantity = product.selectedQuantity
            let type = productUpdateTypes.export
            let price = product.selectedPrice
            await this.Repository.updateQuantity({id,newQuantity,type,price})
        }
        if (exportBill) {
            return {message:"update success"}
        }
    }
}

module.exports = ProductController;