const BaseController = require("./BaseController")
const CustomerRepository = require("@app/Repositories/CustomerRepository")
const OrderRepository = require("@app/Repositories/OrderRepository")
const BookingRepository = require("@app/Repositories/BookingRepository")
const OrderDetailRepository = require("@app/Repositories/OrderDetailRepository")
const ApiException = require("@app/Exceptions/ApiException");
const SmsService = require("@app/Services/Sms")
const _ = require('lodash')
const moment = require('moment')
const { bookingStatus } = require("@config/constant")

class CustomerController extends BaseController {
  constructor() {
    super()
    this.Repository = new CustomerRepository();
    this.OrderRepository = new OrderRepository();
    this.BookingRepository = new BookingRepository();
    this.OrderDetailRepository = new OrderDetailRepository();
  }


  async store() {
    let inputs = this.request.all()

    const allowFields = {
      fullname: "string!",
      phone: "string!",
      address: "string",
      note: "string",
      dob: "date",
      email: "string",
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    });

    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId);

    let checkExist = await this.Repository.query().getOne({ phone: data.phone })
    if (checkExist) {
      throw new ApiException(7001, "Phone already exists!");
    }

    data.companyId = auth.companyId;
    let getInfo = await SmsService.getPhoneInfo(data.phone);

    if (getInfo) {
      data.avatar = getInfo.avatar
      data.cover = getInfo.cover
      data.sdob = getInfo.sdob
      data.dob = moment(getInfo.dob, 'X')
    }

    let result = await this.Repository.query().insertOne(data);
    return result
  }

  async update() {
    const auth = this.request.auth;
    let inputs = this.request.all()

    const allowFields = {
      fullname: "string!",
      phone: "string!",
      address: "string",
      note: "string",
      dob: "date",
      email: "string",
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    })

    this.Repository.setCompanyId(auth.companyId)
    let exist = await this.Repository.getById(inputs.id)

    if (!exist) {
      throw new ApiException(7004, "Customer does not exist!")
    }

    if (data.phone) {
      let checkDuplicate = await this.Repository.query().andWhere({ 'phone': data.phone }).andWhereNot('id', inputs.id);
      if (!_.isEmpty(checkDuplicate)) {
        throw new ApiException(7002, "Category name is exist!")
      }
    }

    data["companyId"] = auth.companyId;
    let result = await this.Repository.query().updateOne(inputs.id, data);
    return result;
  }

  async search() {
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId);

    const inputs = this.request.all();
    const allowFields = {
      keyword: "string!"
    }

    const data = this.validate(inputs, allowFields)

    const select = ['customers.*'];
    let customers = await this.Repository.query()
      .select(select)
      .where('phone', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
      .orWhere('fullname', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
      .limit(12)

    return customers;
  }

  async histories() {
    const auth = this.request.auth;
    const inputs = this.request.all();

    const allowFields = {
      id: "number!"
    }
    const data = this.validate(inputs, allowFields)

    this.OrderRepository.setCompanyId(auth.companyId);

    let select = [
      'order_details.id',
      'order_details.userFullName as userFullname',
      'order_details.productName',
      'order_details.createdAt',
      'order_details.orderId',
      'order_details.quantity',
      'order_details.price',
    ]
    let test = await this.OrderDetailRepository.query()
      .select(select)
      .leftJoin('orders', 'orders.id', 'order_details.orderId')
      .where('orders.customerId', data.id)
      .getForGridTable(inputs);

    return test
  }

  async getBooking() {
    const auth = this.request.auth;
    const params = this.request.all();

    const select = [
      'bookings.*',
      'customers.fullname as customerName',
      'customers.phone as customerPhone'
    ]
    let booking = await this.BookingRepository.query(auth.companyId)
      .select(select)
      .join('customers', 'customers.id', 'bookings.customerId')
      .getForGridTable(params)

    return booking
  }

  async addBooking() {
    const auth = this.request.auth;
    const params = this.request.all();

    const allowFields = {
      customerId: "number!",
      datetime: "date!",
      note: "string"
    }
    let data = this.validate(params, allowFields, { removeNotAllow: true })
    let checkCustomers = await this.Repository.query(auth.companyId).getById(data.customerId);
    if (!checkCustomers) {
      throw new ApiException(7004, "Customer does not exist!")
    }
    data.companyId = auth.companyId

    let booking = await this.BookingRepository.query(auth.companyId).insertOne(data);
    booking.customerName = checkCustomers.fullname

    return booking;
  }

  async updateBooking() {
    const auth = this.request.auth;
    const params = this.request.all();

    const allowFields = {
      id: "number!",
      datetime: "date",
      note: "string",
      status: "number"
    }

    let data = this.validate(params, allowFields, { removeNotAllow: true });

    return await this.BookingRepository.query(auth.companyId).updateOne(data.id, data);
  }

  // async booking() {
  //   const auth = this.request.auth;
  //   const inputs = this.request.all();
  //   this.BookingRepository.setCompanyId(auth.companyId);
  //   let select = ['bookings.*','customers.fullname as customerName','customers.phone as customerPhone']

  //   let bookings = await this.BookingRepository.query()
  //     .select(select)
  //     .join('customers', 'customers.id', 'bookings.customerId')
  //     .getForGridTable(inputs);

  //   return bookings
  // }
  // async updateBooking() {
  //   const auth = this.request.auth;
  //   const inputs = this.request.all();

  //   const allowFields = {
  //     id: "number!",
  //     status: "number!"
  //   }
  //   const data = this.validate(inputs, allowFields)

  //   this.BookingRepository.setCompanyId(auth.companyId);

  //   let exist = await this.BookingRepository.getById(data.id)

  //   if (!exist) {
  //     throw new ApiException(7002, "Booking does not exist!")
  //   }

  //   let histories = await this.BookingRepository.query()
  //     .updateOne(data.id, {
  //     status: data.status
  //   })

  //   return histories
  // }
}

module.exports = CustomerController