const BaseController = require("./BaseController");
const OrderRepository = require("@app/Repositories/OrderRepository");
const OrderDetailRepository = require("@app/Repositories/OrderDetailRepository");
const OrderTempRepository = require("@app/Repositories/OrderTempRepository");
const OrderTempDetailRepository = require("@app/Repositories/OrderTempDetailRepository");
const CustomerRepository = require("@app/Repositories/CustomerRepository");
const ApiException = require("@app/Exceptions/ApiException");
const ServiceScheduleRepository = require("@app/Repositories/ServiceScheduleRepository")
const CustomerScheduleRepository = require("@app/Repositories/CustomerScheduleRepository")
const OrderService = require("@app/Services/OrderService");
const ProductRepository = require("@app/Repositories/ProductRepository");
const RedisService = require('@app/Services/Redis')
const moment = require('moment');
const _ = require('lodash');
const { productUpdateTypes, orderStatus, productTypes } = require('@config/constant')

class OrderController extends BaseController {
    constructor() {
        super();
        this.Repository = new OrderRepository();
        this.OrderDetailRepository = new OrderDetailRepository();
        this.OrderTempRepository = new OrderTempRepository();
        this.OrderTempDetailRepository = new OrderTempDetailRepository();
        this.CustomerRepository = new CustomerRepository();
        this.ServiceScheduleRepository = new ServiceScheduleRepository();
        this.CustomerScheduleRepository = new CustomerScheduleRepository();
        this.ProductRepository = new ProductRepository();
        this.OrderService = new OrderService();
    }

    async index() {
        const auth = this.request.auth;
        const inputs = this.request.all();

        this.Repository.setCompanyId(auth.companyId)
        const select = ['orders.*', 'customers.fullname as customerName', 'customers.phone as customerPhone'];
        let result = await this.Repository.query()
            .select(select)
            .leftJoin('customers', {
                'orders.customerId': 'customers.id'
            })
            .getForGridTable(inputs);
        return result;
    }

    /**
     * ORDER - đơn hàng đã thành công
     */
    async detail() {
        const inputs = this.request.all()
        const auth = this.request.auth;

        let order = await this.Repository.query(auth.companyId).getById(inputs.id);
        if (!order) {
            throw new ApiException(7004, "Order does not exist!")
        }
        let result = {};
        result.customer = await this.CustomerRepository.query(auth.companyId).getById(order.customerId);
        result.orderDetails = await this.OrderDetailRepository.query().getAll({ 'orderId': order.id });
        result.info = order;
        return result;
    }

    /**
     * ORDER - cập nhật trạng thái
     */
    async update() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const allowFields = {
            customer: {
                id: "number",
                companyId: "number",
                fullname: "string",
                phone: "string",
                address: "string",
                note: "string",
                dob: "date",
                email: "string"
            },
            orderDetails: [{
                productId: "number",
                productName: "string",
                productType: "number",
                quantity: "number",
                price: "number",
                discount: "number",
                discountType: "number",
                userId: "number",
                userFullName: "string",
                userPay: "number"
            }],
            info: {
                id: "number",
                total: "number",
                back: "number",
                discount: "number",
                discountType: "number",
                paid: "number",
                note: "string",
                createdAt: "date",
                status: "number"
            }
        };
        const data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        })

        this.Repository.setCompanyId(auth.companyId)
        let checkExist = await this.Repository.query().getById(data.info.id);
        if (!checkExist) {
            throw new ApiException(7004, "Order does not exist!")
        }

        if (checkExist.status === orderStatus.close) {
            throw new ApiException(7005, "Order is closed!")
        }

        let result = {};
        result.customer = data.customer;
        if (_.has(data, 'customer.id')) {
            let checkCustomer = await this.CustomerRepository.query(auth.companyId).getById(data.customer.id);
            if (!checkCustomer) {
                throw new ApiException(7004, "Customer does not exist!")
            }
            data.info.customerId = data.customer.id;
            result.customer = checkCustomer;
        }
        let orderId = data.info.id
        result.info = await this.Repository.query().updateOne(orderId, data.info);
        result.orderDetails = await this.OrderDetailRepository.updateMany(orderId, data.orderDetails);
        return result;
    }

    /**
     * ORDER - Chốt đơn, lưu đơn hàng, xóa đơn tạm
     */
    async store() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const cacheName = `${auth.companyId}:tempOrders:${inputs.id}`
        let checkExist = await RedisService.get(cacheName);
        if (!checkExist) {
            throw new ApiException(7004, "Order does not exist!")
        }

        const allowFields = {
            customer: {
                id: "number!",
                companyId: "number",
                fullname: "string",
                phone: "string",
                address: "string",
                note: "string",
                dob: "date",
                email: "string",
                createdAt: "date",
            },
            orderDetails: [{
                productId: "number!",
                productName: "string",
                productType: "number",
                quantity: "number",
                price: "number",
                discount: "number",
                discountType: "number",
                userId: "number",
                userFullName: "string",
                userPay: "number"
            }],
            info: {
                total: "number",
                back: "number",
                discount: "number",
                discountType: "number",
                paid: "number",
                note: "string",
                createdAt: "date",
            }
        };

        let data = this.validate(checkExist, allowFields, {
            removeNotAllow: true
        });

        let checkCustomer = await this.CustomerRepository.query(auth.companyId).getById(data.customer.id);
        if (!checkCustomer) {
            throw new ApiException(7004, "Customer does not exist!");
        }

        /**
         * Check số lượng sản phẩm
         */
        const productIds = _.map(data.orderDetails, 'productId');
        this.ProductRepository.setCompanyId(auth.companyId);
        let products = await this.ProductRepository.query().whereIn('id', productIds);
        for (let product of products) {
            let requestProduct = data.orderDetails[productIds.indexOf(product.id)]
            if (product.quantity < requestProduct.quantity && product.type === productTypes.product) {
                throw new ApiException(7003, `${product.name} không đủ số lượng!`)
            }
        }

        let orderData = data['info'];
        let orderTotal = await this.OrderService.getOrderTotal(orderData, data.orderDetails);
        orderData.amount = orderTotal.amount;
        orderData.services = orderTotal.services;
        orderData.products = orderTotal.products;
        orderData.companyId = auth.companyId;
        orderData.customerId = data.customer.id;
        orderData.updatedBy = auth.id;
        orderData.code = inputs.id;

        this.Repository.setCompanyId(auth.companyId);
        let order = await this.Repository.query().insertOne(orderData);

        let orderDetails = await this.OrderDetailRepository.insertMany(order.id, data.orderDetails);

        /**
         * Trừ sản phẩm trong kho hàng
         */
        for (let detail of orderDetails) {
            if (detail.productType === productTypes.product) {
                let id = detail.productId
                let newQuantity = detail.quantity
                let type = productUpdateTypes.export
                let price = detail.price
                await this.ProductRepository.updateQuantity({ id, newQuantity, type, price })
            }
        }

        /**
         * Tạo lịch nhắc
         */
        const allProductIds = _.map(orderDetails, 'productId');

        let scheduleData = await this.ServiceScheduleRepository.getSchedule(allProductIds);
        if (scheduleData) {
            for (let i in scheduleData) {
                scheduleData[i].companyId = auth.companyId;
                scheduleData[i].customerId = order.customerId;
                scheduleData[i].orderDate = order.createdAt;
                scheduleData[i].scheduleDate = moment(order.createdAt).add(scheduleData[i].duration, 'days');
                delete scheduleData[i].duration;
            }
            await this.CustomerScheduleRepository.query(auth.companyId).insertMany(scheduleData);
        }

        await RedisService.del(cacheName)
        let result = {};
        result.customer = checkCustomer;
        result.orderDetails = orderDetails;
        result.info = order;
        return result;
    }

    /**
     * TEMP - Danh sách đơn hàng đang thực hiện
     */
    async listOrder() {
        let auth = this.request.auth;

        const cacheName = `${auth.companyId}:tempOrders:*`
        let checkExist = await RedisService.keys(cacheName);
        let data = {}
        for (let i in checkExist) {
            let id = checkExist[i].replace(/^(\d{1,}:\w{10}:)/, '');
            data[id] = await RedisService.get(checkExist[i]);
            data[id].key = id;
        }

        let results = Object.keys(data).map(function (key) { return data[key] });
        return _.orderBy(results, 'key', 'desc');
    }

    /**
     * TEMP - Xem 1 đơn hàng đang thực hiện
     */
    async viewOrder() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const cacheName = `${auth.companyId}:tempOrders:${inputs.id}`
        let checkExist = await RedisService.get(cacheName);
        if (!checkExist) {
            throw new ApiException(7004, "Order does not exist!")
        }
        return checkExist;
    }

    /**
     * TEMP - Khởi tạo đơn hàng mới
     */
    async newOrder() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const allowFields = {
            customer: {
                id: "number",
                companyId: "number",
                fullname: "string",
                phone: "string",
                address: "string",
                note: "string",
                dob: "date",
                email: "string",
                createdAt: "date",
            },
            orderDetails: [{
                productId: "number",
                productName: "string",
                productType: "number",
                quantity: "number",
                price: "number",
                discount: "number",
                discountType: "number",
                userId: "number",
                userFullName: "string",
                userPay: "number"
            }],
            info: {
                total: "number",
                back: "number",
                discount: "number",
                discountType: "number",
                paid: "number",
                note: "string"
            }
        };
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });

        if (Object.keys(data).length === 0) {
            data = {
                customer: {},
                orderDetails: [],
                info: {}
            }
        }

        const key = await this.getOrderCode(auth.companyId);
        data.info.createdAt = moment();
        data.info.createdBy = auth.id;

        const cacheName = `${auth.companyId}:tempOrders:${key}`;
        await RedisService.set(cacheName, data);
        data.key = key;
        return data
    }

    /**
     * TEMP - Chỉnh sửa đơn hàng đang thực hiện (thêm sửa xóa sản phẩm)
     */
    async editOrder() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const cacheName = `${auth.companyId}:tempOrders:${inputs.id}`
        let checkExist = await RedisService.get(cacheName);
        if (!checkExist) {
            throw new ApiException(7004, "Order does not exist!")
        }
        const allowFields = {
            customer: {
                id: "number",
                companyId: "number",
                fullname: "string",
                phone: "string",
                address: "string",
                note: "string",
                dob: "date",
                email: "string",
                createdAt: "date",
            },
            orderDetails: [{
                productId: "number",
                productName: "string",
                productType: "number",
                quantity: "number",
                price: "number",
                discount: "number",
                discountType: "number",
                userId: "number",
                userFullName: "string",
                userPay: "number"
            }],
            info: {
                total: "number",
                back: "number",
                discount: "number",
                discountType: "number",
                paid: "number",
                note: "string",
                createdAt: "date",
                createdBy: "number"
            }
        };
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });

        await RedisService.set(cacheName, data)
        return data
    }

    /**
     * TEMP - Hủy đơn hàng đang thực hiện
     */
    async cancelOrder() {
        const auth = this.request.auth;
        const inputs = this.request.all();
        const cacheName = `${auth.companyId}:tempOrders:${inputs.id}`
        await RedisService.del(cacheName)
        return true;
    }

    /**
     * Tạo mã đơn hàng mới
     */
    async getOrderCode(companyId) {
        const date = moment();
        let code = date.format("YYMM")

        let orderTemp = _.head(await this.listOrder());

        let orderCode = await this.Repository.query(companyId)
            .first('code')
            .whereBetween('createdAt', [date.clone().startOf('month'), date.clone().endOf('month')])
            .orderBy('code', 'desc');
        return await this.OrderService.getCurrentOrderNo(code, orderTemp.key, orderCode.code);
    }


}

module.exports = OrderController;