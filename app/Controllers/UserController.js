const BaseController = require("./BaseController")
const UserRepository = require("@app/Repositories/UserRepository")
const UserGroupRepository = require("@app/Repositories/UserGroupRepository")
const ApiException = require("@app/Exceptions/ApiException");
const Auth = require('@core/Auth');
const authConfig = require('@config/auth')
const _ = require('lodash')

class UserController extends BaseController {
  constructor() {
    super()
    this.Repository = new UserRepository();
    this.UserGroupRepository = new UserGroupRepository();
  }

  async index() {
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId)
    const select = ['users.*', 'user_groups.name as groupName'];
    let result = await this.Repository.query()
    .select(select)
    .leftJoin('user_groups', {
      'users.groupId': 'user_groups.id'
    }).getForGridTable();
    for (let i in Object.keys(result['data'])) {
      delete result['data'][i]['password'];
      delete result['data'][i]['token'];
    }
    return result;
  }

  async detail() {
    let params = this.request.all()
    let id = params.id
    if (!id) throw new ApiException(9996, "ID is required!");
    const auth = this.request.auth || {};
    this.Repository.setCompanyId(auth.companyId)
    let result = await this.Repository.getById(id);
    if (!result) {
      throw new ApiException(7002, 'Data not found')
    }
    delete result['password'];
    delete result['token'];
    return result
  }

  async login() {
    const inputs = this.request.all();
    const allowFields = {
      username: "string!",
      password: "string!"
    }
    
    const data = this.validate(inputs, allowFields);
    let user = await this.Repository.checkLogin({
      username: data.username,
      password: data.password
    })
    if (!user) {
      throw new ApiException(7000, "Can not login")
    }

    const permissions = await this.UserGroupRepository.setCompanyId(user.companyId).getPermissions(user.groupId);
    let token = Auth.generateJWT({
      id: user.id,
      username: user.username,
      roles: user.role,
      companyId: user.companyId,
      permissions: permissions,
      type: "user"
    }, {
      key: authConfig['SECRET_KEY'],
      expiresIn: authConfig['JWT_EXPIRE_USER']
    });

    this.response.success({
      token,
      user: {
        ...user,
        permissions
      }
    })
  }

  async store() {
    let inputs = this.request.all()

    const allowFields = {
      username: "string!",
      fullname: "string!",
      email: "string!",
      password: "string!",
      groupId: "number!",
      phone: "string"
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    });
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId);

    let checkExist = await this.Repository.getByUsername(data['username']);

    if (checkExist) {
      throw new ApiException(7001, "Username already exists!");
    }
    data['token'] = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
    data['companyId'] = auth.companyId;
    if (data['password']) {
      data['password'] = await this.Repository.hash(data['password']);
    }
    let result = await this.Repository.query().insertOne(data);
    delete result['password']
    return result
  }

  async updatePassword() {
    let inputs = this.request.all()

    const allowFields = {
      password: "string!"
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    });
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId);
    let exist = await this.Repository.getById(inputs.id);

    if (!exist) {
      throw new ApiException(7002, "User does not exist!")
    }
    data['password'] = await this.Repository.hash(data['password'])
    let result = await this.Repository.query().updateOne(inputs.id, data);
    delete result['password']
    return result
  }

  async update(id, input) {
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId)
    let inputs = this.request.all()
    const allowFields = {
      username: "string!",
      fullname: "string!",
      email: "string",
      // password: "string!",
      groupId: "number!",
      phone: "string",
      address: "string",
      salary: "number",
      status: "integer"
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    })
    
    let exist = await this.Repository.getById(inputs.id)

    if (!exist) {
      throw new ApiException(7002, "User does not exist!")
    }

    let existUserName = await this.Repository.getByUsername(data['username']);
    if (existUserName && existUserName.id != inputs.id) {
      throw new ApiException(7001, "Username already exists!");
    }
    data["companyId"] = auth.companyId;
    let result = await this.Repository.query().updateOne(inputs.id, data);
    delete result['password'];
    return result;
  }

  async search() {
    const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
    
        const inputs = this.request.all();
        const allowFields = {
          keyword: "string!"
        }
        
        const data = this.validate(inputs, allowFields)
    
        const select = ['users.*'];
        let result = await this.Repository.query()
        .select(select)
        .where('status', 1)
        .where('username', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
        .orWhere('fullname', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
        .orWhere('phone', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
        .orWhere('email', 'ILIKE', `%${data.keyword.replace(/%/, '\%')}%`)
        .limit(12)
        return result;
  }
}

module.exports = UserController