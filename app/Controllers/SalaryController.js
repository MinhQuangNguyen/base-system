const BaseController = require("./BaseController");
const SalaryRepository = require("@app/Repositories/SalaryRepository");
const SalaryDetailRepository = require("@app/Repositories/SalaryDetailRepository");
const UserRepository = require("@app/Repositories/UserRepository");
const OrderRepository = require("@app/Repositories/OrderRepository");
const OrderDetailRepository = require("@app/Repositories/OrderDetailRepository");
const ApiException = require("@app/Exceptions/ApiException");
const moment = require('moment');
const _ = require('lodash');
const { salaryStatus } = require("@config/constant")

class SalaryController extends BaseController {
    constructor() {
        super();
        this.Repository = new SalaryRepository();
        this.SalaryDetailRepository = new SalaryDetailRepository();
        this.UserRepository = new UserRepository();
        this.OrderRepository = new OrderRepository();
        this.OrderDetailRepository = new OrderDetailRepository();
    }

    /**
     * Lấy danh sách bảng lương
     */
    async index() {
        let auth = this.request.auth;
        let inputs = this.request.all();
        let salary = {}
        if (_.isEmpty(inputs)) {
            inputs = {
                companyId: auth.companyId,
                month: moment().format('YYYY-MM')
            }
        }
        else {
            inputs.companyId = auth.companyId
        }
        let checkExist = await this.Repository.query(auth.companyId).getOne({ month: inputs.month })
        if (!checkExist) {
            this.init(inputs.month)
        }
        salary = await this.SalaryDetailRepository.query()
            .select(['salaries.month as month', 'salary_details.*'])
            .join('salaries', 'salaries.id', 'salary_details.salaryId')
            .andWhere(inputs)
            .getForGridTable(inputs)
        return salary;
    }

    /**
     * Xem chi tiết bảng lương
     */
    async detail() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const partern = '^([0-9]{4})-(1[0-2]|0[1-9])?$';
        let match = inputs.month.match(partern);
        if (!match) {
            throw new ApiException(7003, "Month is not valid");
        }

        let checkExist = await this.Repository.query(auth.companyId).getOne({ month: inputs.month })
        if (!checkExist) {
            await this.init(inputs.month)
        }
        inputs.companyId = auth.companyId

        let salary = await this.SalaryDetailRepository.query()
            .select(['salaries.month as month', 'salary_details.*'])
            .join('salaries', 'salaries.id', 'salary_details.salaryId')
            .andWhere(inputs)
            .getForGridTable(inputs)
        return salary;
    }

    /**
     * Xóa bảng lương chưa khóa
     */
    async destroy() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        this.Repository.setCompanyId(auth.companyId);
        let salary = await this.Repository.query().getOne({ 'month': inputs.month });

        if (salary.status === salaryStatus.close) {
            throw new ApiException(7005, "Salary is closed!")
        }

        await this.Repository.query().delById(salary.id);
        return true;
    }

    /**
     * Lưu, cập nhật bảng lương
     */
    async update() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const allowFields = {
            id: "number!",
            status: "number",
            note: "string"
        }
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });

        this.Repository.setCompanyId(auth.companyId);
        let checkExist = await this.Repository.query().getById(data.id);
        if (!checkExist) {
            throw new ApiException(7004, "Salary is not exist!")
        }

        if (checkExist.status === salaryStatus.close) {
            throw new ApiException(7005, "Salary is closed!")
        }
        data.updatedBy = auth.id;
        data.updatedAt = new Date();

        let id = data.id;
        delete data.id;
        return await this.Repository.query().where('id', id).update(data);
    }

    /**
     * Lưu, cập nhật lương 1 Nhân viên
     */
    async updateDetail() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const allowFields = {
            id: "number!",
            salaryId: "number!",
            userId: "number",
            fullname: "string",
            username: "string",
            groupname: "string",
            salary: "number",
            salaryByDate: "number",
            extra: "number",
            fine: "number",
            bonus: "number",
            note: "string",
            workdays: "number"
        }
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });

        this.Repository.setCompanyId(auth.companyId);
        let checkExist = await this.Repository.query().getById(data.salaryId);
        if (!checkExist) {
            throw new ApiException(7004, "Salary is not exist!")
        }


        if (checkExist.status === salaryStatus.close) {
            throw new ApiException(7005, "Salary is closed!")
        }
        let id = data.id;
        delete data.id;
        // return id;
        return await this.SalaryDetailRepository.query().where('id', id).update(data);
    }

    /**
     * Nhập tháng cần tạo bảng lương, kiểm tra nếu chưa có bảng lương tháng đó thì khởi tạo bảng lương mới
     */
    async store() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const allowFields = {
            month: 'string!'
        }
        let data = this.validate(inputs, allowFields);

        const partern = '^([0-9]{4})-(1[0-2]|0[1-9])?$';
        let match = data.month.match(partern);
        if (!match) {
            throw new ApiException(7003, "Month is not valid!");
        }

        this.Repository.setCompanyId(auth.companyId);
        let checkExist = await this.Repository.query().getOne({ 'month': data.month });
        if (checkExist) {
            throw new ApiException(7002, "Salary is exist!");
        }

        return await this.init(data.month);
    }

    /**
     * Khởi tạo bảng lương tháng
     * @param {*} month 
     */
    async init(month) {
        let auth = this.request.auth;
        let date = moment(month);
        let data = {
            companyId: auth.companyId,
            month: month,
            updatedBy: auth.id
        }
        let salaries = await this.Repository.query(auth.companyId).insertOne(data);
        if (salaries) {
            const select = ['users.id as userId',
                'users.username as username',
                'users.fullname as fullname',
                'user_groups.name as groupname',
                'users.salary as salary'
            ]
            let users = await this.UserRepository.query(auth.companyId)
                .select(select)
                .join('user_groups', 'users.groupId', 'user_groups.id');

            let orderIds = await this.OrderRepository.query(auth.companyId)
                .whereBetween('createdAt', [date.clone().startOf('month'), date.clone().endOf('month')])
                .pluck('id');

            let orderDetails = await this.OrderDetailRepository.query()
                .whereIn('orderId', orderIds);

            let userDetails = _.groupBy(orderDetails, 'userId');

            for (let user of users) {
                user.salaryId = salaries.id;
                user.total = user.salary;
                if (userDetails[user.userId]) {
                    user.extra = _.sumBy(userDetails[user.userId], function (o) { return Number(o.userPay * o.quantity) });
                    user.total += Number(user.extra);
                }
            }
            await this.SalaryDetailRepository.query().insertMany(users);
        }
        return true;
    }
}

module.exports = SalaryController;