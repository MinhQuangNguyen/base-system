const BaseController = require("./BaseController");
const CategoryRepository = require("@app/Repositories/CategoryRepository");
const ApiException = require("@app/Exceptions/ApiException");
const ProductHistoryRepository = require("@app/Repositories/ProductHistoryRepository");
const _ = require('lodash');

class CategoryController extends BaseController {
    constructor() {
        super();
        this.Repository = new CategoryRepository();
        this.ProductHistoryRepository = new ProductHistoryRepository();
    }

    async store() {
        let auth = this.request.auth;
        let inputs = this.request.all();
        const allowFields = {
            name: "string!",
            description: "string",
            status: "number",
            parentId:"number"
        };
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });

        this.Repository.setCompanyId(auth.companyId);
        let checkExist = await this.Repository.query().getOne({'name': data.name});
        if (checkExist) {
            throw new ApiException(7002, "Category already exists!")
        }

        data.companyId = auth.companyId;
        data.updatedBy = auth.id;

        return await this.Repository.query().insertOne(data);
    }

    async update() {
        let auth = this.request.auth;
        let inputs = this.request.all();

        const allowFields = {
            name: "string",
            description: "string",
            status: "number",
            parentId:"number"
        };
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });

        this.Repository.setCompanyId(auth.companyId);
        let checkExist = await this.Repository.query().getById(inputs.id);
        if (!checkExist) {
            throw new ApiException(7004, "Category does not exist!")
        }
        
        if (data.name) {
            let checkDuplicate = await this.Repository.query().andWhere({ 'name': data.name }).andWhereNot('id', inputs.id);
            if (!_.isEmpty(checkDuplicate)) {
                throw new ApiException(7002, "Category name is exist!")
            }
        }

        data.updatedAt = new Date();
        data.updatedBy = auth.id;

        return await this.Repository.query().updateOne(inputs.id, data);;
    }

    async getTreeData() {
        let auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
        return await this.Repository.getTreeData()
    }

    async getByProductType() {
        const inputs = this.request.all()
        const allowFields = {
            type: "number!",
        };
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });
        const auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId)
        this.ProductHistoryRepository.setCompanyId(auth.companyId)
        const select = ['categories.*'];
        let result = await this.Repository.query()
            .join('products', {
                'products.categoryId': 'categories.id',
                'products.type': data.type
            })
            .groupBy('categories.id')
            .select(select)
        return result;
    }
}

module.exports = CategoryController;