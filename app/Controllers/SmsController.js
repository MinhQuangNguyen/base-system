const BaseController = require('./BaseController')
const UserRepository = require('@app/Repositories/UserRepository')
const ApiException = require('@app/Exceptions/ApiException')
const SmsService = require('@app/Services/Sms')

class SmsController extends BaseController {
  constructor() {
    super()
    this.UserRepository = new UserRepository()
  }

  async sendUser() {
    const auth = this.request.auth
    const inputs = this.request.all()

    const allowFields = {
      "type": "string!",
      "message": "string!",
      "userId": "number!"
    }

    const data = this.validate(inputs, allowFields)

    let user = this.UserRepository.query(auth.companyId).findById(data.userId)
    if (!user) {
      throw new ApiException(7004, "User does not exist!")
    }
    if (!user.phone) {
      throw new ApiException(7003, "User does not have phone!")
    }

    return await SmsService.send(data.type, data.message, user.phone)
  }

  async sendGroup() {

    return "Đã gửi tin nhắn"
  }
}

module.exports = SmsController