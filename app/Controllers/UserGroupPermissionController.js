const BaseController = require("./BaseController");
const UserGroupPermissionRepository = require("@app/Repositories/UserGroupPermissionRepository");
const UserPermissionRepository = require("@app/Repositories/UserPermissionRepository");
const ApiException = require("@app/Exceptions/ApiException");

class UserGroupPermissionController extends BaseController {
    constructor() {
        super()
        this.Repository = new UserGroupPermissionRepository();
        this.UserPermissionRepository = new UserPermissionRepository();
    }

    async update() {
        let inputs = this.request.all();
        let auth = this.request.auth;
        const { groupId, permissions } = inputs;

        if (!groupId) throw new ApiException(7002, "GroupId is required");
        if (!permissions) throw new ApiException(7002, "No data");

        //kiem tra auth co quyen voi groupId nay hay khong
        //do something....

        for (let permissionKey in permissions) {
            const value = permissions[permissionKey]
            const exist = await this.UserPermissionRepository.getByKey(permissionKey);
            const role = await this.Repository.getByPermissionKey({ permissionKey, groupId }); //chỗ này có thể tối ưu được.

            if (!exist) throw new ApiException(7003, `${permissionKey} doesn't exist`);

            //kiem tra gia tri moi cua quyen
            if (!value) { //truong hop xoa bo quyen cu
                await this.Repository.query().where({ groupId, permissionKey }).del();
            }
            else if (!role) { //quyen moi chua ton tai trong DB
                await this.Repository.query().insertOne({ permissionKey, groupId, value });
            }
            else if (role.value != value) { //update lai gia tri moi
                await this.Repository.query().updateOne(role.id, { value: value })
            }
        }
        return { message: `Update successfully` }
    }
}

module.exports = UserGroupPermissionController