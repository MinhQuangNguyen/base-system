const BaseController = require("./BaseController");
const UserGroupRepository = require("@app/Repositories/UserGroupRepository");
const ApiException = require("@app/Exceptions/ApiException");

class UserGroupController extends BaseController {
    constructor() {
        super()
        this.Repository = new UserGroupRepository();
    }

    async store() {
        const allowFields = {
            name: "string!",
            description: "string"
        }
        let inputs = this.request.all()
        let auth = this.request.auth

        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });
        this.Repository.setCompanyId(auth.companyId);

        let checkExist = await this.Repository.getByUserGroupName(data["name"])

        if (checkExist) {
            throw new ApiException(7001, "UserGroup already exists!")
        }

        return await this.Repository.query().insertOne({
            ...data,
            companyId: auth.companyId
        });
    }

    async update() {
        const allowFields = {
            name: "string!",
            description: "string"
        }
        let inputs = this.request.all();
        let data = this.validate(inputs, allowFields, {
            removeNotAllow: true
        });
        let auth = this.request.auth;
        let exist = await this.Repository.setCompanyId(auth.companyId).getById(inputs.id);
        if (!exist) {
            throw new ApiException(7002, "UserGroup does not exist!");
        }
        let existUserGroupName = await this.Repository.setCompanyId(auth.companyId).getByUserGroupName(data['name']);
        if (existUserGroupName && existUserGroupName.id !=inputs.id) {
            throw new ApiException(7001, "UserGroup_name already exists!");
        }
        return await this.Repository.query().updateOne(inputs.id, data);
    }

    async getPermissions(){
        let inputs = this.request.all();
        if (!inputs.id){
            throw new ApiException(7002, "GroupId is required");
        }
        let auth = this.request.auth;
        this.Repository.setCompanyId(auth.companyId);
        let groupId = inputs.id
        return this.Repository.getPermissions(groupId);
    }
}

module.exports = UserGroupController