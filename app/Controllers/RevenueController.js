const BaseController = require("./BaseController");
const RevenueRepository = require("@app/Repositories/RevenueRepository");
const OrderRepository = require("@app/Repositories/OrderRepository");
const OrderDetailRepository = require("@app/Repositories/OrderDetailRepository");
const CustomerRepository = require("@app/Repositories/CustomerRepository");
const OrderService = require("@app/Services/OrderService");
const UserRepository = require("@app/Repositories/UserRepository")
const moment = require('moment'); 
const _ = require('lodash');
const { orderStatus } = require("@config/constant")


const ApiException = require("@app/Exceptions/ApiException");

class RevenueController extends BaseController {
    constructor() {
        super();
        this.Repository = new RevenueRepository();
        this.OrderRepository = new OrderRepository();
        this.OrderDetailRepository = new OrderDetailRepository();
        this.CustomerRepository = new CustomerRepository();
        this.OrderService = new OrderService();
        this.UserRepository = new UserRepository();
    }

    
    /**
     * const day = '^([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])?$';
     * const month = '^([0-9]{4})-(1[0-2]|0[1-9])?$';
     * const year = '^([0-9]{4})?$';
     */
    async getByDate() {
        let auth = this.request.auth;
        let inputs = this.request.all();
        
        const partern = '^(([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]))$';
        let match = inputs.date.match(partern);
        if (!match) {
            throw new ApiException(7003, "Date is not valid");
        }

        let fromDate = moment(inputs.date).startOf('day');
        let toDate = fromDate.clone().add(1, 'days');

        this.OrderRepository.setCompanyId(auth.companyId);
        let results = await this.OrderRepository.getByDateRange(fromDate, toDate);

        return results;
    }

    async getByMonth() {
        let auth = this.request.auth;
        let inputs = this.request.all();
        
        const partern = '^([0-9]{4})-(1[0-2]|0[1-9])?$';
        let match = inputs.month.match(partern);
        if (!match) {
            throw new ApiException(7003, "Month is not valid");
        }
        inputs.month += '-01'
        let fromDate = moment(inputs.month);
        let toDate = fromDate.clone().add(1, 'months').endOf('day');

        this.OrderRepository.setCompanyId(auth.companyId);
        let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

        let days = (toDate.startOf('day') - fromDate) / (24 * 60 * 60 * 1000);
        let data = {}
        for (let i = 0; i < days; i++) {
            let day = fromDate.clone().add(i, 'days').format("YYYY-MM-DD");
            data[day] = {
                'date': day,
                'revenues': 0,
                'customers': {},
                'orders': 0,
                'products': 0,
                'services': 0
            }
        }

        for (let order of orders) {
            let createdAt = moment(order.createdAt).format("YYYY-MM-DD");
            if (data[createdAt]) {
                data[createdAt].orders++;
                data[createdAt].revenues += Number(order.amount);
                data[createdAt].services += Number(order.services);
                data[createdAt].products += Number(order.products);
                if (!data[createdAt].customers[order.customerId])
                    data[createdAt].customers[order.customerId] = true;
            }
        }

        for (let i in data) {
            data[i].customers = Object.keys(data[i].customers).length;
        }

        let results = Object.keys(data).map(function(key){return data[key]});
        return results;
    }

    async getByYear() {
        let auth = this.request.auth;
        let inputs = this.request.all();
        
        const partern = '^([0-9]{4})?$';
        let match = inputs.year.match(partern);
        if (!match) {
            throw new ApiException(7003, "Year is not valid");
        }
        inputs.year += '-01-01'
        let fromDate = moment(inputs.year).startOf('year');
        let toDate = fromDate.clone().endOf('year');

        this.OrderRepository.setCompanyId(auth.companyId);
        let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

        const months = 12;
        let data = {}
        for (let i = 0; i < months; i++) {
            let month = fromDate.clone().add(i, 'months').format("YYYY-MM");
            data[month] = {
                'date': month,
                'revenues': 0,
                'customers': {},
                'orders': 0,
                'products': 0,
                'services': 0
            }
        }

        for (let order of orders) {
            let createdAt = moment(order.createdAt).format("YYYY-MM");
            if (data[createdAt]) {
                data[createdAt].orders++;
                data[createdAt].revenues += Number(order.amount);
                data[createdAt].services += Number(order.services);
                data[createdAt].products += Number(order.products);
                if (!data[createdAt].customers[order.customerId])
                    data[createdAt].customers[order.customerId] = true;
            }
        }

        for (let i in data) {
            data[i].customers = Object.keys(data[i].customers).length;
        }

        let results = Object.keys(data).map(function(key){return data[key]});
        return results;
    }

    async index() {
        const auth = this.request.auth;
        
        let order = await this.OrderRepository.query(auth.companyId).orderBy('createdAt', 'asc').limit(1);
        let beginDate = moment(order[0].createdAt).startOf('month');
        let toDate = moment().startOf('month');
        let months = {};

        while (toDate.isSameOrAfter(beginDate)) {
            let month = toDate.format('YYYY-MM');
            months[month] = {
                month: month,
                status: 0
            };
            toDate.subtract(1, 'months')
        }
        let revenues = await this.Repository.query(auth.companyId).whereIn('month', Object.keys(months))
        if (revenues) {
            for (let revenue of revenues) {
                revenue.status = 1
                months[revenue.month] = revenue
            }
        }
        return Object.keys(months).map(function(key){return months[key]});
    }

    async closeMonth() {
        const auth = this.request.auth;
        const inputs = this.request.all();

        const partern = '^([0-9]{4})-(1[0-2]|0[1-9])?$';
        let match = inputs.month.match(partern);
        if (!match) {
            throw new ApiException(7003, "Month is not valid");
        }

        let checkExist = await this.Repository.query(auth.companyId).getOne({ 'month': inputs.month })
        if (checkExist) {
            await this.Repository.query(auth.companyId).delById(checkExist.id)
            return {
                month: inputs.month,
                status: 0
            }
        }

        let monthReport = await this.getByMonth({ month: inputs.month })
        let results = {}
        results.month = inputs.month
        results.companyId = auth.companyId
        results.revenue = _.sumBy(monthReport, 'revenues')
        results.orders = _.sumBy(monthReport, 'orders')
        results.products = _.sumBy(monthReport, 'products')
        results.services = _.sumBy(monthReport, 'services')
        results.createdBy = auth.id

        let revenues = await this.Repository.query(auth.companyId).insertOne(results);
        revenues.status = 1;
        await this.closeOrder(inputs.month)
        return revenues;
    }

    async closeOrder(month) {
        let toDate = moment(month).startOf('month')
        let fromDate = moment(month).endOf('month')
        
        return await this.OrderRepository.query().whereBetween('createdAt', [toDate, fromDate]).update({status: orderStatus.close});
    }

    async openOrder(month) {
        let toDate = moment(month).startOf('month')
        let fromDate = moment(month).endOf('month')
        
        return await this.OrderRepository.query().whereBetween('createdAt', [toDate, fromDate]).update({status: orderStatus.open});
    }
}

module.exports = RevenueController;