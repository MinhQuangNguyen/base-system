const BaseController = require("./BaseController")
const PermissionCategoryRepository = require("@app/Repositories/PermissionCategoryRepository")
const ApiException = require("@app/Exceptions/ApiException");
const _ = require('lodash')

class PermissionCategoryController extends BaseController {
  constructor() {
    super()
    this.Repository = new PermissionCategoryRepository();
  }


  async store() {
    let inputs = this.request.all()

    const allowFields = {
        name: "string!",
        description: "string",
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    });
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId);

    let checkExist = await this.Repository.getByName(data['name']);

    if (checkExist) {
      throw new ApiException(7001, "Permission category already exists!");
    }
    data['companyId'] = auth.companyId;
    let result = await this.Repository.query().insertOne(data);
    return result
  }

  async update(id, input) {
    const auth = this.request.auth;
    this.Repository.setCompanyId(auth.companyId)
    let inputs = this.request.all()
    const allowFields = {
        name: "string!",
        description: "string",
    }
    let data = this.validate(inputs, allowFields, {
      removeNotAllow: true
    })
    
    let exist = await this.Repository.getById(inputs.id)

    if (!exist) {
      throw new ApiException(7002, "Permission category does not exist!")
    }

    let existName = await this.Repository.getByName(data['name']);
    if (existName && existName.id != inputs.id) {
      throw new ApiException(7001, "Permission category name already exists!");
    }
    data["companyId"] = auth.companyId;
    let result = await this.Repository.query().updateOne(inputs.id, data);
    return result;
  }
   
}

module.exports = PermissionCategoryController