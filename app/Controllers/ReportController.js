const BaseController = require("@app/Controllers/BaseController")
const OrderRepository = require("@app/Repositories/OrderRepository")
const OrderDetailRepository = require("@app/Repositories/OrderDetailRepository")
const CompanyRepository = require("@app/Repositories/CompanyRepository")
const ApiException = require("@app/Exceptions/ApiException")
const { chartTypes } = require("@config/constant")
const moment = require('moment')
const _ = require('lodash')
class ReportController extends BaseController {
  constructor() {
    super()
    this.OrderRepository = new OrderRepository();
    this.OrderDetailRepository = new OrderDetailRepository();
    this.CompanyRepository = new CompanyRepository();
  }

  /**
   * List year
   */
  async getYear() {
    let auth = this.request.auth;
    let company = await this.CompanyRepository.getById(auth.companyId);
    if (!company) {
      throw new ApiException(7004, "Company does not exist!")
    }
    let fromYear = moment(company.createdAt).startOf('year')
    let toYear = moment("2025-05-03").startOf('year')

    let years = {}
    while (toYear.isSameOrAfter(fromYear)) {
      let year = toYear.format('YYYY');
      years[year] = {
        year: year
      }
      toYear.subtract(1, 'years')
    }
    let sort = _.orderBy(years, 'year', 'desc')
    return _.map(sort, 'year');
  }

  /**
   * Timeline:
   * - Revenue : doanh thu
   * - Order : 
   * - Customer
   */
  async getTimeline() {

  }


  /**
   * Biểu đồ doanh thu
   */
  async revenue() {
    const inputs = this.request.all();

    const allowFields = {
      type: "string!",
      value: "string!"
    }

    let params = this.validate(inputs, allowFields)

    let data = {}
    if (params.type === chartTypes.month) {
      data = await this.revenueByMonth(params.value)
    }
    if (params.type === chartTypes.year) {
      data = await this.revenueByYear(params.value)
    }
    return data
  }

  async revenueByMonth(month) {
    const auth = this.request.auth;
    let date = month + '-01'
    let fromDate = moment(date).startOf('month');
    let toDate = moment(date).endOf('month');

    this.OrderRepository.setCompanyId(auth.companyId)
    let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

    let data = this.initData(chartTypes.month, month)

    for (let order of orders) {
      let createdAt = moment(order.createdAt).format("YYYY-MM-DD");
      data[createdAt] += Number(order.amount);
    }
    return data
  }

  async revenueByYear(year) {
    const auth = this.request.auth;
    let date = year + '-01-01'
    let fromDate = moment(date).startOf('year');
    let toDate = moment(date).endOf('year');

    this.OrderRepository.setCompanyId(auth.companyId)
    let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

    let data = this.initData(chartTypes.year, year)

    for (let order of orders) {
      let createdAt = moment(order.createdAt).format("YYYY-MM");
      data[createdAt] += Number(order.amount);
    }
    return data
  }

  /**
   * Biểu đồ theo hóa đơn
   */
  async order() {
    const inputs = this.request.all();

    const allowFields = {
      type: "string!",
      value: "string!"
    }

    let params = this.validate(inputs, allowFields)

    let data = {}
    if (params.type === 'month') {
      data = await this.orderByMonth(params.value)
    }
    if (params.type === 'year') {
      data = await this.orderByYear(params.value)
    }
    return data
  }

  async orderByMonth(month) {
    const auth = this.request.auth;
    let date = month + '-01'
    let fromDate = moment(date).startOf('month');
    let toDate = moment(date).endOf('month');

    this.OrderRepository.setCompanyId(auth.companyId)
    let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

    let data = this.initData(chartTypes.month, month)

    for (let order of orders) {
      let createdAt = moment(order.createdAt).format("YYYY-MM-DD");
      data[createdAt]++;
    }
    return data
  }

  async orderByYear(year) {
    const auth = this.request.auth;
    let date = year + '-01-01'
    let fromDate = moment(date).startOf('year');
    let toDate = moment(date).endOf('year');

    this.OrderRepository.setCompanyId(auth.companyId)
    let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

    let data = this.initData(chartTypes.year, year)

    for (let order of orders) {
      let createdAt = moment(order.createdAt).format("YYYY-MM");
      data[createdAt]++;
    }
    return data
  }

  /**
   * Biểu đồ theo khách hàng
   */
  async customer() {
    const inputs = this.request.all();

    const allowFields = {
      type: "string!",
      value: "string!"
    }

    let params = this.validate(inputs, allowFields)

    let data = {}
    if (params.type === 'month') {
      data = await this.customerByMonth(params.value)
    }
    if (params.type === 'year') {
      data = await this.customerByYear(params.value)
    }
    return data
  }

  async customerByMonth(month) {
    const auth = this.request.auth
    let date = month + '-01'
    let fromDate = moment(date).startOf('month');
    let toDate = moment(date).endOf('month');

    this.OrderRepository.setCompanyId(auth.companyId)
    let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

    let data = this.initData(chartTypes.month, month)

    let customers = {}
    for (let order of orders) {
      let createdAt = moment(order.createdAt).format("YYYY-MM-DD");
      if (!customers[createdAt]) {
        customers[createdAt] = {}
      }
      customers[createdAt][order.customerId] = true;
    }
    for (let key in customers) {
      data[key] = Object.keys(customers[key]).length;
    }
    return data
  }

  async customerByYear(year) {
    const auth = this.request.auth
    let date = year + '-01-01'
    let fromDate = moment(date).startOf('year');
    let toDate = moment(date).endOf('year');

    this.OrderRepository.setCompanyId(auth.companyId)
    let orders = await this.OrderRepository.getByDateRange(fromDate, toDate);

    let data = this.initData(chartTypes.year, year)

    let customers = {}
    for (let order of orders) {
      let createdAt = moment(order.createdAt).format("YYYY-MM");
      if (!customers[createdAt]) {
        customers[createdAt] = {}
      }
      customers[createdAt][order.customerId] = true;
    }
    for (let key in customers) {
      data[key] = Object.keys(customers[key]).length;
    }
    return data
  }

  /**
   * 
   * @param {string} type 
   */
  initData(type, value) {
    let data = {}
    if (type === chartTypes.day) {

    }
    if (type === chartTypes.month) {
      let fromDate = moment(value).startOf('month')
      let toDate = fromDate.clone().add(1, 'months').startOf('day')
      let days = (toDate.startOf('day') - fromDate) / (24 * 60 * 60 * 1000);
      for (let i = 0; i < days; i++) {
        let day = fromDate.clone().add(i, 'days').format("YYYY-MM-DD");
        data[day] = 0
      }
    }
    if (type === chartTypes.year) {
      let date = moment(value + '-01-01').startOf('year')
      const months = 12;
      for (let i = 0; i < months; i++) {
        let month = date.clone().add(i, 'months').format("YYYY-MM");
        data[month] = 0
      }
    }

    return data
  }
}

module.exports = ReportController