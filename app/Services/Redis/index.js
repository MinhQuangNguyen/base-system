const redisLib = require('redis');

const redis = redisLib.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS,
    db: process.env.REDIS_DB
});
redis.on("error", function (err) {
    console.log("Redis error encountered", err);
});

class RedisService {
    static async set(key, value, options = {}) {
        let expire = options.expire || undefined
        if (expire) {
            redis.set(key, JSON.stringify(value), 'EX', expire)
        }
        else {
            redis.set(key, JSON.stringify(value))
        }
    }

    static async get(key) {
        return await new Promise((resolve, reject) => {
            redis.get(key, function (err, value) {
                if (err) {
                    return reject(err)
                }
                return resolve(value ? JSON.parse(value) : value)
            })
        })
    }

    static async del(key) {
        return await new Promise((resolve, reject) => {
            redis.del(key, function (err, response) {
                if (err) {
                    return reject(err)
                }
                return resolve()
            })
        })
    }

    static async keys(keys) {
        return await new Promise((resolve, reject) => {
            redis.keys(keys, function (err, response) {
                if (err) {
                    return reject(err)
                }
                return resolve(response)
            })
        })
    }
}

module.exports = RedisService;
