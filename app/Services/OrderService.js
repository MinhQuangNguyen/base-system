const {productTypes, discountTypes} = require('@config/constant')

class OrderService {
    constructor() { }
    async getOrderTotal(data, orderDetails) {
        let results = {
            amount: 0,
            services: 0,
            products: 0
        }
        if (orderDetails) {
            for (let detail of orderDetails) {
                let discount;
                switch (detail.discountType) {
                    case discountTypes.amount:
                        discount = detail.discount;
                        break;
                    case discountTypes.percent:
                        discount = detail.price * detail.discount / 100;
                        break;
                    default:
                        discount = 0;
                }
                if (detail.price > discount)
                    results.amount += Number((detail.price - discount) * detail.quantity);
    
                if (detail.productType === productTypes.service) {
                    results.services += Number(detail.quantity);
                }
                else {
                    results.products += Number(detail.quantity);
                }
            }
        }
        
        if (data.discount > 0) {
            let orderDiscount;
            switch (data.discountType) {
                case discountTypes.amount:
                    orderDiscount = data.discount;
                    break;
                case discountTypes.percent:
                    orderDiscount = results.amount * data.discount / 100;
                    break;
                default:
                    orderDiscount = 0;
            }
            if (results.amount > orderDiscount)
                results.amount -= Number(orderDiscount);
        }

        return results;
    }

    async getProductTotal(detail) {
        let results = {
            amount: 0,
            services: 0,
            products: 0
        }
        let discount;
        switch (detail.discountType) {
            case discountTypes.amount:
                discount = detail.discount;
                break;
            case discountTypes.percent:
                discount = detail.price * detail.discount / 100;
                break;
            default:
                discount = 0;
        }
        results.amount = Number((detail.price - discount) * detail.quantity);
        
        if (detail.productType === productTypes.service) {
            results.services = Number(detail.quantity);
        }
        else {
            results.products = Number(detail.quantity);
        }

        return results;
    }

    async getCurrentOrderNo(date, temp, order) {
        let orderNo
        if (order) {
            if (temp) {
                if (order > temp) {
                    orderNo = Number(order.replace(/^(\d{4})/, '')) + 1
                }
                else {
                    orderNo = Number(temp.replace(/^(\d{4})/, '')) + 1
                }
            }
            else {
                orderNo = Number(order.replace(/^(\d{4})/, '')) + 1
            }
        }
        else {
            if (temp) {
                orderNo = Number(temp.replace(/^(\d{4})/, '')) + 1
            }
            else {
                orderNo = "01"
            }
        }
        return date + "" + orderNo.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
    }
}
module.exports = OrderService;