const ZaloSms = require('./ZaloSms')

class SmsService {
  constructor() {
    this.ZaloSms = ZaloSms
  }
  
  static async send(type, message, phone) {
    return await ZaloSms.sentMessageToPhone(phone, message);
  }

  static async getPhoneInfo(phone) {
    return await ZaloSms.phoneToUser(phone)
  }
}

module.exports = SmsService