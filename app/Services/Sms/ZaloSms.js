const Zalo = require('@ngochipx/zalo')

let ZAccount = new Zalo();
ZAccount.init({
    username: "0987626654",
    password: "kzy2kjk1",
    tmpdir: process.cwd() + "/tmp"
});

class ZaloSms {
  static async phoneToUser(phone) {
    return await ZAccount.phoneToUser(phone, false); //cache default: true
  }

  static async sentMessageToPhone(phone, message) {
    return await ZAccount.sentMessageToPhone(phone, message);
  }

  static async getGroupInfo(groupUrl) {
    return await ZAccount.getGroupInfo(groupUrl, false);
  }

  static async joinGroup(groupUrl) {
    return await ZAccount.joinGroup(groupUrl);
  }

  static async sentMessageToGroup(groupUrl, message) {
    let group = await ZAccount.getGroupInfo(groupUrl);
    return await ZAccount.sentMessageToGroup(group.groupId, message);
  }
}
module.exports = ZaloSms