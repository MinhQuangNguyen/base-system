const BaseModel = require("./BaseModel");
const TABLE_NAME = "companies";

class Company extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = Company;