const BaseModel = require("./BaseModel");
const TABLE_NAME = "order_details";

class OrderDetail extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = OrderDetail;