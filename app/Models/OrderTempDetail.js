const BaseModel = require("./BaseModel");
const TABLE_NAME = "order_temp_details";

class OrderTempDetail extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = OrderTempDetail;