const BaseModel = require("./BaseModel");
const TABLE_NAME = "salary_details";

class SalaryDetail extends BaseModel {
    constructor() {
        super(TABLE_NAME)
    }
    static get relationship() {
        return {}
    }
}

module.exports = SalaryDetail;