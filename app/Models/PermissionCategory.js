const BaseModel = require("./BaseModel");
const TABLE_NAME = "permission_categories";

class PermissionCategory extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }

    get allowFields() {
        return [
            'name',
            'description'
        ]
      }
}

module.exports = PermissionCategory;