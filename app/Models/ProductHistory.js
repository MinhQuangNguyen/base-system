const BaseModel = require("./BaseModel");
const TABLE_NAME = "product_histories";

class ProductHistory extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = ProductHistory;