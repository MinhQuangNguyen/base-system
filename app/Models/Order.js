const BaseModel = require("./BaseModel");
const TABLE_NAME = "orders";

class Order extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = Order;