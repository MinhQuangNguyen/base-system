const BaseModel = require("./BaseModel");
const TABLE_NAME = "user_permissions";

class UserPermission extends BaseModel {
  constructor() {
    super(TABLE_NAME);
  }

  static get relationship() {
    return {}
  }

  get softDelete() {
    return true;
  }

  get allowFields() {
    return [
      'id',
      'name',
      'description',
      'categoryId'
    ]
  }

}

module.exports = UserPermission;
