const BaseModel = require("./BaseModel");
const bcrypt = require("bcrypt")
const authConfig = require("@config/auth")
const TABLE_NAME = "users";

class User extends BaseModel {
  constructor() {
    super(TABLE_NAME);
  }

  static get relationship() {
    return {}
  }

  get softDelete() {
    return true;
  }

  get allowFields() {
    return [
      'id',
      'fullname',
      'email',
      'companyId',
      'username',
      'groupId',
      'phone',
      'status',
      'salary',
      'address',
      'createdAt',
      'updatedAt'
    ]
  }

  async hash(plainPassword){
    return await bcrypt.hash(plainPassword + authConfig.SECRET_KEY, 10)
  }
  async compare(plainPassword, encryptedPassword){
    return await bcrypt.compare(plainPassword + authConfig.SECRET_KEY, encryptedPassword)
  }
  async changePassword(id, newPassword){
    newPassword = await this.hash(newPassword)
    return await this.query().updateOne(id,{
      password: newPassword
    })
  }

}

module.exports = User;
