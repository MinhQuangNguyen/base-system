const BaseModel = require("./BaseModel");
const TABLE_NAME = "salaries";

class Salary extends BaseModel {
    constructor() {
        super(TABLE_NAME)
    }
    static get relationship() {
        return {}
    }
}

module.exports = Salary;