const BaseModel = require("./BaseModel");
const TABLE_NAME = "service_schedules";

class Service extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }

    get allowFields() {
        return [
            'duration',
            'content',
            'productId'
        ]
      }
}

module.exports = Service;