const BaseModel = require("./BaseModel");
const TABLE_NAME = "user_group_permissions";

class UserGroupPermission extends BaseModel {
  constructor() {
    super(TABLE_NAME);
  }

  static get relationship() {
    return {}
  }

  get softDelete() {
    return true;
  }

  get allowFields() {
    return [
      'id',
      'name',
      'description',
    ]
  }

}

module.exports = UserGroupPermission;
