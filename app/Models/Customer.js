const BaseModel = require("./BaseModel");
const TABLE_NAME = "customers";

class Customer extends BaseModel {
  constructor() {
    super(TABLE_NAME);
  }

  static get relationship() {
    return {}
  }

  get softDelete() {
    return true;
  }

  get allowFields() {
    return [
      'fullname',
        'phone',
        'address',
        'note',
        'dob',
        'email',
    ]
  }

}

module.exports = Customer;
