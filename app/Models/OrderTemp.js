const BaseModel = require("./BaseModel");
const TABLE_NAME = "order_temps";

class OrderTemp extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = OrderTemp;