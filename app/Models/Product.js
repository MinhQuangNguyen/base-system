const BaseModel = require("./BaseModel");
const TABLE_NAME = "products";

class Product extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }

    get allowFields() {
        return [
            'name',
            'code',
            'categoryId',
            'description',
            'quality',
            'price',
            'discount',
            'percent',
            'status'
        ]
      }
}

module.exports = Product;