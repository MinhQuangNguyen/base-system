const BaseModel = require("./BaseModel");
const TABLE_NAME = "user_groups";

class UserGroup extends BaseModel {
  constructor() {
    super(TABLE_NAME);
  }

  static get relationship() {
    return {}
  }

  get softDelete() {
    return true;
  }

  get allowFields() {
    return [
      'id',
      'name',
      'description',
    ]
  }

}

module.exports = UserGroup;
