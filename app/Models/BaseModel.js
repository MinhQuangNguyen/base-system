const KnexQueryBuilder = require('knex/lib/query/builder');
const Database = require('@core/Database');

class BaseModel{
  constructor(tableName){
    this.tableName = tableName
    this.connection = Database.connection;
  }
  query(){
    return this.connection(this.tableName)
  }
}

module.exports = BaseModel