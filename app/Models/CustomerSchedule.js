const BaseModel = require("./BaseModel");
const TABLE_NAME = "customer_schedules";

class CustomerSchedule extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = CustomerSchedule;