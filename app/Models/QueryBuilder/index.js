const extendClasses = [
    require('./ExtendQuery'),
    require('./GetForGridTable')
]

module.exports = (knex) => {
    return extendClasses.map(extendClass => {
        extendClass.init(knex)
    })
 }