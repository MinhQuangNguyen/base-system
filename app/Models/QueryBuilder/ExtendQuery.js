const Base = require('./Base')
const moment = require('moment')
const DatabaseException = require('@app/Exceptions/DatabaseException')
const debug = require('debug')('mqmodel:getForGridTable')
class ExtendQuery extends Base{
  static get publicMethod(){
    return [
      'getById',
      'getOne',
      'getAll',
      'getByCondition',
      'insertOne',
      'insertMany',
      'updateOne',
      'updateByCondition',
      'delById',
      'delByIds',
      'delByCondition'
    ]
  }
  async getById(id, project ='*'){
    return await this.getOne({id: id}, project)
  }
  // project: object fields ['id', 'createdAt', ...], get all: '*'
  async getOne(condition, project = '*') {
    let [error, result] = await to(this.builder.where(condition).first(project));
    if (error) throw new DatabaseException(error);

    return result;
  }

  async getAll(condition, project = '*') {
    return await this.getByCondition(condition, project)
  }
  // conditionL object {id: 3}
  // project: object fields ['id', 'createdAt', ...], get all: '*'
  async getByCondition(condition = {}, project = '*') {
    let [error, result] = await to(this.builder.where(condition).select(project));
    if (error) throw new DatabaseException(error);
    return result;
  }
  async insertOne(object, cb = false) {
    let [error, result] = await to(this.builder.clone().insert(object).returning('*'));
    if (error) throw new DatabaseException(error);
    [result] = result
    return result;
  }

  async insertMany(array) {
    let [error, result] = await to(this.builder.insert(array).returning('*'));
    if (error) throw new DatabaseException(error);

    return result;
  }

  async updateOne(id, fields) {
    delete fields['id']; //xóa fields _id nếu có trong data update
    fields = {
      ...fields,
      updatedAt: new Date(),
    };
    let builderUpdate = this.builder.clone()
    let builderSelect = this.builder.clone()
    let [err, result] = await to(this.setBuilder(builderUpdate).updateByCondition({id: id}, fields));
    if (err) throw new DatabaseException(err);

    return this.setBuilder(builderSelect).getById(id);
  }

  // Tìm và update theo điều kiện condition
  // Trả về 1 object WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
  async updateByCondition(condition, params = {}) {
    let [error, result] = await to(this.builder.where(condition).update(params));
    if (error) throw new DatabaseException(error);
    return result;
  }

  // id: id của document cần xóa
  async delById(id) {
    let [error, result] = await to(this.builder.where('id', id).del());
    if (error) throw new DatabaseException(error);
    return result;
  }

   // ids: ids của documents cần xóa
   async delByIds(ids = []) {
    let [error, result] = await to(this.builder.whereIn('id', ids).del());
    if (error) throw new DatabaseException(error);
    return result;
  }

  // Nếu ko có condition hoặc condition rỗng thì sẽ tương đương với xóa hết, nên bắt buộc phải có condition để tránh rủi ro
  async delByCondition(condition) {
    if (!condition || typeof condition !== "object" || Object.keys(condition).length === 0) {
      throw new DatabaseException('delete condition is required, and must be an object');
    }
    let [error, result] = await to(this.builder.where(condition).del());
    if (error) throw new DatabaseException(error);
    return result
  }

}

module.exports = ExtendQuery