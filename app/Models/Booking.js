const BaseModel = require("./BaseModel");
const TABLE_NAME = "bookings";

class Booking extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }
}

module.exports = Booking;