const BaseModel = require("./BaseModel");
const TABLE_NAME = "warehouse_bills";

class WareHouseBills extends  BaseModel {
    constructor() {
        super(TABLE_NAME);
    }

    static get relationship() {
        return {}
    }

    get allowFields() {
        return [
            'type',
            'code',
            'date',
            'note',
            'total',
            'cash'
        ]
      }
}

module.exports = WareHouseBills;