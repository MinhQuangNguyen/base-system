const BaseRepository = require("./BaseRepository")
const UserPermissionModel = require("@app/Models/UserPermission")
class UserPermissionRepository extends BaseRepository {
    constructor() {
        super();
        this.Model = new UserPermissionModel();
    }

    async getById(id) {
        return await this.query().getOne({ id })
    }

    async getByKey(key) {
        return await this.query().getOne({ key })
    }

    async getByGroupId(groupId) {
        let permissionArray = await this.query()
            .join('permission_categories', 'permission_categories.id', 'user_permissions.categoryId')
            .select(
                'user_permissions.key',
                'user_permissions.name',
                'user_permissions.description',
                'user_permissions.avalibleValue',
                'permission_categories.name as categoriesName',
                'permission_categories.id as categoriesId',
                'permission_categories.description as categoriesDescription')
            .orderBy(['permission_categories.sort','user_permissions.sort'])


        let groupPermissionArray = await this.query()
            .where({ groupId: groupId })
            .join('permission_categories', 'permission_categories.id', 'user_permissions.categoryId')
            .leftJoin('user_group_permissions', 'user_permissions.key', 'user_group_permissions.permissionKey')
            .select(
                'user_permissions.key',
                'user_permissions.name',
                'user_permissions.description',
                'user_permissions.avalibleValue',
                'user_group_permissions.value',
                'permission_categories.id as categoriesId')

        let permissionObject = {}
        let groupPermissionObject = {}
        let permissions = {}
        permissionArray.map(permission => permissionObject[permission.key] = permission);
        groupPermissionArray.map(permission => groupPermissionObject[permission.key] = permission);
        permissions = Object.values({
            ...permissionObject,
            ...groupPermissionObject
        })

        let result = {}
        for (let permission of permissions) {
            const { categoriesId, categoriesName, categoriesDescription, ...others } = permission
            if (!result[categoriesId]) {
                result[categoriesId] = {
                    id: categoriesId,
                    name: categoriesName,
                    description: categoriesDescription,
                    "permissions": []
                }
            }
            result[categoriesId]["permissions"].push({
                ...others,
                value: others.value || 0
            })

        }
        return Object.values(result)
    }
}

module.exports = UserPermissionRepository