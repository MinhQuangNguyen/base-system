const BaseRepository = require("./BaseByCompanyRepository")
const OrderModel = require("@app/Models/Order")
class OrderRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new OrderModel();
  }
  async getById(id){
    return await this.query().getOne({id})
  }
  async getByDateRange(from, to) {
    return await this.query()
      .select([
        'orders.id as orderId',
        'orders.code as orderCode',
        'orders.customerId',
        'customers.fullname as customerName',
        'orders.amount',
        'orders.services',
        'orders.products',
        'orders.total',
        'orders.createdAt'
      ])
      .leftJoin('customers', {
        'orders.customerId': 'customers.id'
      })
      .whereBetween('orders.createdAt', [from, to]);
  }
}

module.exports = OrderRepository