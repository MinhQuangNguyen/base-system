const BaseRepository = require("./BaseByCompanyRepository");
const ProductHistoryModel = require("@app/Models/ProductHistory");

class ProductHistoryRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new ProductHistoryModel();
  }

  async createHistory(data) {
    return await this.query().insertOne(data);
  }

  
}


module.exports = ProductHistoryRepository