const BaseRepository = require("./BaseByCompanyRepository");
const SalaryModel = require("@app/Models/Salary");

class SalaryRepository extends BaseRepository {
    constructor() {
        super();
        this.Model = new SalaryModel();
    }

    // async getByDateRange(from, to) {
    //     return await this.query()
    //         .whereBetween('createdAt', [from, to]);
    // }

    // async getByMonth(month) {
    //     return await this.query()
    //         .where({ 'month': month });
    // }
}

module.exports = SalaryRepository;