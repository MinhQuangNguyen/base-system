const BaseRepository = require("./BaseRepository")
const OrderDetailModel = require("@app/Models/OrderDetail")
class OrderDetailRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new OrderDetailModel();
  }
  async getById(id){
    return await this.query().getOne({id})
  }

  async insertMany(orderId, orderDetails) {
    for (let orderDetail of orderDetails) {
      orderDetail.orderId = orderId;
    }
    await this.query().insertMany(orderDetails);
    return await this.query().getAll({ 'orderId': orderId });
  }

  async updateMany(orderId, orderDetails) {
    let ids = await this.query().pluck('id').where({'orderId': orderId});

    for (let orderDetail of orderDetails) {
      orderDetail.orderId = orderId;
      if (orderDetail.id) {
        let index = ids.indexOf(orderDetail.id);
        if (index !== -1) {
          ids.splice(index, 1);
          await this.query().updateOne(orderDetail.id, orderDetail);
        }
      }
      else {
        await this.query().insertOne(orderDetail)
      }
    }
    if (ids) {
      await this.query().delByIds(ids);
    }
    return await this.query().getAll({ 'orderId': orderId });
  }
}

module.exports = OrderDetailRepository