const BaseRepository = require("./BaseRepository");
const CompanyModel = require("@app/Models/Company");

class CompanyRepository extends BaseRepository {
    constructor() {
        super();
        this.Model = new CompanyModel();
    }

    async getByIds(ids) {
        return await this.query().whereIn('id', ids);
    }

    async getByName(name) {
        return await this.query().getOne({ name: name });
    }

}

module.exports = CompanyRepository;