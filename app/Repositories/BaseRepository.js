class BaseRepository{

  async getById(id, select = '*'){
    return await this.Model.query().getById(id);
  }
  async delByIds(ids = []){
    return await this.Model.query().delByIds(ids);
  }
  async getAll(condition = {}, project = '*') {
    return await this.Model.query().getAll(condition, project)
  }
  async getByCondition(conditions, select = '*'){

  }

  query(){
    return this.Model.query();
  }
}

module.exports = BaseRepository