const BaseRepository = require("./BaseByCompanyRepository");
const ProductModel = require("@app/Models/Product");
const ProductHistoryModel = require("@app/Models/ProductHistory");
const ApiException = require("@app/Exceptions/ApiException");

class ProductRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new ProductModel();
    this.ProductHistoryModel = new ProductHistoryModel();
  }

  async getById(id){
    return await this.query().getOne({id})
  }

  async getByCode(code) {
    return await this.query().getOne({code: code});
  }

  async getByName(name) {
    return await this.query().getOne({name: name});
  }
  async updateQuantity({id,newQuantity,type,price}) {
    if (!id || !newQuantity) {
      throw new ApiException(7003, "Wrong input data")
    }
    let exist = await this.getById(id)

    if (!exist) {
        throw new ApiException(7004, "Product does not exist!")
    }
    let quantity = exist.quantity
    let updateQuantity = null
    if (type == 0) {
      if (quantity == newQuantity) 
          throw new ApiException(7002, "Data exist!")
      price = exist.price
      updateQuantity = await this.query().updateOne(id, {
          quantity: newQuantity
      });
    }
    else if (type == 1) {
      updateQuantity = await this.query().updateOne(id, {
        quantity: newQuantity + quantity,
        importPrice: price
      });
      newQuantity = newQuantity + quantity
    }
    else {
      updateQuantity = await this.query().updateOne(id, {
        quantity: quantity - newQuantity
      });
      newQuantity = quantity - newQuantity
    }
    
    let product_history = await this.ProductHistoryModel.query().insertOne({
        productId: id,
        type: type,
        price: price,
        quantity: quantity,
        newQuantity: newQuantity
    }); 
    if (updateQuantity && product_history)
        return {message:"update success"}
    }
}

module.exports = ProductRepository