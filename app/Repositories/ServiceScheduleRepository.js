const BaseRepository = require("./BaseRepository")
const ServiceScheduleModel = require("@app/Models/ServiceSchedule")
class ServiceScheduleRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new ServiceScheduleModel();
  }

  async getSchedule(ids) {
    return await this.query()
      .select('id as scheduleId',
          'duration',
      )
      .whereIn('productId', ids)
    ;
  }
}

module.exports = ServiceScheduleRepository