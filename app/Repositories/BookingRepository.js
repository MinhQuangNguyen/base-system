const BaseRepository = require("./BaseByCompanyRepository");
const BookingModel = require("@app/Models/Booking");

class BookingRepository extends BaseRepository {
    constructor(){
        super();
        this.Model = new BookingModel();
    }
    async getById(id){
        return await this.query().getOne({id})
    }
}

module.exports = BookingRepository;