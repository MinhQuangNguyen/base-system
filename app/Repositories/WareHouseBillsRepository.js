const BaseRepository = require("./BaseByCompanyRepository")
const WareHouseBillsModel = require("@app/Models/WareHouseBills")

class WareHouseBillsRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new WareHouseBillsModel();
  }
  async getByCode(code){
    return await this.query().getOne({code})
  }
  
}

module.exports = WareHouseBillsRepository