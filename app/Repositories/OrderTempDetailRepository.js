const BaseRepository = require("./BaseRepository")
const OrderTempDetailModel = require("@app/Models/OrderTempDetail")
class OrderTempDetailRepository extends BaseRepository {
  constructor() {
    super();
    this.Model = new OrderTempDetailModel();
  }
  async getById(id) {
    return await this.query().getOne({ id })
  }

  async insertMany(orderTempId, orderDetails) {
    for (let orderDetail of orderDetails) {
      orderDetail.orderTempId = orderTempId;
    }
    await this.query().insertMany(orderDetails);
    return await this.query().getAll({ 'orderTempId': orderTempId });
  }

  async updateMany(orderTempId, orderDetails) {
    let data = await this.query().select('id').where({ 'orderTempId': orderTempId });
    let arr = [];
    for (let id of data) {
      arr.push(id.id);
    }
    for (let orderDetail of orderDetails) {
      orderDetail.orderTempId = orderTempId;
      if (orderDetail.id) {
        let index = arr.indexOf(orderDetail.id);
        if (index !== -1) {
          arr.splice(index, 1);
          await this.query().updateOne(orderDetail.id, orderDetail);
        }
      }
      else {
        await this.query().insertOne(orderDetail)
      }
    }
    if (arr) {
      await this.query().delByIds(arr);
    }
    return await this.query().getAll({ 'orderTempId': orderTempId });
  }
}

module.exports = OrderTempDetailRepository