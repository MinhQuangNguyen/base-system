const BaseRepository = require("./BaseByCompanyRepository");
const CategoryModel = require("@app/Models/Category");

class CategoryRepository extends BaseRepository {
    constructor(){
        super();
        this.Model = new CategoryModel();
    }

    async getByIds(ids) {
        return await this.query().whereIn('id', ids);
    }

    async getTreeData(parentId = null) {
        let result = [];
        const categories = await this.query().where({
            parentId: parentId
        }).orderBy('id','asc')
        if (!categories) return;
        let pChildrens = []
        for (let category of categories) {
            let info = {
                value: category.id,
                title: category.name,
                children: []
            }
            result.push(info)

            pChildrens.push(this.getTreeData(category.id).then((child) => {
                info["children"] = child
            }))
            
        }
        await Promise.all(pChildrens)
        return result;
    }
}

module.exports = CategoryRepository;