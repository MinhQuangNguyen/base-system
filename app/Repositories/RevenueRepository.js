const BaseRepository = require("./BaseByCompanyRepository")
const RevenueModel = require("@app/Models/Revenue")
class RevenueRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new RevenueModel();
  }
}

module.exports = RevenueRepository