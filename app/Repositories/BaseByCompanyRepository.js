const BaseRepository = require('./BaseRepository')
class BaseByCompanyRepository extends BaseRepository {
    setCompanyId(companyId) {
        this.companyId = companyId
        return this
    }

    query(companyId = this.companyId) {
        this.companyId = companyId;
        if (!companyId) {
            throw new Error('companyId is required in query() function')
        }
        return this.Model.query().on('start', (builder) => {
            builder.where(`${this.Model.tableName}.companyId`, companyId);
        })
            
    }
}

module.exports = BaseByCompanyRepository