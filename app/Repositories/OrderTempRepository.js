const BaseRepository = require("./BaseByCompanyRepository")
const OrderTempModel = require("@app/Models/OrderTemp")
class OrderTempRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new OrderTempModel();
  }
  async getById(id){
    return await this.query().getOne({id})
  }
}

module.exports = OrderTempRepository