const BaseRepository = require("./BaseRepository")
const UserGroupPermissionModel = require("@app/Models/UserGroupPermission")

class UserGroupPermissionRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new UserGroupPermissionModel();
  }
  async getById(id){
    return await this.query().getOne({id})
  }
  async getByPermissionKey({permissionKey, groupId}){
    return await this.query().getOne({permissionKey, groupId})
  }
  
}

module.exports = UserGroupPermissionRepository