const BaseRepository = require("./BaseByCompanyRepository")
const UserGroupModel = require("@app/Models/UserGroup")
class UserGroupRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new UserGroupModel();
  }
  async getById(id){
    return await this.query().getOne({id})
  }
  async getByUserGroupName(name){
    return await this.query().getOne({name})
  }

  async getPermissions(groupId) {
    const result = await this.query()
      .where({groupId: groupId})
      .join('user_group_permissions', 'user_groups.id', 'groupId')
      .join('user_permissions', 'user_permissions.key', 'user_group_permissions.permissionKey')
    
    let permissions = {}
    result.map(permission => permissions[permission.key] = permission.value)
    return permissions
  }
  
}

module.exports = UserGroupRepository