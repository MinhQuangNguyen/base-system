const BaseRepository = require("./BaseByCompanyRepository")
const CustomerModel = require("@app/Models/Customer")
class CustomerRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new CustomerModel();
  }
  async getById(id){
    return await this.query().getOne({id})
  }
  async getByCustomerName(fullname){
    return await this.query().getOne({fullname})
  }
}

module.exports = CustomerRepository