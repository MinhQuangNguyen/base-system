const BaseRepository = require("./BaseRepository");
const SalaryDetailModel = require("@app/Models/SalaryDetail");

class SalaryDetailRepository extends BaseRepository {
    constructor() {
        super();
        this.Model = new SalaryDetailModel();
    }

    async updateMany(salaryId, details) {
        let oldDetails = await this.query()
            .where({ 'salaryId': salaryId })
            .pluck('id');

        for (let detail of details) {
            detail.total = Number(detail.salary) + Number(detail.extra) - Number(detail.fine) + Number(detail.bonus);
            if (detail.id) {
                let index = oldDetails.indexOf(detail.id);
                if (index !== -1) {
                    oldDetails.splice(index, 1)
                    await this.query().updateOne(detail.id, detail)
                }
            }
            else {
                await this.query().insertOne(detail)
            }
        }

        if (oldDetails) {
            await this.query().delByIds(arr);
        }
        return await this.query().getAll({ 'salaryId': salaryId });
    }

    async updateOne(id, data) {

        return await this.query().updateOne(id, data)

    }
}

module.exports = SalaryDetailRepository;