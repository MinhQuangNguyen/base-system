const BaseRepository = require("./BaseByCompanyRepository")
const PermissionCategoryModel = require("@app/Models/PermissionCategory")
class PermissionCategoryRepository extends BaseRepository {
    constructor() {
        super();
        this.Model = new PermissionCategoryModel();
    }

    async getById(id) {
        return await this.query().getOne({ id })
    }

    async getByName(name) {
        return await this.query().getOne({ name })
    }

}

module.exports = PermissionCategoryRepository