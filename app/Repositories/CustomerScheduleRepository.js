const BaseRepository = require("./BaseByCompanyRepository");
const CustomerSchedule = require("@app/Models/CustomerSchedule");

class CustomerScheduleRepository extends BaseRepository {
    constructor(){
        super();
        this.Model = new CustomerSchedule();
    }

    async getList(fromDate, toDate) {
        return await this.query()
            .select('customers.fullname as Name', 'scheduleId', 'orderDate', 'scheduleDate', 'status')
            .whereBetween('scheduleDate', [fromDate, toDate])
            .leftJoin('customers', {
                'customer_schedules.customerId': 'customers.id'
              })
            ;
    }
}

module.exports = CustomerScheduleRepository;