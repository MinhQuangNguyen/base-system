const BaseRepository = require("./BaseByCompanyRepository")
const UserModel = require("@app/Models/User")
class UserRepository extends BaseRepository{
  constructor(){
    super();
    this.Model = new UserModel();
  }

  async checkLogin({username, password}){
    const user = await this.Model.query().getOne({username: username}); //byPass check companyId
    if(!user) return false;

    //await this.Model.changePassword(user.id, "123456")
    let checkPassword = await this.Model.compare(password, user.password);
    delete user.password;
    if(checkPassword) return user;
    return false;
  }

  async hash(planText){
    return this.Model.hash(planText)
  }

  async getByUsername(username){
    return await this.query().getOne({username})
  }

  async getById(id){
    return await this.query().getOne({id})
  }
}

module.exports = UserRepository