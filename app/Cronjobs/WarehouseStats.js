const Base = require('./Base')
const ProductRepository = require("@app/Repositories/ProductRepository");
const CompanyRepository = new (require("@app/Repositories/CompanyRepository"))();
const msg_schedule = ["8", "12", "16:21:00", "16", "20", "24"];
const moment = require('moment')
var fs = require("fs");
const cacheFilePath = 'temp/warehouseStats.cache.json'

class WarehouseStats extends Base {
    static schedule = "* * * * * *"

    static async handle() {
        const now = moment().format("HH:mm:ss");

        //if (msg_schedule.includes(now)) {
        if (true) { //for dev
            let companies = await CompanyRepository.getAll()
            companies.map(company => this.checkByCompany(company))

            //update lasttime
            this.lastTime = moment().toISOString()
        }
    }

    /**
     * Kiểm tra xem @company có sản phẩm sắp hết hay không
     * Nếu có, sent message đến số điện thoại liên hệ của company
     * @param {Company} company 
     */
    static async checkByCompany(company) {
        const products = await this.productsNeedWarning(company)
        if (!products.length) return;

        const message = await this.buildMessage(company, products)
        this.sendMessage(message, company)
    }

    /**
     * Lấy danh sách các sản phẩm cần warning của công ty company
     */
    static productsNeedWarning = async (company) => {
        //chỗ này không được khai báo sẵn Repository vì nếu không sẽ dẫn đến tình trạng bị static companyId trong 2 request song song.
        const ProductRepositoryInstance = new ProductRepository();
        let products = await ProductRepositoryInstance.setCompanyId(company.id).query()
            .where("updatedAt", ">", this.lastTime)
            .andWhere("updatedAt", "<=", moment().toISOString())
            .andWhere("type", 1)

        return products.filter(product => product.quantity <= product.limit)
    }

    static buildMessage = async (company, products) => {
        let data = products.map(product => `${product.name}: ${product.quantity}`).join("\n")

        const template = `
        Chào quý khách ${company.name},
        Hệ thống quản lý sản phẩm của MQ xin thông báo về số lượng sản phẩm trong kho của quý khách:
        ${data}
        Quý khách vui lòng nhập thêm hàng cho những sản phẩm trên.
        `
        return template
    }

    static sendMessage = async (message, company) => {
        //sent message here
        console.log(`sent message to ${company.id}, message: ${message}`)
    }

    /**
    hàm get/set cho lastTime, sử dụng cacheFile
     */
    static get lastTime() {
        if (this._lastTime) return this._lastTime;

        try {
            this._lastTime = JSON.parse(fs.readFileSync(cacheFilePath, 'utf8')).lastTime
        } catch (e) {
            this._lastTime = "2015-03-25T12:00:00Z"
        }

        return this._lastTime
    }
    static set lastTime(value) {
        this._lastTime = value;
        fs.writeFileSync(cacheFilePath, JSON.stringify({
            lastTime: value
        }))
    }
}

module.exports = WarehouseStats