import vi from './vi'
import en from './en'

const resources = {
  vi,
  en
}
export default resources