
import { Progress  } from "antd";


const ProgressMqn = () => {
 

  return (
    <>
      <div className="top50">
        <Progress percent={30} />
        <Progress percent={50} status="active" />
        <Progress percent={70} status="exception" />
        <Progress percent={100} />
        <Progress percent={50} showInfo={false} />
      </div>
    </>
  );
};

export default ProgressMqn;
