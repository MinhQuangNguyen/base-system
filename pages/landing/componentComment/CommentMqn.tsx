import React, { createElement, useState } from "react";
import { Comment, Tooltip, Avatar, Col, Divider, Form, Button, List, Input, Mentions } from "antd";
import moment from "moment";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeFilled,
  LikeFilled,
} from "@ant-design/icons";
import useBaseHooks from 'themes/hooks/BaseHooks';

const { TextArea } = Input;
const { Option, getMentions } = Mentions;
const CommentMqn = () => {

  const { t } = useBaseHooks({ lang: ["common"] });

  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(0);

  const like = () => {
    setLikes(1);
    setDislikes(0);
    setAction(2);
  };

  const dislike = () => {
    setLikes(0);
    setDislikes(1);
    setAction(3);
  };


  const actions = [
    <Tooltip key="comment-basic-like" title={t("Comment.Like")}>
      <span onClick={like}>
        {createElement(action === 2 ? LikeFilled : LikeOutlined)}
        <span className="comment-action">{likes}</span>
      </span>
    </Tooltip>,
    <Tooltip key="comment-basic-dislike" title={t("Comment.Dislike")}>
      <span onClick={dislike}>
        {React.createElement(
          action === 3 ? DislikeFilled : DislikeOutlined
        )}
        <span className="comment-action">{dislikes}</span>
      </span>
    </Tooltip>,
    <span key="comment-basic-reply-to">{t("Comment.ReplyTo")}</span>,
  ];


  return (
    <>
      <div className="table-checkbox-mqn">
        <div className="title-content-table">
          <div className="title-table">{t("Comment.ListComment")}</div>
          <img src="/static/img/more.svg" alt="icon-more" />
        </div>
        <Col span={24}>
          <Comment
            actions={actions}
            author={<p>Minh Quang Nguyen</p>}
            avatar={
              <Avatar
                src="/static/img/avata-mqn.png"
                alt="Minh Quang Nguyen"
              />
            }
            content={
              <p>
                Tại Media Iconic, mỗi dự án đều được triển khai với những định hướng chiến lược rõ ràng, kết hợp với năng lực sáng tạo mạnh mẽ. Cung cấp cho khách hàng các giải pháp về truyền thông, định hướng phát triển thương hiệu cũng như phát triển hình ảnh thương hiệu. Chúng tôi luôn đưa ra nhiều phương án thiết kế và ý tưởng thiết kế một cách rõ ràng, thể hiện rõ khả năng ứng dụng thực tế giúp khách hàng dễ dàng lựa chọn.
              </p>
            }
            datetime={
              <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
                <span>{moment().fromNow()}</span>
              </Tooltip>
            }
          >


            {/* Rep comment */}
            <Comment
              actions={actions}
              author={<p>Admin</p>}
              avatar={
                <Avatar
                  src="/static/img/avata1.png"
                  alt="Admin"
                />
              }
              content={
                <p>
                  Cảm ơn bạn đã phản hồi với chúng tôi!
                </p>
              }
              datetime={
                <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
                  <span>{moment().fromNow()}</span>
                </Tooltip>
              }
            >

              {/* Comment khách */}
              <Comment
                actions={actions}
                author={<p>Minh Quang Nguyen</p>}
                avatar={
                  <Avatar
                    src="/static/img/avata-mqn.png"
                    alt="Minh Quang Nguyen"
                  />
                }
                content={
                  <p>
                    Hãy liên hệ với qua tôi số điện thoại 0345736211 hoặc email alien-designer@mediaiconic.com
                  </p>
                }
                datetime={
                  <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
                    <span>{moment().fromNow()}</span>
                  </Tooltip>
                }
              >

                {/* Comment Admin */}
                <Comment
                  actions={actions}
                  author={<p>Admin</p>}
                  avatar={
                    <Avatar
                      src="/static/img/avata1.png"
                      alt="Admin"
                    />
                  }
                  content={
                    <p>
                      Dạ vâng ạ, em sẽ phản hồi sớm ạ!
                    </p>
                  }
                  datetime={
                    <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
                      <span>{moment().fromNow()}</span>
                    </Tooltip>
                  }
                >
                  {/* Form rep comment */}
                  <Comment
                    avatar={
                      <Avatar
                        src="/static/img/avata-mqn.png"
                        alt="Minh Quang Nguyen"
                      />
                    }
                    content={
                      <>
                        <Form
                          name="repComment"
                          autoComplete="off"
                          layout="vertical"
                        >
                          <Form.Item
                            name="username"
                            rules={[{ required: true, message: t("input.rule.PleaseWriteAReview") }]}
                          >
                            <Mentions rows={4} placeholder={t("Comment.WriteAComment")}>
                              <Option value="MinhQuangNguyen">MinhQuangNguyen</Option>
                              <Option value="DoanTamDan">DoanTamDan</Option>
                            </Mentions>
                          </Form.Item>

                          <Form.Item>
                            <Button htmlType="submit" type="primary" className="float-right">
                              {t("Button.Comment")}
                            </Button>
                          </Form.Item>
                        </Form>
                      </>
                    }
                  />

                  {/* End Form rep comment */}
                </Comment>
                {/* End comment Admin */}

              </Comment>
              {/* End comment khách */}
            </Comment>
            {/* End rep comment */}
          </Comment>



          <Divider />

          <Comment
            actions={actions}
            author={<p>Minh Quang Nguyen</p>}
            avatar={
              <Avatar
                src="/static/img/avata-mqn.png"
                alt="Minh Quang Nguyen"
              />
            }
            content={
              <p>
                Tại Media Iconic, mỗi dự án đều được triển khai với những định hướng chiến lược rõ ràng, kết hợp với năng lực sáng tạo mạnh mẽ. Cung cấp cho khách hàng các giải pháp về truyền thông, định hướng phát triển thương hiệu cũng như phát triển hình ảnh thương hiệu. Chúng tôi luôn đưa ra nhiều phương án thiết kế và ý tưởng thiết kế một cách rõ ràng, thể hiện rõ khả năng ứng dụng thực tế giúp khách hàng dễ dàng lựa chọn.
              </p>
            }
            datetime={
              <Tooltip title={moment().format("DD-MM-YYYY HH:mm:ss")}>
                <span>{moment().fromNow()}</span>
              </Tooltip>
            }
          />

          <Divider />

        </Col>
      </div>
    </>
  );
};

export default CommentMqn;
