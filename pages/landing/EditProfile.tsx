import React, { useState } from 'react';
import Layout from "themes/layouts/Home";
import { Row, Col, Radio, Checkbox, Button, Tabs, Form, Input, Divider, message, Switch } from "antd";
import useBaseHooks from 'themes/hooks/BaseHooks';
import Link from "next/link";
import { route as makeUrl } from "themes/route";
import UploadMqn from "./componentForm/UploadMqn"
import TablePaymenthistory from "./componenProfile/TablePaymenthistory"



const EditProfile = () => {

  const { t } = useBaseHooks();

  const { TabPane } = Tabs;

  const { TextArea } = Input;


  function onChange(checked: any) {
    console.log(`switch to ${checked}`);
  }

  interface FieldData {
    name: string | number | (string | number)[];
    value?: any;
    touched?: boolean;
    validating?: boolean;
    errors?: string[];
  }

  interface CustomizedFormProps {
    onChange: (fields: FieldData[]) => void;
    fields: FieldData[];
  }


  const CustomizedForm: React.FC<CustomizedFormProps> = () => (

    <Row gutter={24}>
      <Col xs={24} xl={12} xxl={12}>
        <Form.Item
          name="firstname"
          label={t("Input.FirstName")}
          rules={[{ required: true, message: 'Vui lòng nhập tên!' }]}
        >
          <Input placeholder={t("Input.FirstName")} allowClear />
        </Form.Item>
      </Col>
      <Col xs={24} xl={12} xxl={12}>
        <Form.Item
          label={t("Input.LastName")}
          name="lastname"
          rules={[{ required: true, message: 'Vui lòng nhập họ!' }]}
        >
          <Input placeholder={t("Input.LastName")} allowClear />
        </Form.Item>
      </Col>
      <Col xs={24} xl={12} xxl={12}>
        <Form.Item
          name="email"
          label={t("Input.Email")}
        >
          <Input placeholder={t("Input.Email")} disabled />
        </Form.Item>
        <div className="item-decs-email">
          <img src="/static/img/warning.png" alt="icon-warning" />
          <div className="text">{t("Input.CheckYourInboxToConfirmOwnershipOfThisEmailAddress")}</div>
        </div>
      </Col>
      <Col xs={24} xl={12} xxl={12}>
        <div className="text-send-again">{t("Input.DidntGetAVerificationEmail")} <span>{t("Input.SendAgain")}</span></div>
      </Col>
      <Col xs={24} xl={12} xxl={12}>
        <Form.Item
          name="phone"
          label={t("Input.Label.PhoneNumber")}
          rules={[
            {
              pattern: new RegExp("^[0-9]*$"),
              message: "Sai định dạng"
            },
          ]}
        >
          <Input style={{ width: '100%' }} placeholder={t("Input.Label.PhoneNumber")}  allowClear />
        </Form.Item>
      </Col>
      <Col xs={24} xl={12} xxl={12}>
        <Form.Item
          label={t("Input.Address")}
          name="address"
        >
          <Input placeholder={t("Input.Address")} allowClear />
        </Form.Item>
      </Col>


      <Col span={24}>
        <Form.Item name="messager" label="Lời nhắn" >
          <TextArea maxLength={100} placeholder={t("Input.IntroduceYourself")} allowClear />
          <div className="item-decs-email top-10">
            <img src="/static/img/warning.png" alt="icon-warning" />
            <div className="text">{t("Input.BriefDescriptionForYouProfile")}</div>
          </div>
        </Form.Item>
      </Col>

      <Col span={24}>
        <div className="title">{t("Input.SocialProfile")}</div>
      </Col>

      <Col span={24}>
        <Form.Item
          label="Facebook"
          name="facebook"
        >
          <Input placeholder="Facebook" allowClear />
        </Form.Item>
      </Col>

      <Col span={24}>
        <Form.Item
          label="Twitter"
          name="twitter"
        >
          <Input placeholder="Twitter" allowClear />
        </Form.Item>
      </Col>

      <Col span={24}>
        <Form.Item
          label="Linkedin"
          name="linkedin"
        >
          <Input placeholder="Linkedin" allowClear />
        </Form.Item>
      </Col>


      <Divider />

      <Col span={"24"}>
        <div className="button-form">
          <Form.Item>
            <Button style={{ float: 'left' }}>
              <Link {...makeUrl("frontend.landing.Profile")}>
                 {t("Button.Prev")}
              </Link>
            </Button>
            <Button type="primary" htmlType="submit" style={{ float: 'right' }}>
             {t("Button.SaveChanges")}
            </Button>
          </Form.Item>
        </div>
      </Col>

    </Row>

  );

  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const [fields, setFields] = useState<FieldData[]>([
    { name: ['firstname'], value: 'Minh Quang' },
    { name: ['lastname'], value: 'Nguyen' },
    { name: ['email'], value: 'alien@mediaiconic.com' },
    { name: ['phone'], value: '0345736211' },
    { name: ['address'], value: '12 Khuất Duy, Thanh Xuân, TP. Hà Nội' }
  ]);



  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <div className="item-edit-view-detail-overview-profile">
            <div className="item-tab-main-overview">
              <Tabs defaultActiveKey="Profile">
                <TabPane tab={t("Profile.Profile")} key="Profile">
                  <div className="item-tab-profile">
                    <Row justify="center">
                      <Col span={24}>
                        <div className="tab-edit-profile-pc">
                          <Tabs tabPosition="left" type="card" defaultActiveKey="Personal">

                            <TabPane tab={t("Profile.Personal")} key="Personal">
                              <Row justify="center">
                                <Col xs={24} xl={22} xxl={22}>
                                  <div className="item-logo-main">
                                    <div className="img-avata">
                                      <UploadMqn />
                                    </div>
                                    <div className="item-content-name-user">
                                      <div className="name">Minh Quang Nguyen</div>
                                      <div className="decs">Founder Media Iconic</div>
                                    </div>
                                  </div>

                                  <div className="item-form-info-profile">
                                    <div className="title">{t("Profile.PersonalInfo")}</div>
                                    <Form
                                      name="ChangeInfo"
                                      initialValues={{ remember: true, prefix: '84', }}
                                      autoComplete="off"
                                      layout="vertical"
                                      fields={fields}
                                      onFinish={onFinish}
                                      onFinishFailed={onFinishFailed}
                                    >
                                      <CustomizedForm
                                        fields={fields}
                                        onChange={newFields => {
                                          setFields(newFields);
                                        }}
                                      />
                                    </Form>
                                  </div>
                                </Col>
                              </Row>
                            </TabPane>

                            <TabPane tab={t("Profile.Billing")} key="Billing">
                              <Row justify="center">
                                <Col xs={24} xl={22} xxl={22}>
                                  <div className="item-list-billing-plugin">
                                    <div className="title">{t("Profile.PaymentHistory")}</div>
                                  </div>
                                  <TablePaymenthistory />
                                </Col>
                              </Row>
                            </TabPane>

                            <TabPane tab={t("Profile.Password")} key="Password">
                              <Row justify="center">
                                <Col xs={24} xl={22} xxl={22}>
                                  <div className="item-form-password">
                                    <div className="title">{t("Input.ChangePassword")}</div>
                                  </div>
                                  <Form
                                    name="changePassword"
                                    initialValues={{ remember: true, prefix: '84', }}
                                    autoComplete="off"
                                    layout="vertical"
                                    fields={fields}
                                    onFinish={onFinish}
                                    onFinishFailed={onFinishFailed}
                                  >
                                    <Row justify="center">
                                      <Col span={24}>
                                        <Form.Item
                                          label={t("Input.ChangePassword")}
                                          name="passwordOld"
                                          rules={[
                                            { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                                            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                                            {
                                              required: true,
                                              pattern: new RegExp("^[a-zA-Z0-9]*$"),
                                             message:t("Input.rule.IncorrectFormatPleaseDoNotEnterSpecialCharacters")
                                            },
                                          ]}
                                        >
                                          <Input.Password placeholder={t("Input.ChangePassword")} allowClear />
                                        </Form.Item>
                                      </Col>

                                      <Col span={24}>
                                        <Form.Item
                                          label={t("Input.currentPassword")}
                                          name="passwordNew"
                                          rules={[
                                            { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                                            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                                            {
                                              required: true,
                                              pattern: new RegExp("^[a-zA-Z0-9]*$"),
                                             message:t("Input.rule.IncorrectFormatPleaseDoNotEnterSpecialCharacters")
                                            },
                                          ]}
                                        >
                                          <Input.Password placeholder={t("Input.currentPassword")} allowClear />
                                        </Form.Item>
                                      </Col>

                                      <Col span={24}>
                                        <Form.Item
                                          label={t("Input.ConfirmPassword")}
                                          name="passwordConfirm"
                                          rules={[
                                            { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                                            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                                            {
                                              required: true,
                                              pattern: new RegExp("^[a-zA-Z0-9]*$"),
                                              message:t("Input.rule.IncorrectFormatPleaseDoNotEnterSpecialCharacters")
                                            },
                                          ]}
                                        >
                                          <Input.Password placeholder={t("Input.ConfirmPassword")} allowClear />
                                        </Form.Item>

                                      </Col>

                                      <Divider />

                                      <Col span={24}>
                                        <div className="button-form">
                                          <Form.Item>
                                            <Button style={{ float: 'left' }}>
                                              <Link {...makeUrl("frontend.landing.Profile")}>
                                                 {t("Button.Prev")}
                                              </Link>
                                            </Button>
                                            <Button type="primary" htmlType="submit" style={{ float: 'right' }}>
                                             {t("Button.SaveChanges")}
                                            </Button>
                                          </Form.Item>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Form>
                                </Col>
                              </Row>
                            </TabPane>

                            <TabPane tab={t("Profile.Setting")} key="Setting">
                              <Form
                                name="setting"
                                initialValues={{ remember: true, prefix: '84', }}
                                autoComplete="off"
                                layout="vertical"
                                fields={fields}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                              >
                                <Row justify="center">
                                  <Col xs={24} xl={22} xxl={22}>
                                    <div className="item-form-setting">
                                      <div className="title">{t("Profile.ActiveStatus")}</div>
                                      <div className="decs">{t("Profile.DependingOnTheOperatingStatusOnOrOff")}</div>
                                    </div>
                                    <div className="item-switch-on-off">
                                      <Switch defaultChecked onChange={onChange} />
                                    </div>
                                    <Divider />

                                    <div className="item-form-setting">
                                      <div className="title">{t("Profile.NotificationsFromUs")}</div>
                                      <div className="decs">{t("Profile.ReceiveTheLastestNewsUpdatesAndIndustryTutorialsFromUs")}</div>
                                    </div>

                                    <div className="item-checkbox-setting">
                                      <Checkbox defaultChecked>
                                      {t("Profile.NewsAndUpdates")} <br /> <span className="decsText">{t("Profile.NewsAboutProductAndFeatureUpdates")}</span>
                                      </Checkbox>
                                    </div>

                                    <div className="item-checkbox-setting">
                                      <Checkbox defaultChecked>
                                      {t("Profile.TipsAndTutorials")} <br /> <span className="decsText">{t("Profile.TipsOnGettingMoreOutOfUntitled")}</span>
                                      </Checkbox>
                                    </div>

                                    <div className="item-checkbox-setting">
                                      <Checkbox>
                                      {t("Profile.UserResearch")} <br /> <span className="decsText">{t("Profile.GetInvolvedInOurBetaTestingProgramOrParticipateInPaidProductUserResearch")}</span>
                                      </Checkbox>
                                    </div>
                                    <Divider />
                                    <Radio.Group name="comment" defaultValue="allComment">
                                      <div className="item-form-setting top-10-setting">
                                        <div className="title">{t("Profile.Comments")}</div>
                                        <div className="decs">{t("Profile.TheseAreNotificationsForCommnentOnYourPostsAndRepliesToYourComments")}</div>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="notNotifiy">
                                          {t("Profile.DoNotNotifiyMe")}
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="Mentions">
                                        {t("Profile.Mentions")} <br /> <span className="decsText">{t("Profile.OnlyNotifyMeIfImMentionedInAComment")}</span>
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="allComment">
                                        {t("Profile.AllCommment")} <br /> <span className="decsText"> {t("Profile.NotifyMeForAllCommentsOnMyPosts")}</span>
                                        </Radio>
                                      </div>
                                    </Radio.Group>
                                    <Divider />
                                    <Radio.Group name="reminders" defaultValue="allReminders">
                                      <div className="item-form-setting top-10-setting">
                                        <div className="title">{t("Profile.Reminders")}</div>
                                        <div className="decs">{t("Profile.TheseAreNotificationToRemindYouOfUpdatesYouMightHaveMissed")}</div>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="notNotifiyRemider">
                                        {t("Profile.DoNotNotifiyMe")}
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="importReminders">
                                          {t("Profile.ImportandRemindersOnly")} <br /> <span className="decsText">{t("Profile.OnlyNotifyMeIfTheReminderIsTaggedAsImportant")}</span>
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="allReminders">
                                          {t("Profile.AllReminders")} <br /> <span className="decsText">{t("Profile.NotifyMeForAllReminders")}</span>
                                        </Radio>
                                      </div>
                                    </Radio.Group>
                                    <Divider />
                                    <Radio.Group name="activity" defaultValue="notNotifiyActivity">
                                      <div className="item-form-setting top-10-setting">
                                        <div className="title">{t("Profile.MoreActivityAboutYou")}</div>
                                        <div className="decs">{t("Profile.TheseAreNotificationForPostsOnYourProfileLikeAndOtherReactionsToYourPostsAndMore")}</div>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="notNotifiyActivity">
                                          {t("Profile.DoNotNotifiyMe")}
                                        </Radio>
                                      </div>

                                      <div className="item-radio-setting">
                                        <Radio value="allActivity">
                                          {t("Profile.AllReminders")} <br /> <span className="decsText">{t("Profile.NotifyMeForAllOtherActivity")}</span>
                                        </Radio>
                                      </div>
                                    </Radio.Group>
                                    <Divider />
                                    <div className="button-form top-40-setting">
                                      <Form.Item>
                                        <Button style={{ float: 'left' }}>
                                          <Link {...makeUrl("frontend.landing.Profile")}>
                                            {t("Button.Prev")}
                                          </Link>
                                        </Button>
                                        <Button type="primary" onClick={() => message.success(t("Desc.SaveSuccessfully"))} htmlType="submit" style={{ float: 'right' }}>
                                            {t("Button.SaveChanges")}
                                        </Button>
                                      </Form.Item>
                                    </div>
                                  </Col>
                                </Row>
                              </Form>
                            </TabPane>
                          </Tabs>
                        </div>

                        <div className="tab-edit-profile-mobile">
                          <Tabs defaultActiveKey="Personal">
                            <TabPane tab="Personal" key="Personal">
                              <Row justify="center">
                                <Col xs={24} xl={22} xxl={22}>
                                  <div className="item-logo-main">
                                    <div className="img-avata">
                                      <UploadMqn />
                                    </div>
                                    <div className="item-content-name-user">
                                      <div className="name">Minh Quang Nguyen</div>
                                      <div className="decs">Founder Media Iconic</div>
                                    </div>
                                  </div>

                                  <div className="item-form-info-profile">
                                    <div className="title">Personal Info</div>
                                    <Form
                                      name="ChangeInfo"
                                      initialValues={{ remember: true, prefix: '84', }}
                                      autoComplete="off"
                                      layout="vertical"
                                      fields={fields}
                                      onFinish={onFinish}
                                      onFinishFailed={onFinishFailed}
                                    >
                                      <CustomizedForm
                                        fields={fields}
                                        onChange={newFields => {
                                          setFields(newFields);
                                        }}
                                      />
                                    </Form>
                                  </div>
                                </Col>
                              </Row>
                            </TabPane>
                            <TabPane tab="Billing" key="Billing">
                              <Row justify="center">
                                <Col xs={24} xl={22} xxl={22}>
                                  <div className="item-list-billing-plugin">
                                    <div className="title">Payment history</div>
                                  </div>
                                  <TablePaymenthistory />
                                </Col>
                              </Row>
                            </TabPane>

                            <TabPane tab="Password" key="Password">
                              <Row justify="center">
                                <Col xs={24} xl={22} xxl={22}>
                                  <div className="item-form-password">
                                    <div className="title">Change Password</div>
                                  </div>
                                  <Form
                                    name="changePassword"
                                    initialValues={{ remember: true, prefix: '84', }}
                                    autoComplete="off"
                                    layout="vertical"
                                    fields={fields}
                                    onFinish={onFinish}
                                    onFinishFailed={onFinishFailed}
                                  >
                                    <Row justify="center">
                                      <Col span={24}>
                                        <Form.Item
                                          label={t("Input.currentPassword")}
                                          name="passwordOld"
                                          rules={[
                                            { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                                            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                                            {
                                              required: true,
                                              pattern: new RegExp("^[a-zA-Z0-9]*$"),
                                              message: "Sai định dạng, vui lòng không nhập kí tự đặc biệt!"
                                            },
                                          ]}
                                        >
                                          <Input.Password placeholder={t("Input.currentPassword")} allowClear />
                                        </Form.Item>
                                      </Col>

                                      <Col span={24}>
                                        <Form.Item
                                          label={t("Input.ANewPassword")}
                                          name="passwordNew"
                                          rules={[
                                            { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                                            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                                            {
                                              required: true,
                                              pattern: new RegExp("^[a-zA-Z0-9]*$"),
                                              message: "Sai định dạng, vui lòng không nhập kí tự đặc biệt!"
                                            },
                                          ]}
                                        >
                                          <Input.Password placeholder={t("Input.ANewPassword")} allowClear />
                                        </Form.Item>
                                      </Col>

                                      <Col span={24}>
                                        <Form.Item
                                          label={t("Input.ConfirmPassword")}
                                          name="passwordConfirm"
                                          rules={[
                                            { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                                            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                                            {
                                              required: true,
                                              pattern: new RegExp("^[a-zA-Z0-9]*$"),
                                              message: "Sai định dạng, vui lòng không nhập kí tự đặc biệt!"
                                            },
                                          ]}
                                        >
                                          <Input.Password placeholder={t("Input.ConfirmPassword")} allowClear />
                                        </Form.Item>

                                      </Col>

                                      <Divider />

                                      <Col span={24}>
                                        <div className="button-form">
                                          <Form.Item>
                                            <Button style={{ float: 'left' }}>
                                              <Link {...makeUrl("frontend.landing.Profile")}>
                                                 {t("Button.Prev")}
                                              </Link>
                                            </Button>
                                            <Button type="primary" htmlType="submit" style={{ float: 'right' }}>
                                             {t("Button.SaveChanges")}
                                            </Button>
                                          </Form.Item>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Form>
                                </Col>
                              </Row>
                            </TabPane>

                            <TabPane tab="Setting" key="Setting">
                              <Form
                                name="setting"
                                initialValues={{ remember: true, prefix: '84', }}
                                autoComplete="off"
                                layout="vertical"
                                fields={fields}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                              >
                                <Row justify="center">
                                  <Col xs={24} xl={22} xxl={22}>
                                    <div className="item-form-setting">
                                      <div className="title">Active status</div>
                                      <div className="decs">Depending on the operating status on or off.</div>
                                    </div>
                                    <div className="item-switch-on-off">
                                      <Switch defaultChecked onChange={onChange} />
                                    </div>
                                    <Divider />

                                    <div className="item-form-setting">
                                      <div className="title">Notifications from us</div>
                                      <div className="decs">Receive the lastest news, updates and industry tutorials from us.</div>
                                    </div>

                                    <div className="item-checkbox-setting">
                                      <Checkbox defaultChecked>
                                        News and updates <br /> <span className="decsText">News about product and feature updates.</span>
                                      </Checkbox>
                                    </div>

                                    <div className="item-checkbox-setting">
                                      <Checkbox defaultChecked>
                                        Tips and tutorials <br /> <span className="decsText">Tips on getting more out of Untitled.</span>
                                      </Checkbox>
                                    </div>

                                    <div className="item-checkbox-setting">
                                      <Checkbox>
                                        User research <br /> <span className="decsText">Get involved in our beta testing program or participate in paid product user research</span>
                                      </Checkbox>
                                    </div>
                                    <Divider />
                                    <Radio.Group name="comment" defaultValue="allComment">
                                      <div className="item-form-setting top-10-setting">
                                        <div className="title">Comments</div>
                                        <div className="decs">These are notifications for commnent on your posts and replies to your comments.</div>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="notNotifiy">
                                          Do not notifiy me
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="Mentions">
                                          Mentions <br /> <span className="decsText">Only notify me if I'm mentioned in a comment.</span>
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="allComment">
                                          All commment <br /> <span className="decsText">Notify me for all comments on my posts.</span>
                                        </Radio>
                                      </div>
                                    </Radio.Group>
                                    <Divider />
                                    <Radio.Group name="reminders" defaultValue="allReminders">
                                      <div className="item-form-setting top-10-setting">
                                        <div className="title">Reminders</div>
                                        <div className="decs">These are notification to remind you of updates you might have missed</div>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="notNotifiyRemider">
                                          Do not notifiy me
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="importReminders">
                                          Importand reminders only <br /> <span className="decsText">Only notify me if the reminder is tagged as important.</span>
                                        </Radio>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="allReminders">
                                          All reminders <br /> <span className="decsText">Notify me for all reminders.</span>
                                        </Radio>
                                      </div>
                                    </Radio.Group>
                                    <Divider />
                                    <Radio.Group name="activity" defaultValue="notNotifiyActivity">
                                      <div className="item-form-setting top-10-setting">
                                        <div className="title">More activity about you</div>
                                        <div className="decs">These are notification for posts on your profile, like and other reactions to your posts, and more.</div>
                                      </div>
                                      <div className="item-radio-setting">
                                        <Radio value="notNotifiyActivity">
                                          Do not notifiy me
                                        </Radio>
                                      </div>

                                      <div className="item-radio-setting">
                                        <Radio value="allActivity">
                                          All reminders <br /> <span className="decsText">Notify me for all other activity.</span>
                                        </Radio>
                                      </div>
                                    </Radio.Group>
                                    <Divider />
                                    <div className="button-form top-40-setting">
                                      <Form.Item>
                                        <Button style={{ float: 'left' }}>
                                          <Link {...makeUrl("frontend.landing.Profile")}>
                                             {t("Button.Prev")}
                                          </Link>
                                        </Button>
                                        <Button type="primary" onClick={() => message.success(t("Desc.SaveSuccessfully"))} htmlType="submit" style={{ float: 'right' }}>
                                         {t("Button.SaveChanges")}
                                        </Button>
                                      </Form.Item>
                                    </div>
                                  </Col>
                                </Row>
                              </Form>
                            </TabPane>
                          </Tabs>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </TabPane>
                
                <TabPane tab={t("Profile.Purchases")} key="Purchases">
                  <div className="item-detail-camp">
                    <div className="list-camp-main">
                      <div className="content-title-camp">
                        <div className="title-camp">{t("title.TheSupportFeaturePacksYouHavePurchased")}</div>
                      </div>
                    </div>
                    <Form
                      name="settingPlugin"
                      initialValues={{ remember: true, prefix: '84', }}
                      autoComplete="off"
                      layout="vertical"
                      fields={fields}
                      onFinish={onFinish}
                      onFinishFailed={onFinishFailed}
                    >
                      <Row gutter={24}>
                        <Col xs={24} xl={8} xxl={8}>
                          <div className="list-camp">
                            <div className="info-plugins">
                              <div className="title-plugins">{t("Plugin.FullyIntegrated")}</div>
                              <div className="decs-plugins">{t("Plugin.DecsPlugin")}</div>
                              <div className="price-camp">
                                {t("Table.Active")} <span>/ {t("title.EXPIRYDATE")}</span>
                              </div>
                            </div>
                            <div className="avata-plugins">
                              <img src="/static/img/plugin1.png" alt="plugin1" />
                              <div className="turn-on-off-plugin">
                                <Switch defaultChecked onChange={onChange} />
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col xs={24} xl={8} xxl={8}>
                          <div className="list-camp">
                            <div className="info-plugins">
                              <div className="title-plugins">{t("Plugin.VoiceNotifications")}</div>
                              <div className="decs-plugins">{t("Plugin.DecsPlugin")}</div>
                              <div className="price-camp">
                               {t("Table.Active")} <span>/ {t("title.EXPIRYDATE")}</span>
                              </div>
                            </div>
                            <div className="avata-plugins">
                              <img src="/static/img/plugin2.png" alt="plugin2" />
                              <div className="turn-on-off-plugin">
                                <Switch defaultChecked onChange={onChange} />
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col xs={24} xl={8} xxl={8}>
                          <div className="list-camp">
                            <div className="info-plugins">
                              <div className="title-plugins">{t("Plugin.IncreaseAdEngagement")}</div>
                              <div className="decs-plugins">{t("Plugin.DecsPlugin")}</div>
                              <div className="price-camp">
                                {t("Table.Active")} <span className="colorRed">/ {t("Table.Expired")}</span>
                              </div>
                            </div>
                            <div className="avata-plugins">
                              <img src="/static/img/plugin3.png" alt="plugin3" />
                              <div className="button-detail">
                                <Button>{t("Button.Extend")}</Button>
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col xs={24} xl={8} xxl={8}>
                          <div className="list-camp">
                            <div className="info-plugins">
                              <div className="title-plugins">{t("Plugin.IncreaseMessageLeed")}</div>
                              <div className="decs-plugins">{t("Plugin.DecsPlugin")}</div>
                              <div className="price-camp">
                                {t("Table.Active")} <span className="colorRed">/ {t("Table.Expired")}</span>
                              </div>
                            </div>
                            <div className="avata-plugins">
                              <img src="/static/img/plugin4.png" alt="plugin4" />
                              <div className="button-detail">
                                <Button>{t("Button.Extend")}</Button>
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col span={24}>
                          <div className="button-form-setting-plugin">
                            <Divider />
                            <Form.Item>
                              <Button style={{ float: 'left' }}>
                                <Link {...makeUrl("frontend.landing.Profile")}>
                                   {t("Button.Prev")}
                                </Link>
                              </Button>
                              <Button type="primary" onClick={() => message.success(t("Desc.SaveSuccessfully"))} htmlType="submit" style={{ float: 'right' }}>
                               {t("Button.SaveChanges")}
                              </Button>
                            </Form.Item>
                          </div>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </TabPane>

                <TabPane tab={t("Profile.CardBank")} key="CardBank">
                  <div className="item-card-bank-setting-profile">
                    <div className="your-saved-card">
                      <div className="content-type-bank">
                        <div className="title">{t("title.InternationalBank")}</div>
                        <span>{t("Button.AddBankCard")}</span>
                      </div>
                      <Row gutter={24}>
                        <Col xs={24} xl={8} xxl={6}>
                          <div className="item-international-bank-card">
                            <div className="item-card-international">
                              <img src="/static/img/icon-visa.png" alt="visa" />
                              <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                              <div className="number-card">
                                <span>1011</span>
                                <span>2536</span>
                                <span>1896</span>
                              </div>
                            </div>
                            <div className="item-info-card">
                              <div className="item-card-info">
                                <div className="title">{t("title.CARDHOLDER")}</div>
                                <div className="decs-text">Minh Quang Nguyen</div>
                              </div>
                              <div className="item-card-info content-right">
                                <div className="title">{t("title.EXPIRYDATE")}</div>
                                <div className="decs-text">28/08</div>
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col xs={24} xl={8} xxl={6}>
                          <div className="item-international-bank-card backgound-mastercard">
                            <div className="item-card-international">
                              <img src="/static/img/mastercard.png" alt="mastercard" />
                              <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                              <div className="number-card">
                                <span>2202</span>
                                <span>1536</span>
                                <span>1236</span>
                              </div>
                            </div>
                            <div className="item-info-card">
                              <div className="item-card-info">
                                <div className="title">{t("title.CARDHOLDER")}</div>
                                <div className="decs-text">Minh Quang Nguyen</div>
                              </div>
                              <div className="item-card-info content-right">
                                <div className="title">{t("title.EXPIRYDATE")}</div>
                                <div className="decs-text">01/03</div>
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col xs={24} xl={8} xxl={6}>
                          <div className="item-international-bank-card backgound-unionpay">
                            <div className="item-card-international">
                              <img src="/static/img/unionpay.png" alt="unionpay" />
                              <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                              <div className="number-card">
                                <span>3022</span>
                                <span>2522</span>
                                <span>3666</span>
                              </div>
                            </div>
                            <div className="item-info-card">
                              <div className="item-card-info">
                                <div className="title">{t("title.CARDHOLDER")}</div>
                                <div className="decs-text">Minh Quang Nguyen</div>
                              </div>
                              <div className="item-card-info content-right">
                                <div className="title">{t("title.EXPIRYDATE")}</div>
                                <div className="decs-text">05/10</div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>

                      <Divider />

                      <div className="content-type-bank">
                        <div className="title">{t("title.ElectronicWallet")}</div>
                        <span>{t("Button.AddBankCard")}</span>
                      </div>
                      <Row gutter={24}>
                        <Col xs={24} xl={8} xxl={6}>
                          <div className="item-international-bank-card solopay">
                            <div className="item-card-international">
                              <img src="/static/img/american-pay.png" alt="americanpay" />
                              <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                              <div className="number-card">
                                <span>3025</span>
                                <span>4632</span>
                                <span>1863</span>
                              </div>
                            </div>
                            <div className="item-info-card">
                              <div className="item-card-info">
                                <div className="title">{t("title.CARDHOLDER")}</div>
                                <div className="decs-text">Minh Quang Nguyen</div>
                              </div>
                              <div className="item-card-info content-right">
                                <div className="title">{t("title.EXPIRYDATE")}</div>
                                <div className="decs-text">28/08</div>
                              </div>
                            </div>
                          </div>
                        </Col>

                        <Col xs={24} xl={8} xxl={6}>
                          <div className="item-international-bank-card momo">
                            <div className="item-card-international">
                              <img src="/static/img/icon-solopay.png" alt="solopay" />
                              <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                              <div className="number-card">
                                <span>6868</span>
                                <span>8833</span>
                                <span>1102</span>
                              </div>
                            </div>
                            <div className="item-info-card">
                              <div className="item-card-info">
                                <div className="title">{t("title.CARDHOLDER")}</div>
                                <div className="decs-text">Minh Quang Nguyen</div>
                              </div>
                              <div className="item-card-info content-right">
                                <div className="title">{t("title.EXPIRYDATE")}</div>
                                <div className="decs-text">28/08</div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </TabPane>
              </Tabs>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default EditProfile;
