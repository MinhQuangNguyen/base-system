import Layout from "themes/layouts/Home";
import { Button, Row, Col, Select, Collapse } from "antd";
import { SettingOutlined } from "@ant-design/icons";
import { DatePicker } from "antd";
import TableMqn from "./TableMqn";
import ProgressMqn from "./ProgressMqn";
import TableMqnChart from "./TableMqnChart";
// Toggle
const { Panel } = Collapse;
// const { Option } = Select;
function callback(key: any) {
  console.log(key);
}

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const genExtra = () => (
  <SettingOutlined
    onClick={(event) => {
      // If you don't want click extra trigger collapse, you can prevent this:
      event.stopPropagation();
    }}
  />
);

// End Toggle

// Datepicker
const { RangePicker } = DatePicker;
// End Datepicker

// select muilti
const { Option } = Select;
const children: any = [];
for (let i = 10; i < 36; i++) {
  children.push(
    <Option value={i.toString(36) + i} key={i.toString(36) + i}>
      {i.toString(36) + i}
    </Option>
  );
}
// End select muilti

function handleChange(value: any) {
  console.log(`selected ${value}`);
}
const Index = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <Row>
            <Col span={24}>
              <Select
                mode="tags"
                style={{ width: "100%" }}
                placeholder="Tags Mode"
                onChange={handleChange}
              >
                {children}
              </Select>
            </Col>
            <Col span={24} className="top50">
              <Collapse defaultActiveKey={["1"]} onChange={callback}>
                <Panel
                  header="This is panel header 1"
                  key="1"
                  extra={genExtra()}
                >
                  <div>{text}</div>
                </Panel>
                <Panel
                  header="This is panel header 2"
                  key="2"
                  extra={genExtra()}
                >
                  <div>{text}</div>
                </Panel>
                <Panel
                  header="This is panel header 3"
                  key="3"
                  extra={genExtra()}
                >
                  <div>{text}</div>
                </Panel>
              </Collapse>
            </Col>
          </Row>

          <RangePicker showTime className="top50" />

          <TableMqn />

          <TableMqnChart />

          <ProgressMqn />
        </div>
      </Layout>
    </>
  );
};

export default Index;
