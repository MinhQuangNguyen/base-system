import React, {  } from 'react';
import { Table, Badge, Button, Tooltip, Space, Tag, Popconfirm} from 'antd';
import { DeleteOutlined, EyeOutlined,  } from '@ant-design/icons';
import useBaseHooks from 'themes/hooks/BaseHooks';

const TableMqn = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  
  const columns: any[] =[
    {
      title:'Photo',
      dataIndex: 'photo',
      key: 'photo',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      filters: [
        {
          key: 'John Brown',
          text: 'John Brown',
          value: 'John Brown',
  
        },
        {
          key: 'category 1',
          text: 'Category 1',
          value: 'Category 1',
          children: [
            {
              key: 'yellow',
              text: 'Yellow',
              value: 'Yellow',
            },
            {
              key: 'pink',
              text: 'Pink',
              value: 'Pink',
            },
          ],
        },
        {
          key: 'category 2',
          text: 'Category 2',
          value: 'Category 2',
          children: [
            {
              key: 'green',
              text: 'Green',
              value: 'Green',
            },
            {
              key: 'black',
              text: 'Black',
              value: 'Black',
            },
          ],
        },
      ],
      filterMode: 'tree',
      filterSearch: true,
      onFilter: (value: any, record: { name: string | any[]; }) => record.name.includes(value),
    },
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date', 
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type', 
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price', 
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      key: 'brand', 
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
  //  {
  //     title: 'Tags',
  //     key: 'tags',
  //     dataIndex: 'tags',
  //     render: (tags: any[]) => (
  //       <>
  //         {tags.map(tag => {
  //           let color = tag.length > 6 ? 'geekblue' : 'green';
  //           if (tag === 'Stop working') {
  //             color = 'volcano';
  //           }
  //           return (
  //             <Tag color={color} key={tag}>
  //               {tag.toUpperCase()}
  //             </Tag>
  //           );
  //         })}
  //       </>
  //     ),
  //   },
    {
      title: 'Sale',
      dataIndex: 'sale',
      key: 'sale',
      sorter: (a: { sale: number; }, b: { sale: number; }) => a.sale - b.sale,
    },
    // {
    //   title: 'Address',
    //   dataIndex: 'address',
    //   key: 'address',
    //   filters: [
    //     {
    //       text: 'London',
    //       value: 'London',
    //       key: 'london'
    //     },
    //     {
    //       text: 'New York',
    //       value: 'New York',
    //       key: 'new york'
    //     },
    //     {
    //       text: 'Ha Long Bay',
    //       value: 'Ha Long Bay',
    //       key: 'ha long bay'
    //     },
    //     {
    //       text: 'Galaxy',
    //       value: 'Galaxy',
    //       key: 'galaxy'
    //     },
    //   ],
    //   onFilter: (value: any, record: { address: string; }) => record.address.startsWith(value),
    //   filterSearch: true,
    // },
    {
      title:'Action',
      value:'action',
      key: 'action',
       render: (text: any) =>
       <Space size="middle">
        <Tooltip title="View">
          <Button type="default"key="detail" shape="circle" className="buttonViewDetail" icon={<EyeOutlined />}/>
        </Tooltip>
        {/* <Tooltip title="Edit">
          <Button type="primary" key="edit" shape="circle" icon={<EditOutlined />} />
        </Tooltip> */}
        <Tooltip title="Delete">
          <Popconfirm title="Bạn chắc chắn chứ?" okText="Đồng ý" cancelText="Hủy bỏ">
            <Button shape="circle" key="delete" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    }
  ];
  
  const data: any[]  = [
    {
      key: 'John Brown',
      photo: <img src="/static/img/Pizza.png" alt="icon-product" className="img-product" />,
      name: <p>John Brown <br/> #123</p>,
      date:'Sept 10,2021',
      type:'food',
      price:'$99.24',
      brand:'Uniqulo',
      status: <Badge key="1" status="processing" text="Available" />,
      // tags: ['Pending'],
      sale: 32,
      // address: 'New York No. 1 Lake Park',
      action:'',
    },
    {
      key: 'Jim Green',
      name: 'Jim Green',
      status: <Badge key="2" status="processing" text="Stop Time" className="stopTimeMqn"/>,
      tags: ['Stop working'],
      sale: 42,
      address: 'London No. 1 Lake Park',
      action:'',
    },
    {
      key: 'Joe Black',
      name: 'Joe Black',
      status: <Badge key="3" status="processing" text="Active Time" className="activeTimeMqn"/>,
      tags: ['Active'],
      sale: 32,
      address: 'Sidney No. 1 Lake Park',
      action:'',
    },
    {
      key: 'Jim Red',
      name: 'Jim Red',
      status: <Badge key="4" status="processing" text="Active Time" className="activeTimeMqn"/>,
      tags: ['Active'],
      sale: 32,
      address: 'London No. 2 Lake Park',
      action:'',
    },
    {
      key: 'Minh Quang Nguyen',
      name: 'Minh Quang Nguyen',
      status: <Badge key="5" status="processing" text="Pending Time" />,
      tags: ['Pending'],
      sale: 24,
      address: 'Ha Long Bay',
      action:'',
    },
    {
      key: 'Alien Design',
      name: 'Alien Design',
      status: <Badge key="6" status="processing" text="Stop Time" className="stopTimeMqn"/>,
      tags: ['Stop working'],
      sale: 25,
      address: 'Galaxy',
      action:'Stop',
    },
  ];
  
  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log('params', pagination, filters, sorter, extra);
  }
  
  
  // Column checked
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };
  // End Column checked

  return (
    <>
        <div className="title-content-table">
          <div className="title-table">Product Popular</div>
          <img src="/static/img/more.svg" alt="icon-more" />
        </div>
        <Table
          rowSelection={rowSelection}
          columns={columns} 
          dataSource={data} 
          onChange={onChange} 
          // scroll={{ y: 240 }} 
        />
    </>
  );
};

export default TableMqn;
