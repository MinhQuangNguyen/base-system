import Layout from "themes/layouts/Home";
import DemoModal from "./componentModal/DemoModal";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <DemoModal/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
