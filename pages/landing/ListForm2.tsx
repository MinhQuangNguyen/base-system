import Layout from "themes/layouts/Home";
import DemoForm2 from "./componentForm/DemoForm2";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <DemoForm2/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
