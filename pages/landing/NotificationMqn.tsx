import Layout from "themes/layouts/Home";
import DemoNotification from "./componentNotification/DemoNotification";


const NotificationMqn = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <DemoNotification/>
        </div>
      </Layout>
    </>
  );
};

export default NotificationMqn;
