import Layout from "themes/layouts/Home";
import { Calendar, Badge } from "antd";
import useBaseHooks from 'themes/hooks/BaseHooks';


const CalendareMqn = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  function getListData(value: { date: () => any; }) {
    let listData;
    switch (value.date()) {
      case 8:
        listData = [
          { type: "warning", content: t("Calendare.PressConference") },
          { type: "success", content: t("Calendare.HavingLunch") },
        ];
        break;
      case 10:
        listData = [
          { type: "warning", content: t("Calendare.DesignTeamMeeting") },
          { type: "error", content: t("Calendare.MeetWithPartner") },
        ];
        break;
      case 15:
        listData = [
          { type: "warning", content: t("Calendare.PlayGolf") },
          { type: "success", content: t("Calendare.SignTheContract") },
        ];
        break;
      default:
    }
    return listData || [];
  }
  
  function dateCellRender(value: any) {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map((item) => (
          <li key={item.content}>
            <p className={item.type}>{item.content}</p>
          </li>
        ))}
      </ul>
    );
  }
  
  function getMonthData(value: { month: () => number; }) {
    if (value.month() === 8) {
      return 1394;
    }
  }
  
  function monthCellRender(value: any) {
    const num = getMonthData(value);
    return num ? (
      <div className="notes-month">
        <section>{num}</section>
        <span>Backlog number</span>
      </div>
    ) : null;
  }
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <Calendar
            dateCellRender={dateCellRender}
            monthCellRender={monthCellRender}
          />
        </div>
      </Layout>
    </>
  );
};

export default CalendareMqn;
