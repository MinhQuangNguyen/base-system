
import React, { useState } from 'react';
import { Input, Button, Modal, Divider, Row, Col, Checkbox, Statistic, Collapse, Radio } from 'antd';
import TableListCamp from "./TableListCamp";
import useBaseHooks from 'themes/hooks/BaseHooks';

const Step2 = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const { Countdown } = Statistic;
  const deadline = Date.now() + 1000 * 60 * 60 * 24 * 2 + 1000 * 30; // Moment is also OK

  function onFinishTime() {
    console.log('finished!');
  }

  function onChange(val: number) {
    if (4.95 * 1000 < val && val < 5 * 1000) {
      console.log('changed!');
    }
  }

  const { Panel } = Collapse;

  const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };


  return <>
    <div className="item-payment-step2">
      <Row gutter={24}>
        <Col xs={24} xl={16} xxl={16}>
          <TableListCamp />
          <div className="your-saved-card">
            <div className="title">{t("title.SelectPaymentMethod")}</div>
            <Radio.Group name="radiogroup" defaultValue="visa">
              <Collapse defaultActiveKey={['1']}>
                <Panel header={t("title.UsingAnInternationalBankCard")} key="1">
                  <Row gutter={24}>
                    <Col xs={24} xl={12} xxl={8}>
                      <Radio value="visa">
                        <div className="item-international-bank-card">
                          <div className="item-card-international">
                            <img src="/static/img/icon-visa.png" alt="visa" />
                            <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                            <div className="number-card">
                              <span>1011</span>
                              <span>2536</span>
                              <span>1896</span>
                            </div>
                          </div>
                          <div className="item-info-card">
                            <div className="item-card-info">
                              <div className="title">{t("title.CARDHOLDER")}</div>
                              <div className="decs-text">Minh Quang Nguyen</div>
                            </div>
                            <div className="item-card-info content-right">
                              <div className="title">{t("title.EXPIRYDATE")}</div>
                              <div className="decs-text">28/08</div>
                            </div>
                          </div>
                        </div>
                      </Radio>
                    </Col>

                    <Col xs={24} xl={12} xxl={8}>
                      <Radio value="master">
                        <div className="item-international-bank-card backgound-mastercard">
                          <div className="item-card-international">
                            <img src="/static/img/mastercard.png" alt="mastercard" />
                            <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                            <div className="number-card">
                              <span>2202</span>
                              <span>1536</span>
                              <span>8888</span>
                            </div>
                          </div>
                          <div className="item-info-card">
                            <div className="item-card-info">
                              <div className="title">{t("title.CARDHOLDER")}</div>
                              <div className="decs-text">Minh Quang Nguyen</div>
                            </div>
                            <div className="item-card-info content-right">
                              <div className="title">{t("title.EXPIRYDATE")}</div>
                              <div className="decs-text">01/03</div>
                            </div>
                          </div>
                        </div>
                      </Radio>
                    </Col>

                    <Col xs={24} xl={12} xxl={8}>
                      <Radio value="unionpay">
                        <div className="item-international-bank-card backgound-unionpay">
                          <div className="item-card-international">
                            <img src="/static/img/unionpay.png" alt="unionpay" />
                            <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                            <div className="number-card">
                              <span>3022</span>
                              <span>2522</span>
                              <span>3666</span>
                            </div>
                          </div>
                          <div className="item-info-card">
                            <div className="item-card-info">
                              <div className="title">{t("title.CARDHOLDER")}</div>
                              <div className="decs-text">Minh Quang Nguyen</div>
                            </div>
                            <div className="item-card-info content-right">
                              <div className="title">{t("title.EXPIRYDATE")}</div>
                              <div className="decs-text">05/10</div>
                            </div>
                          </div>
                        </div>
                      </Radio>
                    </Col>
                  </Row>

                </Panel>
                <Panel header={t("title.UseADomesticATMCard")}  key="2">
                  <div className="item-list-banking-atm">
                    <Row gutter={24}>
                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="mb-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/mb-bank.png" alt="mb-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="bibv-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/bibv-bank.png" alt="bibv-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="ncb-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/ncb-bank.png" alt="ncb-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="viettin-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/viettin-bank.png" alt="viettin-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="lienviet-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/lienviet-bank.png" alt="lienviet-bank" />
                          </div>
                        </Radio>
                      </Col>
                      
                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="vp-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/vp-bank.png" alt="vp-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="tp-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/tp-bank.png" alt="tp-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="techcombank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/techcombank.png" alt="techcombank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="sacobank">
                          <div className="item-bank-atm">
                            <img src="/static/img/sacobank.png" alt="sacobank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="seabank">
                          <div className="item-bank-atm">
                            <img src="/static/img/seabank.png" alt="seabank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="vietcombank">
                          <div className="item-bank-atm">
                            <img src="/static/img/vietcombank.png" alt="vietcombank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="ACBbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/ACBbank.png" alt="ACBbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="scbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/scbank.png" alt="scbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="hdbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/hdbank.png" alt="hdbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="OCBbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/OCBbank.png" alt="OCBbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="kienlongbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/kienlongbank.png" alt="kienlongbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="oceanbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/oceanbank.png" alt="oceanbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="hsbcbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/hsbcbank.png" alt="hsbcbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="royalbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/royalbank.png" alt="royalbank-bank" />
                          </div>
                        </Radio>
                      </Col>

                      <Col xs={12} xl={6} xxl={4}>
                          <Radio value="shinhanbank-bank">
                          <div className="item-bank-atm">
                            <img src="/static/img/shinhanbank.png" alt="shinhanbank-bank" />
                          </div>
                        </Radio>
                      </Col>
                    </Row>
                  </div>
                </Panel>
                <Panel header={t("title.Using e-wallets")} key="3">
                  <Row gutter={24}>
                    <Col xs={24} xl={12} xxl={8}>
                      <Radio value="americanpay">
                        <div className="item-international-bank-card solopay">
                          <div className="item-card-international">
                            <img src="/static/img/american-pay.png" alt="americanpay" />
                            <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                            <div className="number-card">
                              <span>3025</span>
                              <span>4632</span>
                              <span>1863</span>
                            </div>
                          </div>
                          <div className="item-info-card">
                            <div className="item-card-info">
                              <div className="title">{t("title.CARDHOLDER")}</div>
                              <div className="decs-text">Minh Quang Nguyen</div>
                            </div>
                            <div className="item-card-info content-right">
                              <div className="title">{t("title.EXPIRYDATE")}</div>
                              <div className="decs-text">28/08</div>
                            </div>
                          </div>
                        </div>
                      </Radio>
                    </Col>

                    <Col xs={24} xl={12} xxl={8}>
                      <Radio value="solopay">
                        <div className="item-international-bank-card momo">
                          <div className="item-card-international">
                            <img src="/static/img/icon-solopay.png" alt="solopay" />
                            <div className="text-card-number">{t("title.CARDNUMBER")}</div>
                            <div className="number-card">
                              <span>6868</span>
                              <span>8833</span>
                              <span>1102</span>
                            </div>
                          </div>
                          <div className="item-info-card">
                            <div className="item-card-info">
                              <div className="title">{t("title.CARDHOLDER")}</div>
                              <div className="decs-text">Minh Quang Nguyen</div>
                            </div>
                            <div className="item-card-info content-right">
                              <div className="title">{t("title.EXPIRYDATE")}</div>
                              <div className="decs-text">28/08</div>
                            </div>
                          </div>
                        </div>
                      </Radio>
                    </Col>
                  </Row>
                </Panel> 
              </Collapse>
            </Radio.Group>
          </div>
        </Col>
        <Col xs={24} xl={8} xxl={8}>
          <div className="item-info-right-recipient-account">
            <div className="item-account-information">
              <div className="title-header-main-mqn">
                <img src="/static/img/shield-tick.png" alt="shield" />
                <div className="title">{t("title.Recipient'sAccountInformation")}</div>
              </div>
              <div className="content-detail-account">
                <div className="sub-title">{t("title.AccountHolderInformation")}</div>

                <div className="item-list-info">
                  <div className="sub-title-info">{t("title.Beneficiary'sName")}</div>
                  <div className="decs-text-info">Nguyen Quang Minh</div>
                </div>
                <div className="item-list-info">
                  <div className="sub-title-info">{t("title.AccountNumberMqn")}</div>
                  <div className="decs-text-info">10112229969999</div>
                </div>
                <Divider />
                <div className="sub-title">{t("title.AboutThePayment")}</div>
                <div className="item-list-info">
                  <div className="sub-title-info">{t("title.NameBank")}</div>
                  <div className="decs-text-info">
                    <img src="/static/img/VietcombankMqn.png" alt="vietcombank" />
                  </div>
                </div>
                <div className="item-list-info">
                  <div className="sub-title-info">{t("title.MethodOfPayment")}</div>
                  <div className="decs-text-info">{t("title.FastPayment")}</div>
                </div>
              </div>
            </div>
          </div>

          <div className="item-discount-code">
            <div className="title">{t("title.DiscountCode")}</div>
            <Row gutter={24}>
              <Col xs={18} xl={20} xxl={20}>
                <Input placeholder="Code" />
              </Col>
              <Col xs={6} xl={4} xxl={4}>
                <Button type="primary" style={{ float: 'right' }}>
                {t("Button.Apply")}
                </Button>
              </Col>
              <Col span={24}>
                <Checkbox>{t("title.ExportVATInvoice")}</Checkbox>
              </Col>
            </Row>
          </div>

          <div className="item-total-bill-number-mqn">
            <div className="item-header-bill">
              <div className="title">{t("title.ThisSpecialDiscountWillExpireIn")} <span><Countdown value={deadline} onFinish={onFinishTime} /></span></div>
            </div>
            <div className="item-total-price-bill">

              <div className="item-content-bill">
                <div className="title">{t("title.TotalPrice")}</div>
                <div className="item-info-detail-bill">
                  <div className="sub-title">{t("title.RegularPrice")}</div>
                  <div className="decs-price price-sale">$250</div>
                </div>
                <div className="item-info-detail-bill">
                  <div className="sub-title total-bill-price">{t("title.SOLOOFFER")}</div>
                  <div className="decs-price total-bill-price">$128,081,97</div>
                </div>
                <div className="item-info-detail-bill">
                  <div className="sub-title">{t("title.YouCanSaveAnAdditional655OnThisPolicy")}</div>
                  <div className="decs-price">$600.08(23.99%)</div>
                </div>
                <div className="item-more-document-sale">
                  <div className="decs-text">{t("title.YouSave")} <span onClick={showModal}>{t("Button.SeeMore")}</span></div>
                </div>
                <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                  <p>Some contents...</p>
                  <p>Some contents...</p>
                  <p>Some contents...</p>
                </Modal>
              </div>
              <Divider />
              <div className="item-service-detail-bill">
                <div className="item-service">
                  <img src="/static/img/icon-best1.svg" alt="icon-service-1" />
                  <div className="decs">{t("title.BestPriceGuarantee")}</div>
                </div>
                <div className="item-service">
                  <img src="/static/img/icon-best2.svg" alt="icon-service-2" />
                  <div className="decs">{t("title.100SecurePurcharse")}</div>
                </div>
                <div className="item-service">
                  <img src="/static/img/icon-best3.svg" alt="icon-service-3" />
                  <div className="decs">{t("title.30DayMoneyBackGuarantee")}</div>
                </div>
              </div>
              <Divider />
              <div className="item-show-more-promocode">
                <div className="desc">{t("title.DoYouHaveAPromocode?")} <span>{t("Button.SeeMore")}</span></div>
              </div>
            </div>
          </div>
        </Col>
      </Row>

      <Divider />
    </div>
  </>
}

export default Step2