import React, { useState } from "react";
import {
  Form,
  Input,
  Modal,
  Checkbox,
  Select,
  Divider,
  Row,
  Col,
  DatePicker,
  InputNumber,
  Tooltip,
} from "antd";
import { InfoCircleOutlined, UserOutlined } from "@ant-design/icons";
import useBaseHooks from 'themes/hooks/BaseHooks';

const { TextArea } = Input;

const { RangePicker } = DatePicker;

function onChange(value: any) {
  console.log("changed", value);
}

const { Option } = Select;

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

const Step1 = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const selectBefore = (
    <Select defaultValue="https://" className="select-before">
      <Option value="http://">http://</Option>
      <Option value="https://">https://</Option>
    </Select>
  );

  const selectAfter = (
    <Select defaultValue=".com" className="select-after">
      <Option value=".com">.com</Option>
      <Option value=".jp">.jp</Option>
      <Option value=".cn">.cn</Option>
      <Option value=".org">.org</Option>
      <Option value=".xyz">.xyz</Option>
    </Select>
  );

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="84">+84</Option>
        <Option value="85">+85</Option>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
        <Option value="88">+88</Option>
        <Option value="89">+89</Option>
      </Select>
    </Form.Item>
  );

  return (
    <>
      <div className="list-form-1">
        <Row gutter={24}>
          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              label={t("Input.Label.Project")}
              name="username"
              rules={[{ required: true, message: t("Input.rule.PleaseEnterProjectName") }]}
            >
              <Input
                placeholder={t("Input.Label.Project")}
                suffix={
                  <Tooltip title={t("Input.Label.ProjectNameOfTheInvestor")}>
                    <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                  </Tooltip>
                }
              />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="typeReal"
              label={t("Input.Label.TypeOfRealEstate")}
              rules={[
                { required: true, message: t("Input.rule.PleaseSelectPropertyType") },
              ]}
            >
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                placeholder={t("Input.Label.TypeOfRealEstate")}
                onChange={handleChange}
                optionLabelProp="label"
              >
                <Option value={t("Input.Label.Apartment")} label={t("Input.Label.Apartment")}>
                  <div className="demo-option-label-item">{t("Input.Label.Apartment")}</div>
                </Option>
                <Option value={t("Input.Label.ShopHouse")} label={t("Input.Label.ShopHouse")}>
                  <div className="demo-option-label-item">{t("Input.Label.ShopHouse")}</div>
                </Option>
                <Option value={t("Input.Label.MiniApartment")} label={t("Input.Label.MiniApartment")}>
                  <div className="demo-option-label-item">{t("Input.Label.MiniApartment")}</div>
                </Option>
                <Option value={t("Input.Label.RiverfrontLand")} label={t("Input.Label.RiverfrontLand")}>
                  <div className="demo-option-label-item">{t("Input.Label.RiverfrontLand")}</div>
                </Option>
                <Option value={t("Input.Label.RoadSurfaceLand")} label={t("Input.Label.RoadSurfaceLand")}>
                  <div className="demo-option-label-item">
                    {t("Input.Label.RoadSurfaceLand")}
                  </div>
                </Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="projectLocation "
              label={t("Input.Label.ProjectLocation")}
              rules={[{ required: true, message: t("Input.rule.PleaseSelectProjectLocation") }]}
            >
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                placeholder={t("Input.Label.ProjectLocation")}
                onChange={handleChange}
                optionLabelProp="label"
              >
                <Option value=" Hà Nội" label=" Hà Nội">
                  <div className="demo-option-label-item">Hà Nội</div>
                </Option>
                <Option value=" Bắc Ninh" label=" Bắc Ninh">
                  <div className="demo-option-label-item">Bắc Ninh</div>
                </Option>
                <Option value=" Quảng Ninh " label=" Quảng Ninh ">
                  <div className="demo-option-label-item">Quảng Ninh</div>
                </Option>
                <Option value=" Lạng Sơn" label=" Lạng Sơn">
                  <div className="demo-option-label-item">Lạng Sơn</div>
                </Option>
                <Option value="Hải Phòng" label="Hải Phòng">
                  <div className="demo-option-label-item">Hải Phòng</div>
                </Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="projectSegment "
              label={t("Input.Label.ProjectSegment")}
              rules={[{ required: true, message: t("Input.rule.PleaseSelectProjectSegment") }]}
            >
              <Select
                mode="multiple"
                style={{ width: "100%" }}
                placeholder={t("Input.Label.ProjectSegment")}
                onChange={handleChange}
                optionLabelProp="label"
              >
                <Option value={t("Input.Label.high-class")} label={t("Input.Label.high-class")}>
                  <div className="demo-option-label-item">{t("Input.Label.high-class")}</div>
                </Option>
                <Option value={t("Input.Label.Intermediate")} label={t("Input.Label.Intermediate")}>
                  <div className="demo-option-label-item">{t("Input.Label.Intermediate")}</div>
                </Option>
                <Option value={t("Input.Label.Primary")} label={t("Input.Label.Primary")}>
                  <div className="demo-option-label-item">{t("Input.Label.Primary")}</div>
                </Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="gender"
              label={t("Input.Label.ProjectObjectives")}
              rules={[{ required: true, message: t("Input.rule.PleaseTargetTheProject") }]}
            >
              <Select placeholder={t("Input.Label.ProjectObjectives")} allowClear>
                <Option value={t("Input.Label.MessageAdvertising")}>{t("Input.Label.MessageAdvertising")}</Option>
                <Option value={t("Input.Label.SearchForPotentialCustomers")}>
                  {t("Input.Label.SearchForPotentialCustomers")}
                </Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="phone"
              label={t("Input.Label.PhoneNumber")}
              rules={[
                {
                  pattern: /^(?:\d*)$/,
                  message: t("Input.rule.Malformed"),
                },
                { required: true, message: t("Input.rule.PleaseEnterThePhoneNumber") },
                { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                { max: 13, message: t("Input.rule.EnterUpTo13Characters") },
              ]}
            >
              <Input
                addonBefore={prefixSelector}
                style={{ width: "100%" }}
                placeholder={t("Input.Label.PhoneNumber")}
              />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="datepicker"
              label={t("Input.Label.Time")}
              rules={[
                { required: true, message:  t("Input.rule.PleaseChooseTheTime") },
              ]}
            >
              <RangePicker showTime />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="webiste"
              label={t("Input.Label.WebsiteAddress")}
              rules={[
                { required: true, message: t("Input.rule.PleaseEnterTheWebsiteAddress") },
              ]}
            >
              <Input
                addonBefore={selectBefore}
                addonAfter={selectAfter}
                placeholder={t("Input.Label.WebsiteAddress")}
              />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="totalBudget"
              label={t("Input.Label.TotalCampaignBudget")}
              rules={[
                {
                  pattern: /^(?:\d*)$/,
                  message: t("Input.rule.PleaseEnterOnlyNumbers"),
                },
                {
                  required: true,
                  message: t("Input.rule.PleaseEnterTheTotalCampaignBudget"),
                },
                // {
                //   pattern: /^[\d]{0,50}$/,
                //   message: "Giá trị nhỏ hơn 50",
                // },
              ]}
            >
              <InputNumber
                style={{ width: "100%" }}
                formatter={(value) =>
                  `${value} $`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                onChange={onChange}
                placeholder={t("Input.Label.TotalCampaignBudget")}
              />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="dailyBudget"
              label={t("Input.TotalDailyBudget")}
              rules={[
                {
                  pattern: /^(?:\d*)$/,
                  message: t("Input.rule.PleaseEnterOnlyNumbers"),
                },
                { required: true, message: t("Input.rule.PleaseEnterADailyBudget") },
              ]}
            >
              <InputNumber
                style={{ width: "100%" }}
                formatter={(value) =>
                  `${value} $`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                onChange={onChange}
                placeholder={t("Input.Label.DailyBudget")}
              />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item name="messager" label={t("Input.Label.Message")}>
              <TextArea maxLength={100} placeholder={t("Input.Label.Message")} />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item name="PrivacyPolicy">
              <Checkbox defaultChecked disabled>
                {t("Input.Label.IAgreeToTheInformationSecurityPolicy")}
              </Checkbox>
              <span className="showMorePrivacyPolicy" onClick={showModal}>
                {t("Button.SeeMore")}
              </span>
            </Form.Item>
            <Modal
              title={t("Input.Label.InformationPrivacyPolicy")}
              visible={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
            >
              <h4>{t("Input.Label.About")}</h4>
              <ol>
                <li>{t("Input.Label.textProxy1")}</li>
                <li>{t("Input.Label.textProxy2")}</li>
              </ol>
              <h4>{t("Input.Label.CollectPersonalInformation")}</h4>
              <ol>
                <li>{t("Input.Label.textProxy3")}</li>
                <li>{t("Input.Label.textProxy4")}</li>
                <li>{t("Input.Label.textProxy5")}</li>
                <li>{t("Input.Label.textProxy6")}</li>
                <li>{t("Input.Label.textProxy7")}</li>
                <li>{t("Input.Label.textProxy8")}</li>
              </ol>
            </Modal>
          </Col>

          <Divider />
        </Row>
      </div>
    </>
  );
};

export default Step1;
