import React, { } from 'react';
import { Table, Badge, Button, Tooltip, Space, Tag, Popconfirm } from 'antd';
import useBaseHooks from 'themes/hooks/BaseHooks';

const TableMqnCheckbox = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const columns: any[] = [
    {
      title: t("Table.Project"),
      dataIndex: 'project',
      key: 'project',
      filters: [
        {
          key: 'Vinhome',
          text: 'Vinhome',
          value: 'Vinhome',
        },
        {
          key: 'Universive',
          text: 'Universive',
          value: 'Universive',
        },
        {
          key: 'Ecopark',
          text: 'Ecopark',
          value: 'Ecopark',
        },
      ],
      filterMode: 'tree',
      filterSearch: true,
      onFilter: (value: any, record: { project: string | any[]; }) => record.project.includes(value),
    },


    {
      title: t("Table.StartProject"),
      dataIndex: 'startProject',
      key: 'startProject',
    },
    {
      title: t("Table.EndProject"),
      dataIndex: 'endProject',
      key: 'endProject',
    },
    {
      title: t("Table.TotalBudget"),
      dataIndex: 'totalBudget',
      key: 'totalBudget',
    },
    {
      title: t("Table.DailyBudget"),
      dataIndex: 'dailyBudget',
      key: 'dailyBudget',
    },
  ];

  const data: any[] = [
    {
      key: '1',
      project: 'Vinhome Smart City',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>500$</span></>,
      dailyBudget: '22$'
    },
    {
      key: '2',
      project: 'Vinhome Royal City',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>400$</span></>,
      dailyBudget: '14$'
    },
    {
      key: '3',
      project: 'Vinhome Time City',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>300$</span></>,
      dailyBudget: '12$'
    },
    {
      key: '4',
      project: 'Universive Hạ Long',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>255$</span></>,
      dailyBudget: '25$'
    },
    {
      key: '5',
      project: 'Universive Phú Quốc',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>195$</span></>,
      dailyBudget: '14$'
    },
    {
      key: '6',
      project: 'Ecopark Bắc Ninh ',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>132$</span></>,
      dailyBudget: '5$'
    },

    {
      key: '7',
      project: 'Vinhome Smart City',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>500$</span></>,
      dailyBudget: '22$'
    },
    {
      key: '8',
      project: 'Vinhome Royal City',
      startProject: '15-10-2021 16:36:03',
      endProject: '15-10-2021 16:36:03',
      totalBudget: <><span className='totalBudgetText'>400$</span></>,
      dailyBudget: '14$'
    },
  ];

  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log('params', pagination, filters, sorter, extra);
  }


  // Column checked
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };
  // End Column checked

  return (
    <>
      <div className="table-list-camp">
        <div className="title-content-table">
          <div className="content-title">
            <div className="title">
              <div className="title-table">{t("title.CampaignInitiationList")}</div>
            </div>
            <div className="decs-note">
              <img src="/static/img/warning.png" alt="icon-warning" />
              <div className="decs">{t("title.SelectTheCampaignYouWantToPayFor")}</div>
            </div>
          </div>
        </div>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          onChange={onChange}
        // scroll={{ y: 240 }} 
        />
      </div>
    </>
  );
};

export default TableMqnCheckbox;
