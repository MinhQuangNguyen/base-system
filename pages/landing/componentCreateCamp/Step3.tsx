import { Form, Input, Button, Checkbox, Rate , Divider, Row, Col, DatePicker, InputNumber, Tooltip } from 'antd';
import useBaseHooks from 'themes/hooks/BaseHooks';
import moment from "moment";

const Step3 = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  return <>
   <div className="item-step-success-mqn">
      <div className="item-img-success">
        <img src="/static/img/success-img.png" alt="success-img" />
        <div className="item-name-user">{t("title.Hi")}, Minh Quang Nguyen</div>
        <div className="title-success">{t("title.SuccessfulInitialization")}</div>
        <div className="decs-success">{t("title.decs1")}</div>
        <div className="decs-success">{t("title.decs2")}</div>
        <div className="item-total-data-bill">
          <div className="item-data-total">
            <div className="title">{t("title.CodeBill")}</div>
            <div className="text">11200000553068</div>
          </div>
          <div className="item-data-total">
            <div className="title color-blue">{t("title.TotalCampaignBudget")}</div>
            <div className="text color-blue">$128,088,97</div>
          </div>
          <div className="item-data-total">
            <div className="title">{t("title.InitializationTime")}</div>
            <div className="text">{moment().format("DD-MM-YYYY HH:mm:ss")}</div>
          </div>
          <Divider />
          <div className="decs-rate-bill">{t("title.decs3")}</div>
          <Rate allowHalf defaultValue={4.5} />
        </div>
      </div>
   </div>
   <Divider />
  </>
}

export default Step3