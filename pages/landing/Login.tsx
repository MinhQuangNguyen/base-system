import { Row, Col, Form, Input, Button, Divider, Checkbox, Carousel, Select } from "antd";
import Link from "next/link";
import { route as makeUrl } from "themes/route";
import useBaseHooks from 'themes/hooks/BaseHooks';
import { i18n } from "themes/modules/I18n";

const Login = () => {
  const { t, redirect } = useBaseHooks({ lang: ["common"] });
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const { Option } = Select;

  return (
    <>
      <div className="login-main-mqn">
        <Row>
          <Col xs={24} xl={10} xxl={10} offset={2}>
            <div className="item-form-login-main">
              <Row>
                <Col span={20}>
                  <div className="logo"></div>
                </Col>
                <Col span={4}>
                  <div className="flag-icon-lang">
                    {/* <Button onClick={() => {i18n.changeLanguage('vi'); redirect("localhost:3000/?lang=vi");}}><img src="/static/img/vi-flag.png" alt="lang-flag" /></Button> */}
                    <Select defaultValue={i18n.language} key="vi" bordered={false}>
                      <Option key="vi" value="vi">
                        <img
                          src="/static/img/vi-flag.png"
                          className="langActive"
                          alt="hinhanh"
                          onClick={() => {
                            i18n.changeLanguage("vi");
                            redirect("localhost:3000/?lang=vi");
                          }}
                        />
                      </Option>
                      <Option key="en" value="en">
                        <img
                          src="/static/img/en-flag.png"
                          className="langActive"
                          alt="hinhanh"
                          onClick={() => {
                            i18n.changeLanguage("en");
                            redirect("localhost:3000/?lang=en");
                          }}
                        />
                      </Option>
                    </Select>
                  </div>
                </Col>
              </Row>
              <div className="title">{t("login.Login")}</div>
              <div className="decs-text">{t("login.NewGenerationBusinessManagementSystem")}</div>
              <div className="item-login-google">
                <img src="/static/img/icon-google.png" alt="icon-google" />
                <div className="text">{t("login.SignInWithGoogle")}</div>
              </div>
              <Divider plain>{t("login.orSignInWithEmail")}</Divider>

              <Form
                name="basic"
                initialValues={{ remember: true, prefix: '84', }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                layout="vertical"
              >
                <Row>
                  <Col span={24}>
                    <Form.Item
                      name="email"
                      label={t("Input.Email")}
                      rules={[
                        {
                          type: 'email',
                          message: t("Input.rule.InvalidEmail"),
                        },
                        {
                          required: true,
                          message: t("Input.rule.PleaseEnterYourEmailAddress"),
                        },
                      ]}
                    >
                      <Input placeholder="mail@website.com" allowClear />
                    </Form.Item>
                  </Col>

                  <Col span={24}>
                    <Form.Item
                      label={t("Input.Password")}
                      name="password"
                      rules={[
                        { required: true, message: t("Input.rule.PleaseEnterAPassword") },
                        { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                      ]}
                    >
                      <Input.Password placeholder={t("Input.Min9Character")} allowClear />
                    </Form.Item>
                  </Col>

                  <Col xs={11} xl={12} xxl={12}>
                    <Form.Item name="remember" valuePropName="checked">
                      <Checkbox>{t("Input.RememberMe")}</Checkbox>
                    </Form.Item>
                  </Col>

                  <Col xs={13} xl={12} xxl={12}>
                    <div className="forgot-your-password">{t("Input.ForgotYourPassword")}</div>
                  </Col>

                  <Col span={24}>
                    <Form.Item>
                      <Button type="primary" htmlType="submit">
                        Login
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
              <div className="item-create-account">
                {t("login.NotRegisteredYet")}
                <span>
                  <Link {...makeUrl("frontend.landing.Signup")}>
                    {t("login.CreateAnAccount")}
                  </Link>
                </span>
              </div>

              <div className="item-copyright-login-system">© Copyright 2021 - Solo. All Rights Reserved</div>
            </div>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <div className="item-background-info-img">
              <Carousel autoplay>
                <div className="item-info-img-slide">
                  <img src="/static/img/img-slide-login1.png" alt="slide1" />
                  <div className="content-slide">
                    <div className="title">Turn your ideas into reality.</div>
                    <div className="decs">Consistent quality and experience across all platforms and devices.</div>
                  </div>
                </div>

                <div className="item-info-img-slide">
                  <img src="/static/img/img-slide-login1.png" alt="slide1" />
                  <div className="content-slide">
                    <div className="title">Turn your ideas into reality.</div>
                    <div className="decs">Consistent quality and experience across all platforms and devices.</div>
                  </div>
                </div>

                <div className="item-info-img-slide">
                  <img src="/static/img/img-slide-login1.png" alt="slide1" />
                  <div className="content-slide">
                    <div className="title">Turn your ideas into reality.</div>
                    <div className="decs">Consistent quality and experience across all platforms and devices.</div>
                  </div>
                </div>
              </Carousel>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Login;
