import React from 'react';
import { Table, Badge} from 'antd';
import useBaseHooks from 'themes/hooks/BaseHooks';

const TablePaymenthistory = () => {
  const { t } = useBaseHooks();

  const columns: any[] = [
    {
      title: t("Table.ServicePack"),
      dataIndex: 'service',
      key: 'service',
    },
    {
      title: t("Table.Date"),
      dataIndex: 'date',
      key: 'date',
    },
    {
      title: t("Table.Duration"),
      dataIndex: 'duration',
      key: 'duration',
    },
    {
      title: t("Table.Price"),
      dataIndex: 'price',
      key: 'price',
    },
  
    {
      title: t("Table.Status"),
      dataIndex: 'status',
      key: 'status',
    },
  ];
  
  const data: any[] = [
    {
      key: 1,
      service: <><span className="nameUser text-decs-table-bold">Combo platinum</span> <br /> <span className="tagid">#20009821</span></>,
      date:'25/10/2021',
      duration: '6 mo',
      price: <span className="pricePopular">$99.24</span>,
      status: <><span className="text-status"><Badge key="1" status="processing" /> {t("Table.ActiveSt")}</span> <br /> <span className="tagid">28K {t("Table.ViewInADay")}</span></>,
      action: '',
    },
    {
      key: 2,
      service: <><span className="nameUser text-decs-table-bold">Basic</span> <br /> <span className="tagid">#00013624</span></>,
      date: '24/10/021',
      duration: '3 mo',
      price: <span className="pricePopular">Free</span>,
      status: <><span className="text-status text-Pending"><Badge key="3" status="processing" className="PendingTimeMqn" /> {t("Table.Pending")}</span> <br /> <span className="tagid">8K {t("Table.ViewInADay")}</span></>,
      action: '',
    },
    {
      key: 3,
      service: <><span className="nameUser text-decs-table-bold">Combo Gold</span> <br /> <span className="tagid">#10032142</span></>,
      date: '04/01/2021',
      duration: 'Expired',
      price: <span className="pricePopular">$49.24</span>,
      status: <><span className="text-status text-red"><Badge key="2" status="processing" className="stopTimeMqn" /> {t("Table.StopWorking")}</span> <br /> <span className="tagid">28K {t("Table.ViewInADay")}</span></>,
      action: '',
    }
  ];
  
  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log('params', pagination, filters, sorter, extra);
  }
  return (
    <>
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange} 
      />
    </>
  );
};

export default TablePaymenthistory;
