import Layout from "themes/layouts/Home";
import DemoChartAreaBizChart from "./componentChart/DemoChartAreaBizChart";
import DemoChartSongBizChart from "./componentChart/DemoChartSongBizChart";
import DemoChartColumBizChart from "./componentChart/DemoChartColumBizChart";
import DemoChartcolumngangBizChart from "./componentChart/DemoChartcolumngangBizChart";
import DemoChartShapeBizChart from "./componentChart/DemoChartShapeBizChart";
import DemoChartDoubleaxesBizChart from "./componentChart/DemoChartDoubleaxesBizChart";
import DemoChartColumMiniBizChart from "./componentChart/DemoChartColumMiniBizChart";
import DemoChartGroupedBizChart from "./componentChart/DemoChartGroupedBizChart";
import DemoChartAreaMqnBizChart from "./componentChart/DemoChartAreaMqnBizChart";
import DemoChartAxisColumBizChart from "./componentChart/DemoChartAxisColumBizChart";
import DemoChartpieStyleBizChart from "./componentChart/DemoChartpieStyleBizChart";
const Index = () => {
  return (
    <Layout>
      <div className="site-layout-background">
        <div className="chart-report-dasbroad">
          <DemoChartAreaBizChart />
        </div>
        <DemoChartpieStyleBizChart />
        <DemoChartSongBizChart />
        <DemoChartColumBizChart />
        <DemoChartcolumngangBizChart />
        <DemoChartShapeBizChart />
        <DemoChartDoubleaxesBizChart />
        <DemoChartColumMiniBizChart />
        <DemoChartGroupedBizChart />
        <DemoChartAreaMqnBizChart />
        <DemoChartAxisColumBizChart />

      </div>
    </Layout>
  );
};

export default Index;
