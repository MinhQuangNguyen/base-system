import Layout from "themes/layouts/Home";
import TableMqnOptionChange from "./componentTable/TableMqnOptionChange";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <TableMqnOptionChange/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
