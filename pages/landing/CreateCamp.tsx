import Layout from "themes/layouts/Home";
import { Steps, Button, message, Form } from "antd";
import Step1 from "./componentCreateCamp/Step1";
import Step2 from "./componentCreateCamp/Step2";
import Step3 from "./componentCreateCamp/Step3";
import React from "react";
import useBaseHooks from 'themes/hooks/BaseHooks';


const CreateCamp = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const { Step } = Steps;

const steps = [
  {
    title: t("Input.Label.Establish"),
    description: t("Input.Label.FillInInformation"),
    content: (
      <>
        <Step1 />
      </>
    ),
  },
  {
    title: t("Input.Label.Pay"),
    description: t("Input.Label.BillingInformation"),
    content: (
      <>
        <Step2 />
      </>
    ),
  },
  {
    title: t("Input.Label.Confirm"),
    description: t("Input.Label.CompleteSetup"),
    content: (
      <>
        <Step3 />
      </>
    ),
  },
];

  const [current, setCurrent] = React.useState(0);

  const prev = () => {
    setCurrent(current - 1);
  };

  const onFinish = (values: any) => {
    console.log("Success:", values);
    setCurrent(current + 1);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <Steps current={current}>
            {steps.map((item) => (
              <Step
                key={item.title}
                title={item.title}
                description={item.description}
              />
            ))}
          </Steps>
          <Form
            name="basic"
            initialValues={{ remember: true, prefix: "84" }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <div className="steps-content">{steps[current].content}</div>
            <div className="steps-action">
              {current < steps.length - 1 && (
                <Button htmlType="submit" type="primary" className="nextStep">
                  {t("Button.Next")}
                </Button>
              )}
              {current === steps.length - 1 && (
                <Button
                  type="primary"
                  className="done"
                  onClick={() => message.success(t("Desc.SuccessfulEvaluation"))}
                >
                  {t("Button.Rate")}
                </Button>
              )}
              {current > 0 && (
                <Button
                  style={{ margin: "0 8px" }}
                  className="previous"
                  onClick={() => prev()}
                >
                   {t("Button.Prev")}
                </Button>
              )}
            </div>
          </Form>
        </div>
      </Layout>
    </>
  );
};

export default CreateCamp;
