import Layout from "themes/layouts/Home";
import TableDelete from "./componentTable/TableDelete";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <TableDelete/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
