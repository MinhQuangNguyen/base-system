import Layout from "themes/layouts/Home";
import DemoCompomemLibrary from "./componenLibrary/DemoCompomemLibrary";


const ComponentLibrary = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <DemoCompomemLibrary/>
        </div>
      </Layout>
    </>
  );
};

export default ComponentLibrary;
