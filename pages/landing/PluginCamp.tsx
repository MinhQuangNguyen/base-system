import Layout from "themes/layouts/Home";
import { Row, Col, Button, Progress } from "antd";
import useBaseHooks from "themes/hooks/BaseHooks";
import DemoChartColumBizChartPlugin from "./componenLibrary/DemoChartColumBizChartPlugin";
import moment from "moment";

const PluginCamp = () => {
  const { t } = useBaseHooks();
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <div className="camp-plugin-mqn">
            <Row gutter={24}>
              <Col xs={24} xl={15} xxl={15}>
                <div className="item-header-background-camp">
                  <div className="img-head-camp">
                    <img src="/static/img/img-camp.png" alt="" />
                  </div>
                  <div className="item-content-title-header-camp">
                    <div className="title">{t("Plugin.FullyIntegrated")}</div>
                    <div className="decs-camp">{t("Plugin.Decs")}</div>
                    <Button>{t("Button.GetStardet")}</Button>
                  </div>
                </div>
              </Col>

              <Col xs={24} xl={9} xxl={9}>
                <div className="item-card-bank">
                  <Row gutter={24}>
                    <Col span={14}>
                      <div className="content-title-left">
                        <div className="title-bank">
                          {t("Card.BlanceSumary")}
                        </div>
                        <div className="item-card-bank-detail-text">
                          <div className="title-bank-detail">
                            {t("Card.SolopayBalance")}
                          </div>
                          <div className="text-time">25/10/2021</div>
                          <div className="item-numberbank-pay">
                            <div className="title-number-bank">**2808</div>
                            <div className="sologpay">
                              <img
                                src="/static/img/icon-vi.svg"
                                alt="icon-vi"
                              />
                              <div className="text-option">Solopay</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col span={10}>
                      <div className="item-text-right">
                        <img src="/static/img/icon-add.svg" alt="icon-add" />
                      </div>
                      <div className="item-total-monney-detail-right">
                        <div className="text-total-monney">$50,258.58</div>
                        <div className="text-decs-total-monney">
                        {t("Card.ActiveBalance")}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>

          <div className="item-detail-camp">
            <Row gutter={24}>
              <Col xs={24} xl={15} xxl={15}>
                <div className="list-camp-main">
                  <div className="content-title-camp">
                    <div className="title-camp">
                      {t("Plugin.InteractCampaign")}
                    </div>
                    <div className="show-more">{t("Button.SeeMore")}</div>
                  </div>
                </div>
                <Row gutter={24}>
                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreaseAdEngagement")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $99,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin1.png" alt="plugin1" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>

                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.VoiceNotifications")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $69,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin2.png" alt="plugin2" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>

                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreaseAdEngagement")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $59,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin3.png" alt="plugin3" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>

                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreaseMessageLeed")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $59,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin4.png" alt="plugin4" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>

                <div className="list-camp-main">
                  <div className="content-title-camp">
                    <div className="title-camp">
                      {t("Plugin.OptimalCampaign")}
                    </div>
                    <div className="show-more">{t("Button.SeeMore")}</div>
                  </div>
                </div>
                <Row gutter={24}>
                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreaseClicksToBuy")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $99,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin5.png" alt="plugin5" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>

                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreaseClicksToBuy")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $99,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin6.png" alt="plugin6" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>

                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreasePhoneNumber")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $39,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin7.png" alt="plugin7" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>

                  <Col xs={24} xl={12} xxl={12}>
                    <div className="list-camp">
                      <div className="info-plugins">
                        <div className="title-plugins">
                          {t("Plugin.IncreaseNotifications")}
                        </div>
                        <div className="decs-plugins">
                          {t("Plugin.DecsPlugin")}
                        </div>
                        <div className="price-camp">
                          $29,99 <span>/ {t("Plugin.Mo")}</span>
                        </div>
                      </div>
                      <div className="avata-plugins">
                        <img src="/static/img/plugin8.png" alt="plugin8" />
                        <div className="button-detail">
                          <Button>{t("Button.Detail")}</Button>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col xs={24} xl={9} xxl={9}>
                <div className="item-optimal-usage-main">
                  <div className="title-content">
                    <div className="title">{t("Plugin.OptimalUsageFlow")}</div>
                    <img src="/static/img/more.svg" alt="icon-more" />
                  </div>

                  <div className="item-detial-info-optimal-plugin">
                    <div className="item-progess-plugin-mqn">
                      <Progress type="circle" percent={40} className="sucess" />
                    </div>
                    <div className="item-info-plugin-progess">
                      <div className="title">
                        {t("Plugin.PluginIncreaseYourBudget")}
                      </div>
                      <div className="time-day">{t("Plugin.Today")}</div>
                    </div>
                    <div className="item-time-price-setting-plugin">
                      <div className="price">$521</div>
                      <div className="time-day">
                        {moment().format("DD-MM-YYYY")}
                      </div>
                    </div>
                  </div>

                  <div className="item-detial-info-optimal-plugin">
                    <div className="item-progess-plugin-mqn">
                      <Progress
                        type="circle"
                        percent={60}
                        className="warning"
                      />
                    </div>
                    <div className="item-info-plugin-progess">
                      <div className="title">
                        {t("Plugin.PluginVoiceNotifications")}
                      </div>
                      <div className="time-day">{t("Plugin.Today")}</div>
                    </div>
                    <div className="item-time-price-setting-plugin">
                      <div className="price">$1.221</div>
                      <div className="time-day">
                        {moment().format("DD-MM-YYYY")}
                      </div>
                    </div>
                  </div>

                  <div className="item-detial-info-optimal-plugin">
                    <div className="item-progess-plugin-mqn">
                      <Progress type="circle" percent={80} className="error" />
                    </div>
                    <div className="item-info-plugin-progess">
                      <div className="title">
                        {t("Plugin.PluginIncreaseAdEngagement")}
                      </div>
                      <div className="time-day">{t("Plugin.Today")}</div>
                    </div>
                    <div className="item-time-price-setting-plugin">
                      <div className="price">$2.521</div>
                      <div className="time-day">
                        {moment().format("DD-MM-YYYY")}
                      </div>
                    </div>
                  </div>

                  <div className="item-detial-info-optimal-plugin">
                    <div className="item-progess-plugin-mqn">
                      <Progress type="circle" percent={35} className="good" />
                    </div>
                    <div className="item-info-plugin-progess">
                      <div className="title">
                        {t("Plugin.PluginIncreaseMessageLeed")}
                      </div>
                      <div className="time-day">{t("Plugin.Today")}</div>
                    </div>
                    <div className="item-time-price-setting-plugin">
                      <div className="price">$321</div>
                      <div className="time-day">
                        {moment().format("DD-MM-YYYY")}
                      </div>
                    </div>
                  </div>

                  <div className="item-traffic-social-mqn-plugin">
                    <div className="title">{t("Plugin.TrafficStatistics")}</div>

                    <DemoChartColumBizChartPlugin />
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default PluginCamp;
