import Layout from "themes/layouts/Home";
import TableMqnSearch from "./componentTable/TableMqnSearch";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <TableMqnSearch/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
