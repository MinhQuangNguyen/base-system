import Layout from "themes/layouts/Home";
import TableMqnNested from "./componentTable/TableMqnNested";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <TableMqnNested/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
