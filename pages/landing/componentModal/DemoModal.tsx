import React, { useState } from "react";
import { Descriptions, Badge, Divider, Button, Modal } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import useBaseHooks from 'themes/hooks/BaseHooks';




const DemoModal = () => {

  const { t } = useBaseHooks({ lang: ["common"] });

  function confirm() {
    Modal.confirm({
      title: t("Desc.AreYouSureYouWantToPause"),
      icon: <ExclamationCircleOutlined />,
      content: 'Some descriptions',
      okType: 'danger',
      okText:  t("Button.Ok"),
      cancelText: t("Button.Cancel"),
      onOk() {
        console.log('yes');
      },
      onCancel() {
        console.log('cancel');
      },
    });
  }

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <div className="Modal-mqn">
        <div className="title-content-table">
          <div className="title-table">Table chart</div>
          <img src="/static/img/more.svg" alt="icon-more" />
        </div>
        <Descriptions layout="vertical" bordered>
          <Descriptions.Item label="Product">Cloud Database</Descriptions.Item>
          <Descriptions.Item label="Billing Mode">Prepaid</Descriptions.Item>
          <Descriptions.Item label="Automatic Renewal"><Button onClick={confirm}>{t("Button.Pause")}</Button></Descriptions.Item>
          <Descriptions.Item label="Order time">
            2018-04-24 18:00:00
          </Descriptions.Item>
          <Descriptions.Item label="Usage Time" span={2}>
            2019-04-24 18:00:00
          </Descriptions.Item>
          <Descriptions.Item label="Status" span={3}>
            <Badge status="processing" text="Running" />
          </Descriptions.Item>
          <Descriptions.Item label="Negotiated Amount">
            $80.00
          </Descriptions.Item>
          <Descriptions.Item label="Discount">$20.00</Descriptions.Item>
          <Descriptions.Item label="Official Receipts">
            $60.00
          </Descriptions.Item>
          <Descriptions.Item label="Config Info">
            Data disk type: MongoDB
            <br />
            Database version: 3.4
            <br />
            Package: dds.mongo.mid
            <br />
            Storage space: 10 GB
            <br />
            Replication factor: 3
            <br />
            Region: East China 1<br />
          </Descriptions.Item>
        </Descriptions>

        <Divider />

        <Button style={{ float: "left" }}>{t("Button.Prev")}</Button>
        <Button type="primary" onClick={showModal} style={{ float: "right" }}>
          {t("Button.ViewReport")}
        </Button>
        <Modal
          title="Chi tiết báo cáo"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          okText="Đồng ý"
          cancelText="Hủy bỏ"
          width={1000}
          style={{ top: 20 }}
        >
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
      </div>
    </>
  );
};

export default DemoModal;
