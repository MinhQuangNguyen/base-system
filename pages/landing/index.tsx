import Layout from "themes/layouts/Home";
import { Row, Col,  Menu, Dropdown, Button  } from "antd";
import DemoChartAreaBizChart from "./componentChart/DemoChartAreaBizChart";
import TableMqn from "./TableMqn";
import useBaseHooks from 'themes/hooks/BaseHooks';
import Link from "next/link";
import { route as makeUrl } from "themes/route";

const Index = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const menu = (
    <Menu>
      <Menu.Item>
        <Link {...makeUrl("frontend.landing.index")}>{t("Button.Detail")}</Link>
      </Menu.Item>
      <Menu.Item>
        <Link {...makeUrl("frontend.landing.index")}>{t("Button.InvitePeople")}</Link>
      </Menu.Item>
      <Menu.Item>
        <Link {...makeUrl("frontend.landing.index")}>{t("Button.CopyLink")}</Link>
      </Menu.Item>
    </Menu>
  )

  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <div className="total-monney-header-mqn">
            <Row gutter={24}>
              <Col xs={24} xl={5} xxl={5}>
                <div className="item-total-monney">
                  <div className="icon-total-img">
                    <img src="/static/img/bag-2.svg" alt="icon-bag" />
                  </div>
                  <div className="number-total">
                    <img src="/static/img/arrow-up.svg" alt="icon-arrow-up" />
                    <div className="number-text">
                      +28.08 <span>%</span>
                    </div>
                  </div>
                  <div className="content-main">
                    <div className="number-total-main">$42,302.00</div>
                    <div className="text-number">{t("title.Total.TotalSale")}</div>
                  </div>
                </div>
              </Col>

              <Col xs={24} xl={5} xxl={5}>
                <div className="item-total-monney">
                  <div className="icon-total-img">
                    <img src="/static/img/chart2.svg" alt="icon-bag" />
                  </div>
                  <div className="number-total">
                    <img
                      src="/static/img/arrow-down.svg"
                      alt="icon-arrow-down"
                    />
                    <div className="number-text color-text">
                      -08.08 <span>%</span>
                    </div>
                  </div>
                  <div className="content-main">
                    <div className="number-total-main">$12,200.00</div>
                    <div className="text-number">{t("title.Total.TotalExpenses")}</div>
                  </div>
                </div>
              </Col>

              <Col xs={24} xl={5} xxl={5}>
                <div className="item-total-monney">
                  <div className="icon-total-img">
                    <img src="/static/img/people.svg" alt="icon-bag" />
                  </div>
                  <div className="number-total">
                    <img src="/static/img/arrow-up.svg" alt="icon-arrow-up" />
                    <div className="number-text">
                      +12.80 <span>%</span>
                    </div>
                  </div>
                  <div className="content-main">
                    <div className="number-total-main">$20,821.00</div>
                    <div className="text-number">{t("title.Total.TotalVusitors")}</div>
                  </div>
                </div>
              </Col>

              <Col xs={24} xl={9} xxl={9}>
                <div className="item-card-bank">
                  <Row gutter={24}>
                    <Col span={14}>
                      <div className="content-title-left">
                        <div className="title-bank">{t("Card.BlanceSumary")}</div>
                        <div className="item-card-bank-detail-text">
                          <div className="title-bank-detail">
                            {t("Card.SolopayBalance")}
                          </div>
                          <div className="text-time">25/10/2021</div>
                          <div className="item-numberbank-pay">
                            <div className="title-number-bank">**2808</div>
                            <div className="sologpay">
                              <img
                                src="/static/img/icon-vi.svg"
                                alt="icon-vi"
                              />
                              <div className="text-option">Solopay</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col span={10}>
                      <div className="item-text-right">
                        <img src="/static/img/icon-add.svg" alt="icon-add" />
                      </div>
                      <div className="item-total-monney-detail-right">
                        <div className="text-total-monney">$50,258.58</div>
                        <div className="text-decs-total-monney">
                        {t("Card.ActiveBalance")}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>

          <div className="sale-repost-mqn-main">
            <Row gutter={24}>
              <Col xs={24} xl={15} xxl={15}>
                <div className="chart-report-dasbroad">
                  <DemoChartAreaBizChart />
                </div>
              </Col>
              <Col xs={24} xl={9} xxl={9}>
                <div className="list-user-report-trade">
                  <div className="title-list-user">
                    <div className="title">{t("title.CampaignRequest")}</div>
                    <Dropdown overlay={menu} placement="bottomRight" trigger={['click']}>
                      <img src="/static/img/more.svg" alt="icon-more" />
                    </Dropdown>
                  </div>
                  <div className="item-detail-user-trade">
                    <Row gutter={24}>
                      <Col span={18}>
                        <div className="item-info-avata">
                          <img src="/static/img/avata1.png" alt="avata1" />
                        </div>
                        <div className="item-info-name">
                          <div className="name-user">Quang Minh Nguyen</div>
                          <div className="time-ago">2 {t("Desc.Minutes")}</div>
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="total-money-user-trade">$854.24</div>
                      </Col>
                    </Row>
                  </div>

                  <div className="item-detail-user-trade">
                    <Row gutter={24}>
                      <Col span={18}>
                        <div className="item-info-avata">
                          <img src="/static/img/avata2.png" alt="avata1" />
                        </div>
                        <div className="item-info-name">
                          <div className="name-user">Hoang Duc Vu</div>
                          <div className="time-ago">5 {t("Desc.HoursAgo")}</div>
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="total-money-user-trade">$928.32</div>
                      </Col>
                    </Row>
                  </div>

                  <div className="item-detail-user-trade">
                    <Row gutter={24}>
                      <Col span={18}>
                        <div className="item-info-avata">
                          <img src="/static/img/avata3.png" alt="avata1" />
                        </div>
                        <div className="item-info-name">
                          <div className="name-user">Duy Long Dang</div>
                          <div className="time-ago">7 {t("Desc.HoursAgo")}</div>
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="total-money-user-trade">$358.35</div>
                      </Col>
                    </Row>
                  </div>

                  <div className="item-detail-user-trade">
                    <Row gutter={24}>
                      <Col span={18}>
                        <div className="item-info-avata">
                          <img src="/static/img/avata4.png" alt="avata1" />
                        </div>
                        <div className="item-info-name">
                          <div className="name-user">Viet Duc Nguyen</div>
                          <div className="time-ago">12 {t("Desc.HoursAgo")}</div>
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="total-money-user-trade">$582.36</div>
                      </Col>
                    </Row>
                  </div>

                  <div className="item-detail-user-trade">
                    <Row gutter={24}>
                      <Col span={18}>
                        <div className="item-info-avata">
                          <img src="/static/img/avata5.png" alt="avata1" />
                        </div>
                        <div className="item-info-name">
                          <div className="name-user">Hong Ngoc Vu</div>
                          <div className="time-ago">15 {t("Desc.HoursAgo")}</div>
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="total-money-user-trade">$735.36</div>
                      </Col>
                    </Row>
                  </div>

                </div>
              </Col>
            </Row>
          </div>

          <div className="list-product-popular">
            <Row>
              <Col span={24}>
                <TableMqn />
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default Index;
