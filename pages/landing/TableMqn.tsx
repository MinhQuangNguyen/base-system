import React, { useState } from 'react';
import { Table, Badge, Button, Tooltip, Space, Tag, Popconfirm, Dropdown, Menu } from 'antd';
import Link from "next/link";
import { route as makeUrl } from "themes/route";
import useBaseHooks from 'themes/hooks/BaseHooks';

const TableMqn = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const menu = (
    <Menu>
      <Menu.Item>
        <Link {...makeUrl("frontend.landing.CreateCamp")}>{t("Button.Create")}</Link>
      </Menu.Item>
      <Menu.Item>
        <Link {...makeUrl("frontend.landing.index")}>{t("Button.InvitePeople")}</Link>
      </Menu.Item>
      <Menu.Item>
        <Link {...makeUrl("frontend.landing.index")}>{t("Button.CopyLink")}</Link>
      </Menu.Item>
    </Menu>
  );

  const columns: any[] = [
    {
      title: t("Table.Photo"),
      dataIndex: 'photo',
      key: 'photo',
    },
    {
      title: t("Table.Name"),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: t("Table.Date"),
      dataIndex: 'date',
      key: 'date',
    },
    {
      title: t("Table.Type"),
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: t("Table.Price"),
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: t("Table.Brand"),
      dataIndex: 'brand',
      key: 'brand',
    },
    {
      title: t("Table.Status"),
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: t("Table.Sale"),
      dataIndex: 'sale',
      key: 'sale',
    },
    {
      title: 'Action',
      value: 'action',
      key: 'action',
      render: (_: any, record: { key: string }) =>
        <Space size="middle">
          <Tooltip title={t("Button.ViewDetail")}>
            <Button type="default" key="detail" shape="circle" className="buttonViewDetail"><img src="/static/img/eye.svg" alt="icon-view" /></Button>
          </Tooltip>
          <Tooltip title={t("Button.Delete")}>
            <Popconfirm
              title={t("Button.Confirm")}
              okText={t("Button.Ok")}
              cancelText={t("Button.Cancel")}
              onConfirm={() => handleDelete(record.key)}
            >
              <Button shape="circle" className="buttonDeleteDetail" key="delete">
                <img src="/static/img/delete.svg" alt="icon-delete" />
              </Button>
            </Popconfirm>
          </Tooltip>
        </Space>
    }
  ];

  const [dataSource, setDataSource] = useState([
    {
      key: '1',
      photo: <img src="/static/img/product1.png" alt="icon-product" className="img-product" />,
      name: <><span className="nameUser text-decs-table-bold">Minh Quang Nguyen</span> <br /> <span className="tagid">#20009821</span></>,
      date: <span className="text-decs-table">10/10/2021</span>,
      type: <span className="text-decs-table">Food</span>,
      price: <span className="pricePopular">$99.24</span>,
      brand: <span className="text-decs-table">Uniqulo</span>,
      status: <><span className="text-status"><Badge key="1" status="processing" /> {t("Table.Available")}</span> <br /> <span className="tagid">14K {t("Table.ViewInADay")}</span></>,
      sale: <><span className="nameUser">$8.200</span> <br /> <span className="tagid">{t("Table.SalesInWeeks")}</span></>,
      action: '',
    },
    {
      key: '2',
      photo: <img src="/static/img/product2.png" alt="icon-product" className="img-product" />,
      name: <><span className="nameUser text-decs-table-bold">Hoang Duc Anh</span> <br /> <span className="tagid">#20006582</span></>,
      date: <span className="text-decs-table">11/10/2021</span>,
      type: <span className="text-decs-table">Food</span>,
      price: <span className="pricePopular">$19.75</span>,
      brand: <span className="text-decs-table">Rich</span>,
      status: <><span className="text-status text-red"><Badge key="2" status="processing" className="stopTimeMqn" /> {t("Table.Unavailable")}</span> <br /> <span className="tagid">10K {t("Table.ViewInADay")}</span></>,
      sale: <><span className="nameUser">$6.325</span> <br /> <span className="tagid">{t("Table.SalesInWeeks")}</span></>,
      action: '',
    },
    {
      key: '3',
      photo: <img src="/static/img/product3.png" alt="icon-product" className="img-product" />,
      name: <><span className="nameUser text-decs-table-bold">Hoang Thuy Linh</span> <br /> <span className="tagid">#20005647</span></>,
      date: <span className="text-decs-table">12/10/2021</span>,
      type: <span className="text-decs-table">Food</span>,
      price: <span className="pricePopular">$59.26</span>,
      brand: <span className="text-decs-table">Gucci</span>,
      status: <><span className="text-status text-Pending"><Badge key="3" status="processing" className="PendingTimeMqn" /> {t("Table.Pending")}</span> <br /> <span className="tagid">8K {t("Table.ViewInADay")}</span></>,
      sale: <><span className="nameUser">$7.385</span> <br /> <span className="tagid">{t("Table.SalesInWeeks")}</span></>,
      action: '',
    },
    {
      key: '4',
      photo: <img src="/static/img/product4.png" alt="icon-product" className="img-product" />,
      name: <><span className="nameUser text-decs-table-bold">Len Anh Duc</span> <br /> <span className="tagid">#20005238</span></>,
      date: <span className="text-decs-table">13/10/2021</span>,
      type: <span className="text-decs-table">Food</span>,
      price: <span className="pricePopular">$159.42</span>,
      brand: <span className="text-decs-table">Superme</span>,
      status: <><span className="text-status text-red"><Badge key="4" status="processing" className="stopTimeMqn" /> {t("Table.Unavailable")}</span> <br /> <span className="tagid">28K {t("Table.ViewInADay")}</span></>,
      sale: <><span className="nameUser">$6.324</span> <br /> <span className="tagid">{t("Table.SalesInWeeks")}</span></>,
      action: '',
    },
    {
      key: '5',
      photo: <img src="/static/img/product5.png" alt="icon-product" className="img-product" />,
      name: <><span className="nameUser text-decs-table-bold">Dinh Thuy Linh</span> <br /> <span className="tagid">#20008468</span></>,
      date: <span className="text-decs-table">14/10/2021</span>,
      type: <span className="text-decs-table">Food</span>,
      price: <span className="pricePopular">$99.24</span>,
      brand: <span className="text-decs-table">Uniqulo</span>,
      status: <><span className="text-status"><Badge key="5" status="processing" /> {t("Table.Available")}</span> <br /> <span className="tagid">13K {t("Table.ViewInADay")}</span></>,
      sale: <><span className="nameUser">$3.321</span> <br /> <span className="tagid">{t("Table.SalesInWeeks")}</span></>,
      action: '',
    },
  ]);

  const handleDelete = (key: string) => {
    const newData = dataSource.filter((item) => item.key !== key);
    setDataSource(newData);
  };


  return (
    <>
      <div className="title-content-table">
        <div className="title-table">{t("title.CampaignList")}</div>
        <Dropdown overlay={menu} placement="bottomRight" trigger={['click']}>
          <img src="/static/img/more.svg" alt="icon-more" />
        </Dropdown>
      </div>
      <Table
        columns={columns}
        dataSource={dataSource}
        pagination={false}
      />
    </>
  );
};

export default TableMqn;
