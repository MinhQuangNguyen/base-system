import React from "react";
import { Chart, Tooltip, Interval, Annotation, Legend, Axis } from "bizcharts";

const data = [

  { name: 'Facebook', nation: 'Jan', coin: 18.9 },
  { name: 'Facebook', nation: 'Feb', coin: 28.8 },
  { name: 'Facebook', nation: 'Mar', coin: 39.3 },
  { name: 'Facebook', nation: 'Apr', coin: 71.4 },
  { name: 'Facebook', nation: 'May', coin: 47 },
  { name: 'Facebook', nation: 'Jun', coin: 20.3 },
  { name: 'Facebook', nation: 'Jul', coin: 24 },
  { name: 'Facebook', nation: 'Aug', coin: 35.6 },
  { name: 'Google', nation: 'Jan', coin: 12.4 },
  { name: 'Google', nation: 'Feb', coin: 23.2 },
  { name: 'Google', nation: 'Mar', coin: 34.5 },
  { name: 'Google', nation: 'Apr', coin: 86.7 },
  { name: 'Google', nation: 'May', coin: 52.6 },
  { name: 'Google', nation: 'Jun', coin: 35.5 },
  { name: 'Google', nation: 'Jul', coin: 37.4 },
  { name: 'Google', nation: 'Aug', coin: 42.4 },
];


const axisConfig = {
  label: {
    style: {
      textAlign: "center",
    }, // 设置坐标轴文本样式
  },
  line: {
    style: {
      stroke: "#ffffff",
      fill: "#ffffff",
      lineWidth: 1,
      // lineDash: [3, 3],
    }, // 设置坐标轴线样式
  },
  grid: {
    line: {
      style: {
        stroke: "#edf5ff",
        lineWidth: 1,
        fill: "#ffffff",
        // lineDash: [3, 3],
      },
    }, // 设置坐标系栅格样式
  },
};



const DemoChartColumBizChart = () => (
  <div className="item-chart-plugin">
    <Chart appendPadding={[50, 0, 0, 0]} data={data} autoFit>
      <Interval
        adjust={[
          {
            type: "dodge",
            marginRatio: 0,
          },
        ]}
        color={["name", ["#0075FF", "#24D299"]]}
        position="nation*coin"
      />
      <Tooltip shared />

      <Axis
          name="coin"
          {...axisConfig}
          label={{ offset: 10, formatter: (val) => `${val}M` }}
        />

      <Annotation.Text
        position={["min", "max"]}
        // content="Sale report"
        offsetX={0}
        offsetY={0}
        color="#292930"
        style={{
          fontSize: 22,
          fontWeight: "bold",
          fontFamily: "Nunito",
          fill: "#292930",
        }}
      />

      {/* <Tooltip shared={true} showMarkers={true} /> */}

      <Legend
        position="top-right"
        // itemName={{ formatter: () => 'custname' }} itemValue={{ formatter: () => 323 }}
        itemName={{
          spacing: 10, // 文本同滑轨的距离
          style: {
            fill: "#6E88A9",
            fontFamily: "Nunito",
            fontSize: 16,
          },
        }}
      />

    </Chart>
  </div>
);

export default DemoChartColumBizChart;
