import React from "react";
import { Button, Tabs, Row, Col, Progress } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import TableMqnChartAllJob from "./TableMqnChartAllJob";
import TableMqnChartNewJob from "./TableMqnChartNewJob";
import TableMqnChartMineJob from "./TableMqnChartMineJob";
import TableMqnChartPublishedJob from "./TableMqnChartPublishedJob";
import TableMqnChartDraftsJob from "./TableMqnChartDraftsJob";
import DemoChartpieStyleBizChart from "../componentChart/DemoChartpieStyleBizChart";
import TableRecentOrderComponent from "./TableRecentOrderComponent";
import DemoChartColumBizChart from "./DemoChartColumBizChart";
import useBaseHooks from 'themes/hooks/BaseHooks';

const { TabPane } = Tabs;

const DemoCompomemLibrary = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  return (
  <>
    <Row gutter={24}>
      <Col xs={24} xl={18} xxl={18}>
        <Row gutter={24}>
          <Col xs={24} xl={6} xxl={6}>
            <div className="item-list-dowload-file">
              <div className="item-icon-file">
                <div className="item-icon">
                  <img src="/static/img/document-text.svg" alt="icon-document" />
                </div>
                <div className="item-more">
                  <img src="/static/img/more.svg" alt="icon-more" />
                </div>
              </div>
              <div className="item-process-hour">
                <div className="text-progress">{t("Component.Documents")}</div>
                <Progress percent={80} className="ProgressProfile color-pink" />
              </div>
              <div className="item-info-size-file">
                <div className="total-file">1,328 {t("Component.file")}</div>
                <div className="total-size">1.3GB</div>
              </div>
            </div>
          </Col>
          <Col xs={24} xl={6} xxl={6}>
            <div className="item-list-dowload-file">
              <div className="item-icon-file">
                <div className="item-icon">
                  <img src="/static/img/google-drive.svg" alt="icon-google-drive" />
                </div>
                <div className="item-more">
                  <img src="/static/img/more.svg" alt="icon-more" />
                </div>
              </div>
              <div className="item-process-hour">
                <div className="text-progress">{t("Component.GoogleDrvie")}</div>
                <Progress percent={50} className="ProgressProfile color-yellow" />
              </div>
              <div className="item-info-size-file">
                <div className="total-file">2,329 {t("Component.file")}</div>
                <div className="total-size">2.9GB</div>
              </div>
            </div>
          </Col>
          <Col xs={24} xl={6} xxl={6}>
            <div className="item-list-dowload-file">
              <div className="item-icon-file">
                <div className="item-icon">
                  <img src="/static/img/folder-cloud.svg" alt="icon-folder-cloud" />
                </div>
                <div className="item-more">
                  <img src="/static/img/more.svg" alt="icon-more" />
                </div>
              </div>
              <div className="item-process-hour">
                <div className="text-progress">{t("Component.OneDrive")}</div>
                <Progress percent={60} className="ProgressProfile color-aqua" />
              </div>
              <div className="item-info-size-file">
                <div className="total-file">1,928 {t("Component.file")}</div>
                <div className="total-size">1.7GB</div>
              </div>
            </div>
          </Col>
          <Col xs={24} xl={6} xxl={6}>
            <div className="item-list-dowload-file">
              <div className="item-icon-file">
                <div className="item-icon">
                  <img src="/static/img/dropbox.svg" alt="icon-dropbox" />
                </div>
                <div className="item-more">
                  <img src="/static/img/more.svg" alt="icon-more" />
                </div>
              </div>
              <div className="item-process-hour">
                <div className="text-progress">{t("Component.Dropbox")}</div>
                <Progress percent={30} className="ProgressProfile color-blue" />
              </div>
              <div className="item-info-size-file">
                <div className="total-file">323 {t("Component.file")}</div>
                <div className="total-size">1.1GB</div>
              </div>
            </div>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xs={24} xl={12} xxl={12}>
            <div className="item-recent-order-table">
              <div className="title-content">
                <div className="title">{t("Component.RecentOrder")}</div>
                <img src="/static/img/more.svg" alt="icon-more" />
              </div>
              <TableRecentOrderComponent/>
            </div>
          </Col>
          <Col xs={24} xl={12} xxl={12}>
            <div className="item-analytics-chart-mqn">
              <div className="title-content">
                <div className="title">{t("Component.Analytics")}</div>
              </div>
              <DemoChartColumBizChart/>
            </div>
          </Col>
        </Row>
      </Col>
      <Col xs={24} xl={6} xxl={6}>
        <div className="item-chart-starage-detail">
          <div className="title-content">
            <div className="title">{t("Component.StarageDetails")}</div>
            <img src="/static/img/more.svg" alt="icon-more" />
          </div>
          <div className="item-chart-pie">
            <DemoChartpieStyleBizChart />
            <div className="item-list-file-total">
              <div className="item-icon-file">
                <img src="/static/img/document-text.svg" alt="icon-document" />
              </div>
              <div className="content-file">
                <div className="title-file">{t("Component.Documents")}</div>
                <div className="total-file">1,328 {t("Component.file")}</div>
              </div>
              <div className="content-size">
                <div className="total-size">1.3GB</div>
              </div>
            </div>

            <div className="item-list-file-total">
              <div className="item-icon-file">
                <img src="/static/img/video-play.svg" alt="icon-video" />
              </div>
              <div className="content-file">
                <div className="title-file">{t("Component.Media")}</div>
                <div className="total-file">1,328 {t("Component.file")}</div>
              </div>
              <div className="content-size">
                <div className="total-size">15.1GB</div>
              </div>
            </div>

            <div className="item-list-file-total">
              <div className="item-icon-file">
                <img src="/static/img/folder-favorite.svg" alt="icon-favorite" />
              </div>
              <div className="content-file">
                <div className="title-file">{t("Component.Study")}</div>
                <div className="total-file">1,328 {t("Component.file")}</div>
              </div>
              <div className="content-size">
                <div className="total-size">8.3GB</div>
              </div>
            </div>

            <div className="item-list-file-total">
              <div className="item-icon-file">
                <img src="/static/img/folder-cross.svg" alt="icon-cross" />
              </div>
              <div className="content-file">
                <div className="title-file">{t("Component.Unknow")}</div>
                <div className="total-file">1,328 {t("Component.file")}</div>
              </div>
              <div className="content-size">
                <div className="total-size">2.3GB</div>
              </div>
            </div>
          </div>
        </div>
      </Col>
    </Row>
    <div className="list-component-library">
      <div className="title-content-library">
        <div className="title-library">{t("Component.ListJob")}</div>
        <div className="item-button-new-job">
          <Button type="primary" size="large" icon={<PlusOutlined />}>
           {t("Button.NewJob")}
          </Button>
        </div>
      </div>
      <div className="list-tabs-job-mqn">
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={
              <span>
                {t("Component.AllJob")}
                <span className="TagsTotal">12</span>
              </span>
            }
            key="1"
          >
            <TableMqnChartAllJob />
          </TabPane>

          <TabPane
            tab={
              <span>
                {t("Component.NewJob")}
                <span className="TagsTotal">02</span>
              </span>
            }
            key="2"
          >
            <TableMqnChartNewJob />
          </TabPane>

          <TabPane
            tab={
              <span>
                {t("Component.Mine")}
                <span className="TagsTotal">08</span>
              </span>
            }
            key="3"
          >
            <TableMqnChartMineJob />
          </TabPane>

          <TabPane
            tab={
              <span>
                {t("Component.Published")}
                <span className="TagsTotal">03</span>
              </span>
            }
            key="4"
          >
            <TableMqnChartPublishedJob />
          </TabPane>

          <TabPane
            tab={
              <span>
                {t("Component.Drafts")}
                <span className="TagsTotal">01</span>
              </span>
            }
            key="5"
          >
            <TableMqnChartDraftsJob />
          </TabPane>
        </Tabs>
      </div>
    </div>
  </>
  );
};

export default DemoCompomemLibrary;
