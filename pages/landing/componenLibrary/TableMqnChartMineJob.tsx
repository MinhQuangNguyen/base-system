import React from "react";
import { Chart, Interval, Tooltip } from "bizcharts";
import { Table, Badge, Tag } from "antd";
import ComponentActionTable from "./ComponentActionTable";
import moment from "moment";
import useBaseHooks from "themes/hooks/BaseHooks";

const TableMqnChartMineJob = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const columns: any[] = [
    {
      title: t("Table.Job"),
      dataIndex: "jobtitle",
      key: "jobtitle",
      filters: [
        {
          text: "Dev",
          value: "Dev",
          key: "Dev",
        },
        {
          text: "Design",
          value: "Design",
          key: "Design",
        },
        {
          text: "Task",
          value: "Task",
          key: "Task",
        },
      ],
      filterMode: "tree",
      onFilter: (value: any, record: { jobtitle: string }) =>
        record.jobtitle.startsWith(value),
      filterSearch: true,
    },
    {
      title: t("Table.Menber"),
      dataIndex: "teammenber",
      key: "teammenber",
      sorter: (a: { teammenber: number }, b: { teammenber: number }) =>
        a.teammenber - b.teammenber,
    },
    {
      title: t("Table.Status"),
      dataIndex: "status",
      key: "status",
    },
    {
      title: t("Table.Date"),
      dataIndex: "datemodified",
      key: "datemodified",
    },
    {
      title: t("Table.Candidates"),
      dataIndex: "candidates",
      key: "candidates",
      sorter: (a: { candidates: number }, b: { candidates: number }) =>
        a.candidates - b.candidates,
    },
    {
      title: t("Table.Analytics"),
      dataIndex: "analytics",
      key: "analytics",
      render: (analytics: any[]) => (
        <>
          <div
            style={{
              height: 40,
              padding: "10px 30px",
              width: 100,
              marginLeft: "-30px",
            }}
          >
            <Chart data={analytics} autoFit pure>
              <Interval position="year*analytics" />
              <Tooltip
                linkage="sameKey"
                region={{ start: ["0%", "0%"], end: ["200%", "200%"] }}
                shared
              >
                {(title, items: any) => {
                  return `${items[0].data.year}:${items[0].data.analytics}`;
                }}
              </Tooltip>
            </Chart>
          </div>
        </>
      ),
    },
    {
      title: t("Table.Action"),
      value: "action",
      key: "action",
      render: () => (
        <>
          <ComponentActionTable />
        </>
      ),
    },
  ];

  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log("params", pagination, filters, sorter, extra);
  }

  const data = [
    {
      key: "4",
      jobtitle: "Design UI/UX Company",
      teammenber: 8,
      status: (
        <Badge
          key="Active3"
          status="processing"
          text={t("Table.Active")}
          className="activeTimeMqn"
        />
      ),
      datemodified: <span>{moment().format("DD-MM-YYYY")}</span>,
      candidates: 24,
      analytics: [
        { year: "1951", analytics: 38 },
        { year: "1952", analytics: 24 },
        { year: "1956", analytics: 25 },
        { year: "1957", analytics: 45 },
        { year: "1958", analytics: 76 },
        { year: "1959", analytics: 64 },
        { year: "1960", analytics: 98 },
        { year: "1962", analytics: 19 },
      ],
    },
    {
      key: "5",
      jobtitle: "Design Travel Globle",
      teammenber: 10,
      status: (
        <Badge
          key="Stop2"
          status="processing"
          text={t("Table.StopWorking")}
          className="stopTimeMqn"
        />
      ),
      datemodified: <span>{moment().format("DD-MM-YYYY")}</span>,
      candidates: 23,
      analytics: [
        { year: "1951", analytics: 45 },
        { year: "1952", analytics: 25 },
        { year: "1956", analytics: 12 },
        { year: "1957", analytics: 42 },
        { year: "1958", analytics: 34 },
        { year: "1959", analytics: 25 },
        { year: "1960", analytics: 64 },
        { year: "1962", analytics: 24 },
      ],
    },

    {
      key: "6",
      jobtitle: "Dev Back-end MeeyAds",
      teammenber: 2,
      status: (
        <Badge
          key="Active1"
          status="processing"
          text={t("Table.Active")}
          className="activeTimeMqn"
        />
      ),
      datemodified: <span>{moment().format("DD-MM-YYYY")}</span>,
      candidates: 24,
      analytics: [
        { year: "1951", analytics: 42 },
        { year: "1952", analytics: 54 },
        { year: "1956", analytics: 24 },
        { year: "1957", analytics: 15 },
        { year: "1958", analytics: 24 },
        { year: "1959", analytics: 64 },
        { year: "1960", analytics: 34 },
        { year: "1962", analytics: 24 },
      ],
    },
    {
      key: "7",
      jobtitle: "Design theme MeeyPage",
      teammenber: 1,
      status: (
        <Badge
          key="Syop1"
          status="processing"
          text={t("Table.StopWorking")}
          className="stopTimeMqn"
        />
      ),
      datemodified: <span>{moment().format("DD-MM-YYYY")}</span>,
      candidates: 15,
      analytics: [
        { year: "1951", analytics: 24 },
        { year: "1952", analytics: 34 },
        { year: "1956", analytics: 23 },
        { year: "1957", analytics: 44 },
        { year: "1958", analytics: 23 },
        { year: "1959", analytics: 54 },
        { year: "1960", analytics: 23 },
        { year: "1962", analytics: 16 },
      ],
    },
    {
      key: "8",
      jobtitle: "Task Daily Meet",
      teammenber: 35,
      status: (
        <Badge
          key="Active2"
          status="processing"
          text={t("Table.Active")}
          className="activeTimeMqn"
        />
      ),
      datemodified: <span>{moment().format("DD-MM-YYYY")}</span>,
      candidates: 11,
      analytics: [
        { year: "1951", analytics: 38 },
        { year: "1952", analytics: 52 },
        { year: "1956", analytics: 61 },
        { year: "1957", analytics: 35 },
        { year: "1958", analytics: 48 },
        { year: "1959", analytics: 38 },
        { year: "1960", analytics: 28 },
        { year: "1962", analytics: 18 },
      ],
    },
  ];

  return (
    <>
      <div className="item-table">
        <Table dataSource={data} columns={columns} onChange={onChange} />
      </div>
    </>
  );
};

export default TableMqnChartMineJob;
