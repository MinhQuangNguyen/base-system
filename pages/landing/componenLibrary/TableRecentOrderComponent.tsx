import React, { } from 'react';
import { Table } from 'antd';
import moment from 'moment';
import useBaseHooks from 'themes/hooks/BaseHooks';

const TableRecentOrderComponent = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const columns: any[] = [
    {
      title: t("Table.file"),
      dataIndex: 'img',
      key: 'img',
      width: 65
    },
    {
      title: '',
      dataIndex: 'filename',
      key: 'filename',
      filters: [
        {
          key: 'Figma',
          text: 'Figma',
          value: 'Figma',
  
        },
        {
          key: 'Word',
          text: 'Word',
          value: 'Word',
        },
        {
          key: 'Excel',
          text: 'Excel',
          value: 'Excel',
        },
      ],
      filterMode: 'tree',
      filterSearch: true,
      onFilter: (value: any, record: { filename: string | any[]; }) => record.filename.includes(value),
    },
  
  
    {
      title: t("Table.Date"),
      dataIndex: 'date',
      key: 'date',
    },
  
    {
      title: t("Table.Size"),
      dataIndex: 'size',
      key: 'size',
    },
  ];
  
  const data: any[] = [
    {
      key: '1',
      img: <><img src="/static/img/figma.svg" alt="icon-figma" /></>,
      filename: 'Figma Solo',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '3.5mb',
    },
    {
      key: '2',
      img: <><img src="/static/img/media-file.svg" alt="icon-media" /></>,
      filename: 'Media',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '2.5mb',
    },
    {
      key: '3',
      img: <><img src="/static/img/icon-word.svg" alt="icon-word" /></>,
      filename: 'Word',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '1.4mb',
    },
    {
      key: '4',
      img: <><img src="/static/img/icon-excel.svg" alt="icon-excel" /></>,
      filename: 'Excel',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '3.2mb',
    },
    {
      key: '5',
      img: <><img src="/static/img/figma.svg" alt="icon-figma" /></>,
      filename: 'Figma Media',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '1.4mb',
    },
    {
      key: '6',
      img: <><img src="/static/img/media-file.svg" alt="icon-media" /></>,
      filename: 'Media Music',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '2.5mb',
    },
    {
      key: '7',
      img: <><img src="/static/img/icon-word.svg" alt="icon-word" /></>,
      filename: 'Word Report',
      date: <span>{moment().format("DD-MM-YYYY")}</span>,
      size: '1.4mb',
    },
  ];
  
  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log('params', pagination, filters, sorter, extra);
  }
  


  return (
    <>
      <div className="chartRencentMqn">
        <Table
          columns={columns}
          dataSource={data}
          onChange={onChange}
          scroll={{ y: 390 }}
        />
      </div>
    </>
  );
};

export default TableRecentOrderComponent;
