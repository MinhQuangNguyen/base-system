import React from "react";
import { Button, Tooltip, Space, Popconfirm } from "antd";
import useBaseHooks from "themes/hooks/BaseHooks";

const ComponentActionTable = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  return (
    <>
      <Space size="middle">
        <Tooltip title={t("Button.ViewDetail")}>
          <Button
            type="default"
            key="detail"
            shape="circle"
            className="buttonViewDetail"
          >
            <img src="/static/img/eye.svg" alt="icon-view" />
          </Button>
        </Tooltip>
        <Tooltip title={t("Button.Delete")}>
          <Popconfirm
            title={t("Button.Confirm")}
            okText={t("Button.Ok")}
            cancelText={t("Button.Cancel")}
          >
            <Button shape="circle" className="buttonDeleteDetail" key="delete">
              <img src="/static/img/delete.svg" alt="icon-delete" />
            </Button>
          </Popconfirm>
        </Tooltip>
      </Space>
    </>
  );
};

export default ComponentActionTable;
