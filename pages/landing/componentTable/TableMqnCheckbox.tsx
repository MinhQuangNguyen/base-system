import React from "react";
import { Table, Badge, Button, Tooltip, Space, Tag, Popconfirm } from "antd";
import useBaseHooks from "themes/hooks/BaseHooks";

const TableMqnCheckbox = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const columns: any[] = [
    {
      title: t("Table.Name"),
      dataIndex: "name",
      key: "name",
      filters: [
        {
          key: "John Brown",
          text: "John Brown",
          value: "John Brown",
        },
        {
          key: "category 1",
          text: "Category 1",
          value: "Category 1",
          children: [
            {
              key: "yellow",
              text: "Yellow",
              value: "Yellow",
            },
            {
              key: "pink",
              text: "Pink",
              value: "Pink",
            },
          ],
        },
        {
          key: "category 2",
          text: "Category 2",
          value: "Category 2",
          children: [
            {
              key: "green",
              text: "Green",
              value: "Green",
            },
            {
              key: "black",
              text: "Black",
              value: "Black",
            },
          ],
        },
      ],
      filterMode: "tree",
      filterSearch: true,
      onFilter: (value: any, record: { name: string | any[] }) =>
        record.name.includes(value),
    },

    {
      title: t("Table.Status"),
      dataIndex: "status",
      key: "status",
    },
    {
      title: t("Table.Tags"),
      key: "tags",
      dataIndex: "tags",
      render: (tags: any[]) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 6 ? "geekblue" : "green";
            if (tag === t("Table.StopWorking")) {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: t("Table.Age"),
      dataIndex: "age",
      key: "age",
      sorter: (a: { age: number }, b: { age: number }) => a.age - b.age,
    },
    {
      title: t("Table.Address"),
      dataIndex: "address",
      key: "address",
      filters: [
        {
          text: "London",
          value: "London",
          key: "london",
        },
        {
          text: "New York",
          value: "New York",
          key: "new york",
        },
        {
          text: "Ha Long Bay",
          value: "Ha Long Bay",
          key: "ha long bay",
        },
        {
          text: "Galaxy",
          value: "Galaxy",
          key: "galaxy",
        },
      ],
      onFilter: (value: any, record: { address: string }) =>
        record.address.startsWith(value),
      filterSearch: true,
    },
    {
      title: t("Table.Action"),
      value: "action",
      key: "action",
      render: (text: any) => (
        <Space size="middle">
          <Tooltip title={t("Button.ViewDetail")}>
            <Button
              type="default"
              key="detail"
              shape="circle"
              className="buttonViewDetail"
            >
              <img src="/static/img/eye.svg" alt="icon-view" />
            </Button>
          </Tooltip>
          <Tooltip title={t("Button.Delete")}>
            <Popconfirm
              title={t("Button.Confirm")}
              okText={t("Button.Ok")}
              cancelText={t("Button.Cancel")}
            >
              <Button
                shape="circle"
                className="buttonDeleteDetail"
                key="delete"
              >
                <img src="/static/img/delete.svg" alt="icon-delete" />
              </Button>
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  const data: any[] = [
    {
      key: "John Brown",
      name: "John Brown",
      date: "Sept 10,2021",
      type: "food",
      price: "$99.24",
      brand: "Uniqulo",
      status: (
        <Badge key="1" status="processing" text={t("Table.ActiveTime")} />
      ),
      tags: [t("Table.Pending")],
      age: 32,
      address: "New York No. 1 Lake Park",
      action: "",
    },
    {
      key: "Jim Green",
      name: "Jim Green",
      status: (
        <Badge
          key="2"
          status="processing"
          text={t("Table.OutOfDate")}
          className="stopTimeMqn"
        />
      ),
      tags: [t("Table.StopWorking")],
      age: 42,
      address: "London No. 1 Lake Park",
      action: "",
    },
    {
      key: "Joe Black",
      name: "Joe Black",
      status: (
        <Badge
          key="3"
          status="processing"
          text={t("Table.IsActive")}
          className="activeTimeMqn"
        />
      ),
      tags: [t("Table.Active")],
      age: 32,
      address: "Sidney No. 1 Lake Park",
      action: "",
    },
    {
      key: "Jim Red",
      name: "Jim Red",
      status: (
        <Badge
          key="4"
          status="processing"
          text={t("Table.IsActive")}
          className="activeTimeMqn"
        />
      ),
      tags: [t("Table.Active")],
      age: 32,
      address: "London No. 2 Lake Park",
      action: "",
    },
    {
      key: "Minh Quang Nguyen",
      name: "Minh Quang Nguyen",
      status: (
        <Badge key="5" status="processing" text={t("Table.ActiveTime")} />
      ),
      tags: [t("Table.Pending")],
      age: 24,
      address: "Ha Long Bay",
      action: "",
    },
    {
      key: "Alien Design",
      name: "Alien Design",
      status: (
        <Badge
          key="6"
          status="processing"
          text={t("Table.OutOfDate")}
          className="stopTimeMqn"
        />
      ),
      tags: [t("Table.StopWorking")],
      age: 25,
      address: "Galaxy",
      action: "Stop",
    },
  ];

  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log("params", pagination, filters, sorter, extra);
  }

  // Column checked
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };
  // End Column checked

  return (
    <>
      <div className="table-checkbox-mqn">
        <div className="title-content-table">
          <div className="title-table">{t("Table.tableCheckboxMqn")}</div>
          <img src="/static/img/more.svg" alt="icon-more" />
        </div>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          onChange={onChange}
          // scroll={{ y: 240 }}
        />
      </div>
    </>
  );
};

export default TableMqnCheckbox;
