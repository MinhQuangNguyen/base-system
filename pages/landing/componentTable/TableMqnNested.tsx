import React from "react";
import { Table, Badge, Menu, Dropdown, Space } from 'antd';
import { DownOutlined } from "@ant-design/icons";
import useBaseHooks from 'themes/hooks/BaseHooks';
import moment from "moment";


const TableMqnNested = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const menu = (
    <Menu>
      <Menu.Item>Action 1</Menu.Item>
      <Menu.Item>Action 2</Menu.Item>
    </Menu>
  );

  const expandedRowRender = () => {
    const columns = [
      {
        title: t("Table.Date"),
        dataIndex: 'date',
        key: 'date'
      },
      {
        title: t("Table.Name"),
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: t("Table.Status"),
        key: 'state',
        render: () => (
          <span>
            <Badge key="1" status="processing" text={t("Table.Finished")} />
          </span>
        ),
      },
      {
        title:  t("Table.UpgradeStatus"),
        dataIndex: 'upgradeNum',
        key: 'upgradeNum'
      },
      {
        title: t("Table.Action"),
        dataIndex: 'operation',
        key: 'operation',
        render: () => (
          <Space size="middle">
            <p>{t("Table.Pause")}</p>
            <p>{t("Table.Stop")}</p>
            <Dropdown overlay={menu}>
              <p>
                {t("Table.More")} <DownOutlined />
              </p>
            </Dropdown>
          </Space>
        ),
      },
    ];

    const data = [];
    for (let i = 0; i < 3; ++i) {
      data.push({
        key: i,
        date: <>{moment().format("DD-MM-YYYY  HH:mm:ss")}</>,
        name: 'Minh Quang Nguyen',
        upgradeNum: 'Upgraded: 56',
      });
    }
    return <Table columns={columns} dataSource={data} pagination={false} />;
  };

  const columns = [
    {
      title: t("Table.Name"),
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: t("Table.Platform"),
      dataIndex: 'platform',
      key: 'platform'
    },
    {
      title: t("Table.Version"),
      dataIndex: 'version',
      key: 'version'
    },
    {
      title: t("Table.Upgraded"),
      dataIndex: 'upgradeNum',
      key: 'upgradeNum'
    },
    {
      title: t("Table.Creator"),
      dataIndex: 'creator',
      key: 'creator'
    },
    {
      title: t("Table.Date"),
      dataIndex: 'createdAt',
      key: 'createdAt'
    },
    {
      title: t("Table.Action"),
      key: 'operation',
      render: () => <p>Publish</p>
    },
  ];

  const data: any[] | undefined = [];
  for (let i = 0; i < 3; ++i) {
    data.push({
      key: i,
      name: 'Screem',
      platform: 'iOS',
      version: '10.3.4.5654',
      upgradeNum: 500,
      creator: 'Minh Quang Nguyen',
      createdAt: <>{moment().format("DD-MM-YYYY  HH:mm:ss")}</>,
    });
  }

  return (
    <>
      <div className="table-checkbox-mqn">
        <div className="title-content-table">
          <div className="title-table">{t("Table.tableNestedMqn")}</div>
          <img src="/static/img/more.svg" alt="icon-more" />
        </div>
        <Table
          className="components-table-demo-nested"
          columns={columns}
          expandable={{ expandedRowRender }}
          dataSource={data}
        />
      </div>
    </>
  );
};

export default TableMqnNested;
