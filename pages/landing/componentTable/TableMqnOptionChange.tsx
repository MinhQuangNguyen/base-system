
import { Table, Button } from 'antd';
import useBaseHooks from 'themes/hooks/BaseHooks';


const TableMqnOptionChange = () => {
  const { t } = useBaseHooks({ lang: ["common"] });

  const columns = [
    {
      title: t("Table.Name"),
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: t("Table.Age"),
      dataIndex: 'age',
      key: 'age'
    },
    {
      title: t("Table.Address"),
      dataIndex: 'address',
      key: 'address'
    },
    {
      title: t("Table.Action"),
      dataIndex: '',
      key: 'x',
      render: () => <p>{t("Button.Delete")}</p>
    },
  ];

  const data = [
    { key: 1, name: 'John Brown', age: 32, address: 'New York No. 1 Lake Park', description: 'My name is John Brown, I am 32 years old, living in New York No. 1 Lake Park.' },
    { key: 2, name: 'Jim Green', age: 42, address: 'London No. 1 Lake Park', description: 'My name is Jim Green, I am 42 years old, living in London No. 1 Lake Park.' },
    { key: 3, name: 'Joe Black', age: 32, address: 'Sidney No. 1 Lake Park', description: 'My name is Joe Black, I am 32 years old, living in Sidney No. 1 Lake Park.' },
  ];

  return (
    <>
      <div className="table-checkbox-mqn">
        <div className="title-content-table">
          <div className="title-table">{t("Table.TabeChangeNestedMqn")}</div>
          <img src="/static/img/more.svg" alt="icon-more" />
        </div>
        <Table
          columns={columns}
          expandedRowRender={record => <p>{record.description}</p>}
          dataSource={data}
          expandIconColumnIndex={2}
        />
      </div>
    </>
  );
};

export default TableMqnOptionChange;
