import { Button, Form, Input, Popconfirm, Table } from "antd";
import React, { useContext, useEffect, useRef, useState } from "react";
import useBaseHooks from "themes/hooks/BaseHooks";




const EditableContext = React.createContext(null);

const EditableRow = ({ index, ...props }: any) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}: any) => {
  const [editing, setEditing] = useState(false);

  const inputRef: any = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log("Save failed:", errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

const TableDelete = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const [dataSource, setDataSource] = useState([
    {
      key: "0",
      name: "Minh Quang Nguyen",
      age: "25",
      address: "Hạ Long",
    },
    {
      key: "1",
      name: "Đoàn Tâm Đan",
      age: "27",
      address: "Hà Nội",
    },
  ]);
  // randome age
  const [count, setCount] = useState(2);
  // End random age

  const handleDelete = (key: string) => {
    const newData = dataSource.filter((item) => item.key !== key);
    setDataSource(newData);
  };

  const defaultColumns = [
    {
      title: "Name",
      dataIndex: "name",
      width: "30%",
      editable: true,
      sorter: (a:any, b:any) => a.name.localeCompare(b.name),
    },
    {
      title: "Age",
      dataIndex: "age",
      sorter: (a: { age: number }, b: { age: number }) => a.age - b.age,
    },
    {
      title: "Address",
      dataIndex: "address",
      filters: [
        {
          text: "Hạ Long",
          value: "Hạ Long",
          key: "Hạ Long",
        },
        {
          text: "Hà Nội",
          value: "Hà Nội",
          key: "Hà Nội",
        },
        {
          text: "Hồ Chí Minh",
          value: "Hồ Chí Minh",
          key: "Hồ Chí Minh",
        },
      ],
      onFilter: (value: any, record: { address: string }) =>
      record.address.startsWith(value),
      filterSearch: true,
    },
    {
      title: "Action",
      dataIndex: "operation",
      render: (_: any, record: { key: string }) =>
        dataSource.length >= 1 ? (
          <Popconfirm
            title={t("Button.Confirm")}
            okText={t("Button.Ok")}
            cancelText={t("Button.Cancel")}
            onConfirm={() => handleDelete(record.key)}
          >
            <Button shape="circle" className="buttonDeleteDetail" key="delete">
              <img src="/static/img/delete.svg" alt="icon-delete" />
            </Button>
          </Popconfirm>
        ) : null,
    },
  ];
  //

  // B1: data name
  const researchTitles = [
    "Hoàng Anh Minh",
    "Hoàng Văn Dũng",
    "Tạ Quang Bửu",
    "Nguyễn Quốc Chiểu",
    "Nguyễn Thị Liên",
    "Đào Quốc Cường",
  ];

  const getRandomResearchTitle = () => {
    return researchTitles[Math.floor(Math.random() * researchTitles.length)];
  };

  const [researchTitle, setResearchTitle] = useState(getRandomResearchTitle());
  // End data name

  // B1: data city
  const researchCitys = [
    "Hà Nội",
    "Quảng Ninh",
    "Hải Phòng",
    "Sài Gòn",
    "Hồ Chí Minh",
    "Đà Nẵng",
  ];

  const getRandomResearchCity = () => {
    return researchCitys[Math.floor(Math.random() * researchCitys.length)];
  };

  const [researchCity, setResearchCity] = useState(getRandomResearchCity());
  // End data city

  const handleAdd = () => {
    // B2: Render Random name
    const randomResearchTitle = getRandomResearchTitle();
    setResearchTitle(randomResearchTitle);
    // End Random name

    // B2: Render Random City
    const randomResearchCity = getRandomResearchCity();
    setResearchCity(randomResearchCity);
    // End Random City

    const newData = {
      key: count,
      id: `${count}`,
      name: `${researchTitle}`,
      age: `${count}`,
      address: `${researchCity}`,
    };
    setDataSource([...dataSource, newData]);
    setCount(count + Math.floor(Math.random() * 11));
  };

  const handleSave = (row: {
    key: any;
    name?: string;
    age?: string;
    address?: string;
  }) => {
    const newData = [...dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    setDataSource(newData);
  };

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };
  const columns = defaultColumns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record: any) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave,
      }),
    };
  });
  return (
    <div>
      <Button
        onClick={handleAdd}
        type="primary"
        style={{
          marginBottom: 16,
        }}
      >
        Add a row
      </Button>
      <Table
        components={components}
        rowClassName={() => "editable-row"}
        bordered
        dataSource={dataSource}
        columns={columns}
      />
    </div>
  );
};

export default TableDelete;
