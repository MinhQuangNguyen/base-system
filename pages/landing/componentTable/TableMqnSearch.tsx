import React, { Component } from "react";
import { Table, Input, Button, Space, Tooltip, Popconfirm, Tag } from "antd";
import { SearchOutlined } from "@ant-design/icons";

const data = [
  {
    key: "1",
    name: "Minh Quang Nguyen",
    tags: ['Pending'],
    age: 25,
    address: "Hạ Long Bay",
  },
  {
    key: "2",
    name: "Alien Designer",
    tags: ['Stop working'],
    age: 26,
    address: "Galaxy Black",
  },
  {
    key: "3",
    name: "Vũ Thị Hồng Ngọc",
    tags: ['Active'],
    age: 31,
    address: "Sidney No. 1 Lake Park",
  },
  {
    key: "4",
    name: "Hoàng Đức Anh",
    tags: ['Pending'],
    age: 32,
    address: "London No. 2 Lake Park",
  },
];

class TableMqnSearch extends Component {
  state = {
    searchText: "",
    searchedColumn: "",
  };

  getColumnSearchProps = (dataIndex: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }: any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: any) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (
      value: string,
      record: { [x: string]: { toString: () => string } }
    ) =>
      record[dataIndex]
        ? record[dataIndex]
          .toString()
          .toLowerCase()
          .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible: any) => {
      if (visible) {
      }
    },
  });

  handleSearch = (selectedKeys: any[], confirm: () => void, dataIndex: any) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters: () => void) => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  searchInput: Input | null | undefined;
  render() {
    const columns: any = [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        width: "20%",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "Tag",
        dataIndex: "tags",
        key: "tags",
        render: (tags: any[]) => (
          <>
            {tags.map(tag => {
              let color = tag.length > 6 ? 'geekblue' : 'green';
              if (tag === 'Stop working') {
                color = 'volcano';
              }
              return (
                <Tag color={color} key={tag}>
                  {tag.toUpperCase()}
                </Tag>
              );
            })}
          </>
        ),
        width: "20%",
      },
      {
        title: "Age",
        dataIndex: "age",
        key: "age",
        width: "20%",
        ...this.getColumnSearchProps("age"),
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
        ...this.getColumnSearchProps("address"),
        sorter: (
          a: { address: string | any[] },
          b: { address: string | any[] }
        ) => a.address.length - b.address.length,
        sortDirections: ["descend", "ascend"],
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        width: "20%",
        render: (text: any) =>
          <Space size="middle">
            <Tooltip title="View Detail">
              <Button type="default" key="detail" shape="circle" className="buttonViewDetail"><img src="/static/img/eye.svg" alt="icon-view" /></Button>
            </Tooltip>
            <Tooltip title="Delete">
              <Popconfirm title="Bạn chắc chắn chứ?" okText="Đồng ý" cancelText="Hủy bỏ">
                <Button shape="circle" className="buttonDeleteDetail" key="delete"><img src="/static/img/delete.svg" alt="icon-delete" /></Button>
              </Popconfirm>
            </Tooltip>
          </Space>
      },
    ];
    return (
      <>
        <div className="table-checkbox-mqn">
          <div className="title-content-table">
            <div className="title-table">Table Search</div>
            <img src="/static/img/more.svg" alt="icon-more" />
          </div>
          <Table columns={columns} dataSource={data} />
        </div>
      </>
    );
  }
}

export default TableMqnSearch;
