import { Card, Avatar, Row, Col, notification, Button } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined, InfoCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import { SmileOutlined, FrownOutlined } from '@ant-design/icons';

const close = () => {
  console.log(
    'Notification was closed. Either the close button was clicked or duration time elapsed.',
  );
};

const successnNotification = () => {
  const key = `open${Date.now()}`;
  const btn = (
    <Button type="primary" size="small" onClick={() => notification.close(key)}>
      Xem chi tiết
    </Button>
  );
  notification.open({
    message: 'Cập nhập thông tin thành công',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    icon: <SmileOutlined style={{ color: '#108ee9' }} />,
    btn,
    key,
    onClose: close,
  });
};

const errorNotification = () => {
  const key = `open${Date.now()}`;
  const btn = (
    <Button type="primary" size="small" onClick={() => notification.close(key)}>
      Thử lại
    </Button>
  );
  notification.open({
    message: 'Cập nhập thông tin thất bại',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    icon: <FrownOutlined style={{ color: '#ff4d4f' }} />,
    btn,
    key,
    onClose: close,
  });
};



const { Meta } = Card;

const DemoNotification = () => {
  return <>
    <div className="table-checkbox-mqn">
      <div className="title-content-table">
        <div className="title-table">Card Notification</div>
        <img src="/static/img/more.svg" alt="icon-more" />
      </div>
      <Row gutter={24}>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail1.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata4.png" />}
              title="Minh Quang Nguyen"
              description="This is the description"
            />
          </Card>
        </Col>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail2.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata1.png" />}
              title="Hoàng Anh Minh"
              description="This is the description"
            />
          </Card>
        </Col>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail3.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata2.png" />}
              title="Vũ Thị Hồng Ngọc"
              description="This is the description"
            />
          </Card>
        </Col>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail4.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata5.png" />}
              title="Hoàng Đức Anh"
              description="This is the description"
            />
          </Card>
        </Col>

        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail4.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata1.png" />}
              title="Hoàng Anh Minh"
              description="This is the description"
            />
          </Card>
        </Col>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail3.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata2.png" />}
              title="Hoàng Thùy"
              description="This is the description"
            />
          </Card>
        </Col>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail2.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata4.png" />}
              title="Minh Trí"
              description="This is the description"
            />
          </Card>
        </Col>
        <Col xs={24} xl={6} xxl={6}>
          <Card
            cover={
              <img
                alt="thumbnail1"
                src="/static/img/Thumbnail1.svg"
              />
            }
            actions={[
              <InfoCircleOutlined key="info" onClick={errorNotification} />,
              <SettingOutlined key="success" onClick={successnNotification} />,
              <EditOutlined key="warning" onClick={errorNotification} />,
              <DeleteOutlined key="error" onClick={successnNotification} />,
            ]}
          >
            <Meta
              avatar={<Avatar src="/static/img/avata1.png" />}
              title="Duy Đạt"
              description="This is the description"
            />
          </Card>
        </Col>

      </Row>
    </div>
  </>
}

export default DemoNotification