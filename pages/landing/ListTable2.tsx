import Layout from "themes/layouts/Home";
import TableMqnChart from "./componentTable/TableMqnChart";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <TableMqnChart/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
