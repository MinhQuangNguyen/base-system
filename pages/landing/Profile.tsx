import Layout from "themes/layouts/Home";
import { Col, Row, Avatar, Tooltip, Progress } from "antd";
import DemoChartColumBizChart from "./componentChart/DemoChartColumBizChart";
import DemoChartSongBizChart from "./componentChart/DemoChartSongBizChart";
import DemoChartAreaBizChartProfile from "./componentChart/DemoChartAreaBizChartProfile";
import Link from "next/link";
import { route as makeUrl } from "themes/route";
import useBaseHooks from 'themes/hooks/BaseHooks';

const Profile = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <Row gutter={24}>
            <Col xs={24} xl={6} xxl={5}>
              <div className="public-profile-mqn">
                <div className="title-content">
                  <div className="title">{t("Profile.PublicProfile")}</div>
                  <Link {...makeUrl("frontend.landing.EditProfile")}>
                    <img src="/static/img/iccon-button-edit.svg" alt="icon-edit" />
                  </Link>
                </div>
                <div className="content-info-user-profile-mqn">
                  <div className="avata-profile-mqn-main">
                    <img src="/static/img/avata-profile.png" alt="avata-user" />
                    <div className="tick-online"></div>
                  </div>
                  <div className="profile-name">Minh Quang Nguyen</div>
                  <div className="profile-position">Founder Media Iconic</div>
                </div>
                <div className="content-contact-profile-user">
                  <div className="item-contact-user">
                    <div className="label-contact">{t("Profile.Email")}</div>
                    <div className="text-contact">
                      alien@mediaiconic.com
                    </div>
                  </div>
                  <div className="item-contact-user">
                    <div className="label-contact">{t("Profile.Phone")}</div>
                    <div className="text-contact">0345736211</div>
                  </div>
                  <div className="item-contact-user">
                    <div className="label-contact">{t("Profile.Address")}</div>
                    <div className="text-contact">
                      12 Khuất Duy, Thanh Xuân, TP. Hà Nội
                    </div>
                  </div>
                  <div className="item-contact-user">
                    <div className="label-contact">{t("Profile.Contacts")}</div>
                    <div className="list-avata">
                      <Tooltip title="Admin Solo" placement="top">
                        <Avatar src="/static/img/avata3.png" />
                      </Tooltip>
                      <Tooltip title="Le Anh Duc" placement="top">
                        <Avatar src="/static/img/avata4.png" />
                      </Tooltip>
                      <Tooltip title="Hoang Duc Anh" placement="top">
                        <Avatar src="/static/img/avata5.png" />
                      </Tooltip>
                      <Tooltip title="Minh Quang Nguyen" placement="top">
                        <Avatar src="/static/img/avata1.png" />
                      </Tooltip>
                    </div>
                  </div>
                  <div className="item-contact-user">
                    <div className="label-contact">{t("Profile.ACTIVITY")}</div>
                    <div className="tag-activity">
                      <div className="text">{t("Profile.Active")}</div>
                    </div>
                    <div className="item-process-hour">
                      <div className="text-progress">{t("Profile.2HoursReponseTime")}</div>
                      <Progress percent={88} className="ProgressProfile" />
                    </div>
                  </div>

                  <div className="item-total-data-item-contact">
                    <div className="item-total">
                      <div className="label-item-total">{t("Profile.TOTALVIEW")}</div>
                      <div className="number-item-total">95,288</div>
                    </div>
                    <div className="item-total right">
                      <div className="label-item-total">{t("Profile.MESSAGES")}</div>
                      <div className="number-item-total">324</div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} xl={18} xxl={19}>
              <Row gutter={24}>
                <Col xs={24} xl={16} xxl={16}>
                  <div className="item-chart-colum-profile">
                    <div className="title-content">
                      <div className="title">{t("Profile.SaleReport")}</div>
                      {/* <img src="/static/img/more.svg" alt="icon-more" /> */}
                    </div>
                    <DemoChartColumBizChart />
                  </div>
                </Col>
                <Col xs={24} xl={8} xxl={8}>
                  <div className="total-money-main-mqn">
                    <div className="title-content">
                      <div className="title">{t("Profile.ActivityStart")}</div>
                      <img src="/static/img/more.svg" alt="icon-more" />
                    </div>
                    <div className="item-chart-total-money-main-mqn">
                      <div className="item-chart-song-mqn-main">
                        <DemoChartSongBizChart />
                      </div>
                      <div className="text-decs-total-mqn">
                      {t("Profile.Decs")}
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col xs={24} xl={8} xxl={8}>
                  <div className="item-total-social-data-mqn">
                    <div className="more-action">
                      <img src="/static/img/more.svg" alt="icon-more" />
                    </div>
                    <div className="item-main-social-data">
                      <div className="item-avata-social">
                        <img src="/static/img/facebook.png" alt="icon-social" />
                      </div>
                      <div className="item-content-total-number-mqn">
                        <div className="number-total-monney">$252,212</div>
                        <div className="number-decs">{t("Profile.Like")}</div>
                        <div className="number-monney-total">
                          <img
                            src="/static/img/arrow-up.svg"
                            alt="icon-arrow-up"
                          />
                          <div className="number-text">
                            +28.08 <span>%</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>

                <Col xs={24} xl={8} xxl={8}>
                  <div className="item-total-social-data-mqn">
                    <div className="more-action">
                      <img src="/static/img/more.svg" alt="icon-more" />
                    </div>
                    <div className="item-main-social-data">
                      <div className="item-avata-social">
                        <img src="/static/img/google.png" alt="icon-social" />
                      </div>
                      <div className="item-content-total-number-mqn">
                        <div className="number-total-monney">$120,212</div>
                        <div className="number-decs">{t("Profile.Share")}</div>
                        <div className="number-monney-total">
                          <img
                            src="/static/img/arrow-down.svg"
                            alt="icon-arrow-down"
                          />
                          <div className="number-text color-text">
                            -08.08 <span>%</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>

                <Col xs={24} xl={8} xxl={8}>
                  <div className="item-total-social-data-mqn">
                    <div className="more-action">
                      <img src="/static/img/more.svg" alt="icon-more" />
                    </div>
                    <div className="item-main-social-data">
                      <div className="item-avata-social">
                        <img src="/static/img/youtube.png" alt="icon-social" />
                      </div>
                      <div className="item-content-total-number-mqn">
                        <div className="number-total-monney">$429,236</div>
                        <div className="number-decs">{t("Profile.Like")}</div>
                        <div className="number-monney-total">
                          <img
                            src="/static/img/arrow-up.svg"
                            alt="icon-arrow-up"
                          />
                          <div className="number-text">
                            +12.08 <span>%</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>

                <Col xs={24} xl={24} xxl={24}>
                  <div className="item-chart-area-total-profile-main">
                    <DemoChartAreaBizChartProfile />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </Layout>
    </>
  );
};

export default Profile;
