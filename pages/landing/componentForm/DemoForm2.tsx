
import { Form, Input, Button, Checkbox, Select, DatePicker, Row, Col, Divider } from 'antd';
import { UserOutlined, LockOutlined, PhoneOutlined, MailOutlined } from '@ant-design/icons';
import useBaseHooks from 'themes/hooks/BaseHooks';



const DemoForm2 = () => {

  const { t } = useBaseHooks({ lang: ["common"] });

  const { TextArea } = Input;

  const { Option } = Select;

  const { RangePicker } = DatePicker;

  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const selectBefore = (
    <Select defaultValue="https://" className="select-before">
      <Option value="http://">http://</Option>
      <Option value="https://">https://</Option>
    </Select>
  );

  const selectAfter = (
    <Select defaultValue=".com" className="select-after">
      <Option value=".com">.com</Option>
      <Option value=".jp">.jp</Option>
      <Option value=".cn">.cn</Option>
      <Option value=".org">.org</Option>
    </Select>
  );

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="84">+84</Option>
        <Option value="85">+85</Option>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
        <Option value="88">+88</Option>
        <Option value="89">+89</Option>
      </Select>
    </Form.Item>
  );


  return <>

    <div className="list-form-2">
      <Form
        name="basic"
        initialValues={{ remember: true, prefix: '84', }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label={t("Input.Account")}
              name="username"
              rules={[{ required: true, message: t("Input.rule.PleaseEnterTheAccount") }]}
            >
              <Input placeholder={t("Input.Account")} prefix={<UserOutlined />} allowClear />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              label={t("Input.Password")}
              name="password"
              rules={[{ required: true, message:t("Input.rule.PleaseEnterAPassword") }]}
            >
              <Input.Password placeholder={t("Input.Password")} prefix={<LockOutlined />} allowClear />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              label={t("Input.ConfirmPassword")}
              name="passwordConfirm"
              rules={[{ required: true, message:t("Input.rule.PleaseEnterAPassword") }]}
            >
              <Input.Password placeholder={t("Input.ConfirmPassword")} prefix={<LockOutlined />} allowClear />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="phone"
              label={t("Input.Label.PhoneNumber")}
              rules={[
                {
                  pattern: /^(?:\d*)$/,
                  message: t("Input.rule.Malformed"),
                },
                { required: true, message: t("Input.rule.PleaseEnterThePhoneNumber") },
                { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
                { max: 13, message: t("Input.rule.EnterUpTo13Characters") },
              ]}
            >
              <Input addonBefore={prefixSelector} style={{ width: '100%' }} placeholder="Số điện thoại" prefix={<PhoneOutlined />} allowClear />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="email"
              label={t("Input.Email")}
              rules={[
                {
                  type: 'email',
                  message: t("Input.rule.InvalidEmail"),
                },
                {
                  required: true,
                  message: t("Input.rule.PleaseEnterYourEmailAddress"),
                },
              ]}
            >
              <Input placeholder={t("Input.Email")} prefix={<MailOutlined />} allowClear />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="datepicker"
              label={t("Input.Label.Time")}
              rules={[{ required: true, message: t("Input.rule.PleaseChooseTheTime") }]}
            >
              <RangePicker showTime />
            </Form.Item>
          </Col>

          <Col xs={24} xl={12} xxl={12}>
            <Form.Item
              name="webiste"
              label={t("Input.Label.WebsiteAddress")}
              rules={[{ required: true, message: t("Input.rule.PleaseEnterTheWebsiteAddress") }]}
            >
              <Input addonBefore={selectBefore} addonAfter={selectAfter} placeholder={t("Input.Label.WebsiteAddress")} allowClear />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item name="messager" label={t("Input.Label.Message")} >
              <TextArea maxLength={100} allowClear />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item name="remember" valuePropName="checked">
              <Checkbox>{t("Input.Label.IAgreeToTheInformationSecurityPolicy")}</Checkbox>
            </Form.Item>
          </Col>

          <Divider />

          <Col span={24}>
            <Form.Item>
              <Row gutter={24}>
                <Col span={12}>
                  <Button style={{ float: 'left' }}>
                    {t("Button.Prev")}
                  </Button>
                </Col>
                <Col span={12}>
                  <Button type="primary" htmlType="submit" style={{ float: 'right' }}>
                    {t("Button.Initialization")}
                  </Button>
                </Col>
              </Row>
            </Form.Item>
          </Col>
        </Row>
      </Form>

    </div>
  </>
}

export default DemoForm2