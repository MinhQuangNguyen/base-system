
import { Form, Input, Button, Checkbox, Select, Divider, TreeSelect } from 'antd';
import useBaseHooks from 'themes/hooks/BaseHooks';


const DemoForm1 = () => {

  const { t } = useBaseHooks({ lang: ["common"] });

  const { TextArea } = Input;

  const { Option } = Select;

  function handleChange(value: any) {
    console.log(`selected ${value}`);
  }


  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="84">+84</Option>
        <Option value="85">+85</Option>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
        <Option value="88">+88</Option>
        <Option value="89">+89</Option>
      </Select>
    </Form.Item>
  );

  const { SHOW_PARENT } = TreeSelect;

  const treeData = [
    {
      title: 'Darkwear',
      value: '0-0',
      key: '0-0',
      children: [
        {
          title: 'Rickowen',
          value: '0-0-0',
          key: '0-0-0',
        },
      ],
    },
    {
      title: 'Streatwear',
      value: '0-1',
      key: '0-1',
      children: [
        {
          title: 'OFF-White',
          value: '0-1-0',
          key: '0-1-0',
        },
        {
          title: 'Superme',
          value: '0-1-1',
          key: '0-1-1',
        },
        {
          title: 'Bage',
          value: '0-1-2',
          key: '0-1-2',
        },
      ],
    },
  ];

  const tProps = {
    treeData,
    treeCheckable: true,
    showCheckedStrategy: SHOW_PARENT,
    placeholder: t("Input.ChooseYourStyle"),
    style: {
      width: '100%',
    },
  };

  return <>
    <div className="list-form-1">
      <Form
        name="basic"
        initialValues={{ remember: true, prefix: '84', }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
      >
        <Form.Item
          label={t("Input.Account")}
          name="username"
          rules={[{ required: true, message: t("Input.rule.PleaseEnterTheAccount") }]}
        >
          <Input placeholder={t("Input.Account")} allowClear />
        </Form.Item>

        <Form.Item
          label={t("Input.Password")}
          name="password"
          rules={[{ required: true, message: t("Input.rule.PleaseEnterAPassword") }]}
        >
          <Input.Password placeholder={t("Input.Password")} allowClear />
        </Form.Item>

        <Form.Item
          label={t("Input.ConfirmPassword")}
          name="passwordConfirm"
          rules={[{ required: true, message: t("Input.rule.PleaseEnterAPassword") }]}
        >
          <Input.Password placeholder={t("Input.ConfirmPassword")} allowClear />
        </Form.Item>

        <Form.Item
          name="phone"
          label={t("Input.Label.PhoneNumber")}
          rules={[
            {
              pattern: /^(?:\d*)$/,
              message: t("Input.rule.Malformed"),
            },
            { required: true, message: t("Input.rule.PleaseEnterThePhoneNumber") },
            { min: 9, message: t("Input.rule.EnterAtLeast9Characters") },
            { max: 13, message: t("Input.rule.EnterUpTo13Characters") },
          ]}
        >
          <Input addonBefore={prefixSelector} style={{ width: '100%' }} placeholder={t("Input.Label.PhoneNumber")} allowClear />
        </Form.Item>


        <Form.Item
          name="email"
          label={t("Input.Email")}
          rules={[
            {
              type: 'email',
              message: t("Input.rule.InvalidEmail"),
            },
            {
              required: true,
              message: t("Input.rule.PleaseEnterYourEmailAddress"),
            },
          ]}
        >
          <Input placeholder={t("Input.Email")} allowClear />
        </Form.Item>

        <Form.Item name="gender" label={t("Input.RoleSelection")} rules={[{ required: true, message:  t("Input.rule.PleaseSelectTheRole") }]}>
          <Select
            placeholder={t("Input.RoleSelection")}
            allowClear
          >
            <Option value={t("Input.Developer")}>{t("Input.Developer")}</Option>
            <Option value={t("Input.Designer")}>{t("Input.Designer")}</Option>
            <Option value={t("Input.ProjectManager")}>{t("Input.ProjectManager")}</Option>
          </Select>
        </Form.Item>

        <Form.Item name="category" label={t("Input.GenreSelection")} rules={[{ required: true, message: t("Input.rule.PleaseSelectCategory") }]}>
          <Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder={t("Input.GenreSelection")}
            onChange={handleChange}
            optionLabelProp="label"
            allowClear
          >
            <Option value={t("Input.Design3D")} label={t("Input.Design3D")}>
              <div className="demo-option-label-item">
                {t("Input.Design3D")}
              </div>
            </Option>
            <Option value={t("Input.Design2D")} label={t("Input.Design2D")}>
              <div className="demo-option-label-item">
                {t("Input.Design2D")}
              </div>
            </Option>
            <Option value={t("Input.DesignUIUX")} label={t("Input.DesignUIUX")}>
              <div className="demo-option-label-item">
                {t("Input.DesignUIUX")}
              </div>
            </Option>
            <Option value={t("Input.Developer")} label={t("Input.Developer")}>
              <div className="demo-option-label-item">
                {t("Input.Developer")}
              </div>
            </Option>
            <Option value={t("Input.ProjectManager")} label={t("Input.ProjectManager")}>
              <div className="demo-option-label-item">
                {t("Input.ProjectManager")}
              </div>
            </Option>
          </Select>
        </Form.Item>


        <Form.Item
          name="styleMqn"
          label={t("Input.ChooseYourStyle")}
          rules={[{ required: true, message: t("Input.rule.PleaseChooseStyle") }]}
        >
          <TreeSelect 
            allowClear
            {...tProps} 
            filterTreeNode={(search, item) => {
              return item.title.toLowerCase().indexOf(search.toLowerCase()) >= 0;
            }}
          />
        </Form.Item>

        <Form.Item name="messager" label={t("Input.Label.Message")} >
          <TextArea maxLength={100} allowClear />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked">
          <Checkbox>{t("Input.Label.IAgreeToTheInformationSecurityPolicy")}</Checkbox>
        </Form.Item>

        <Divider />

        <Form.Item>
          <Button style={{ float: 'left' }}>
            {t("Button.Prev")}
          </Button>
          <Button type="primary" htmlType="submit" style={{ float: 'right' }}>
            {t("Button.Initialization")}
          </Button>
        </Form.Item>
      </Form>
    </div>
  </>
}

export default DemoForm1