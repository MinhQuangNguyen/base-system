
import React, { useState } from 'react';
import { Upload, message } from 'antd';




function beforeUpload(file: { type: string; size: number; }) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('Bạn chỉ có thể tải lên tệp JPG / PNG!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Hình ảnh phải nhỏ hơn 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const UploadMqn = () => {

  const [fileList, setFileList]: any[] = useState([
    {
      uid: '1',
      name: 'image.png',
      status: 'done',
      url: '/static/img/avata-profile.png',
    },
  ]);

  const onChange = ({ fileList: newFileList }: any) => {
    setFileList(newFileList);
  };


  return <>
    <Upload
      action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
      listType="picture-card"
      fileList={fileList}
      beforeUpload={beforeUpload}
      onChange={onChange}
    >
      {fileList.length < 1 && 'Avata'}
    </Upload>
  </>
}

export default UploadMqn