import React from 'react';
import { Chart, Interval, LineAdvance } from 'bizcharts';

const data = [
  { genre: 'Sports', sold: 275 },
  { genre: 'Strategy', sold: 155 },
  { genre: 'Action', sold: 230 },
  { genre: 'Shooter', sold: 250 },
  { genre: 'Other', sold: 450 },

  { genre: 'Sports1', sold: 275 },
  { genre: 'Strategy1', sold: 155 },
  { genre: 'Action1', sold: 230 },
  { genre: 'Shooter1', sold: 250 },
  { genre: 'Other1', sold: 450 }
];




const DemoChartColumMiniBizChart = () => (
  <>
  <div style={{ width: 200, height: 50, margin: '50px auto' }}>
    <Chart data={data} autoFit pure >
      <Interval position="genre*sold" />
    </Chart>
  </div>
  </>
);

export default DemoChartColumMiniBizChart;