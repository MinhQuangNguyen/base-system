import React from "react";
import {
  Chart,
  Tooltip,
  Interval,
  Effects,
  Legend,
  Guide,
  Axis,
} from "bizcharts";

const DemoChartSongBizChart = () => {
  const { Text } = Guide;

  const data = [
    {
      key: "1",
      gender: "male",
      value: 34,
    },
  ];

  const scale = {
    value: {
      key: "2",
      min: 0,
      max: 100,
    },
  };

  return (
    <>
      <Chart data={data} width={200} height={200} padding={0} scale={scale}>
        <Tooltip />
        <Axis visible={false} />
        <Interval
          position="gender*value"
          color="#369FFF"
          shape="liquid-fill-gauge"
          style={{
            lineWidth: 10,
            fillOpacity: 0.75,
          }}
          size={160}
          customInfo={{}}
        />
        <Legend visible={false} />
        <Effects>
          {(chart: {
            geometries: {
              customInfo: (arg0: {
                radius: number;
                outline: { border: number; distance: number };
                wave: { count: number; length: number };
              }) => void;
            }[];
          }) => {
            chart.geometries[0].customInfo({
              radius: 0.9,
              outline: { border: 2, distance: 0 },
              wave: { count: 3, length: 192 },
            });
          }}
        </Effects>
        <Guide>
          {data.map((row) => (
            <Text
              content={`${row.value}%`}
              key="song"
              top
              position={{
                gender: row.gender,
                value: 50,
              }}
              style={{
                opacity: 1,
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 26,
                lineHeight: 32,
                fontFamily: "Nunito",
                fill: "#292930",
              }}
            />
          ))}
        </Guide>
      </Chart>
    </>
  );
};

export default DemoChartSongBizChart;
