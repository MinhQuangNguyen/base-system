import React from "react";
import { Chart, Geom, Axis, Area } from "bizcharts";
import moment from 'moment';

const data = [
  { city: "Hà Nội", value: "10" },
  { city: "Hải Phòng", value: "23" },
  { city: "Lạng Sơn", value: "35" },
  { city: "Hạ Long", value: "20" },
  { city: "Yên Bái", value: "46" },
  { city: "Bắc Ninh", value: "25" },
  { city: "Hà Nam", value: "15" },
  { city: "Thái Bình", value: "60" },
  { city: "Nam Định", value: "43" },
];

const scale = {
  // city: {type: 'cat'},
  value: {
    type: "linear",
    formatter: (val: string) => {
      return val + "%";
    },
    tickCount: 5,
    ticks: ["0", "25", "50", "75", "100"],
  },
};
const DemoChartAreaMqnBizChart = () => (
  <>
    <Chart
      autoFit
      height={400}
      data={data.map((item) => {
        const city = moment(item.city).format("YYYY-MM-DD");
        return Object.assign({ city }, item);
      })}
      scale={scale}
    >

      <Axis name="city" />
      <Axis name="value" />
      <Area position="city*value"

        shape="smooth" color="l (270) 0:rgba(255, 146, 255, 1) .5:rgba(100, 268, 255, 1) 1:rgba(215, 0, 255, 1)" />
      {/* <Geom type="line" position="city*value" shape="smooth" /> */}
    </Chart>
  </>
);

export default DemoChartAreaMqnBizChart;
