import React from "react";
import {
	Chart,
	Interval,
	Tooltip,
	Axis,
	Coordinate,
	getTheme,
} from "bizcharts";


const data = [
	{
		country: "Việt Nam",
		population: 131744
	},
	{
		country: "Thái Lan",
		population: 104970
	},
	{
		country: "Singapore",
		population: 29034
	},
	{
		country: "Tây Ban Nha",
		population: 23489
	},
	{
		country: "Pháp",
		population: 18203
	}
];
data.sort((a, b) => a.population - b.population)

const DemoChartcolumngangBizChart = () => (
  <>
	<Chart height={400} data={data} autoFit>
		<Coordinate transpose />
		<Interval position="country*population" />
	</Chart>
  </>
);

export default DemoChartcolumngangBizChart;
