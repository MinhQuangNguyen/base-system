import React from "react";
import { Chart, Tooltip, Axis, Interaction, Legend, Interval } from "bizcharts";

const data = [
  { year: '2013', value: -3.1, marked: true },
  { year: '2014', value: 0.8, marked: true },
  { year: '2015', value: 2.3, marked: false },
  { year: '2016', value: -3.5, marked: false },
  { year: '2017', value: 5.4, marked: true },
  { year: '2018', value: -2.3, marked: false },
  { year: '2019', value: 3.5, marked: false },
  { year: '2020', value: 4.4, marked: true },
  { year: '2021', value: 5.3, marked: false },
  { year: '2022', value: -1.5, marked: false },
  { year: '2023', value: -7.4, marked: true },
  { year: '2024', value: -3.4, marked: true },
  { year: '2025', value: 4.3, marked: false },
  { year: '2026', value: 6.5, marked: false },
  { year: '2027', value: -1.4, marked: true },
];

const scale = {
  'value': {
    alias: '现金流(亿)',
    nice: true,
    formatter: (val: any) => {
      return `${val} 亿`;
    }
  }
}




const DemoChartAxisColumBizChart = () => (
  <>
    <Chart
      padding={[30, 20, 50, 70]}
      autoFit
      height={500}
      data={data}
      scale={scale}
    >
      <Axis name="year" />
 
      < Interaction type="active-region" />

      <Tooltip showCrosshairs showMarkers={false} />
      <Interval position="year*value" color={
        ['year*value*marked', (year, value, marked) => {
          return marked ? '#24D299' : '#2697FE';
        }]} label={
          ['year*value', (year, value) => {
            return {
              content: (originData) => {
                if (originData.year === '2014') {
                  return null;
                }
                return value;
              },
            };
          }]} />
      <Legend visible={false} />
    
    </Chart>
  </>
);

export default DemoChartAxisColumBizChart;
