import React from "react";
import {
  Chart,
  LineAdvance,
  Legend,
  Annotation,
  Axis,
  Tooltip,
} from "bizcharts";
import useBaseHooks from "themes/hooks/BaseHooks";

const DemoChartAreaBizChartProfile = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const data = [
    {
      month: "Jan",
      social: "Google",
      temperature: 7,
    },
    {
      month: "Jan",
      social: "Facebook",
      temperature: 3.9,
    },
    {
      month: "Feb",
      social: "Google",
      temperature: 13,
    },
    {
      month: "Feb",
      social: "Facebook",
      temperature: 4.2,
    },
    {
      month: "Mar",
      social: "Google",
      temperature: 16.5,
    },
    {
      month: "Mar",
      social: "Facebook",
      temperature: 5.7,
    },
    {
      month: "Apr",
      social: "Google",
      temperature: 14.5,
    },
    {
      month: "Apr",
      social: "Facebook",
      temperature: 8.5,
    },
    {
      month: "May",
      social: "Google",
      temperature: 10,
    },
    {
      month: "May",
      social: "Facebook",
      temperature: 11.9,
    },
    {
      month: "Jun",
      social: "Google",
      temperature: 7.5,
    },
    {
      month: "Jun",
      social: "Facebook",
      temperature: 15.2,
    },
    {
      month: "Jul",
      social: "Google",
      temperature: 9.2,
    },
    {
      month: "Jul",
      social: "Facebook",
      temperature: 17,
    },
    {
      month: "Aug",
      social: "Google",
      temperature: 14.5,
    },
    {
      month: "Aug",
      social: "Facebook",
      temperature: 16.6,
    },
    {
      month: "Sep",
      social: "Google",
      temperature: 9.3,
    },
    {
      month: "Sep",
      social: "Facebook",
      temperature: 14.2,
    },
    {
      month: "Oct",
      social: "Google",
      temperature: 8.3,
    },
    {
      month: "Oct",
      social: "Facebook",
      temperature: 10.3,
    },
    {
      month: "Nov",
      social: "Google",
      temperature: 8.9,
    },
    {
      month: "Nov",
      social: "Facebook",
      temperature: 5.6,
    },
    {
      month: "Dec",
      social: "Google",
      temperature: 5.6,
    },
    {
      month: "Dec",
      social: "Facebook",
      temperature: 9.8,
    },
  ];

  const axisConfig = {
    label: {
      style: {
        textAlign: "center",
      }, // 设置坐标轴文本样式
    },
    line: {
      style: {
        stroke: "#ffffff",
        fill: "#ffffff",
        lineWidth: 1,
        // lineDash: [3, 3],
      }, // 设置坐标轴线样式
    },
    grid: {
      line: {
        style: {
          stroke: "#edf5ff",
          lineWidth: 1,
          fill: "#ffffff",
          // lineDash: [3, 3],
        },
      }, // 设置坐标系栅格样式
    },
  };

  const scale = {
    temperature: { min: 0 },
  };
  return (
    <>
      <div className="titleChartArea">{t("Profile.CustomerAnalytics")}</div>
      <div className="chartAreaMqn">
        <Chart appendPadding={[50, 0, 0, 0]} autoFit data={data} scale={scale}>
          <LineAdvance
            shape="smooth"
            point
            area
            position="month*temperature"
            color={["social", ["#0075FF", "#24d299"]]}
            size={3}
            // label="temperature"
            // size={['count', [1, 10]]}
          />

          <Axis
            name="temperature"
            {...axisConfig}
            label={{ offset: 10, formatter: (val) => `${val}M` }}
          />

          <Annotation.Text
            position={["min", "max"]}
            // content="Sale report"
            offsetX={0}
            offsetY={-25}
            color="#292930"
            style={{
              fontSize: 22,
              fontWeight: "bold",
              fontFamily: "Nunito",
              fill: "#292930",
            }}
          />
          <Tooltip shared={true} showMarkers={true} />

          <Legend
            position="top-right"
            marker={{
              symbol: "circle",
              style: {
                fill: "white",
                lineWidth: 2,
                fontWeight: "bold", // 文本粗细
              },
            }}
            itemName={{
              spacing: 5, // 文本同滑轨的距离
              style: {
                // stroke: 'blue',
                fill: "#6E88A9",
                fontFamily: "Nunito",
                fontSize: 16,
              },
            }}
          />
          {/* <Legend 
        position="top-left"
        // itemName={{ formatter: () => 'custname' }} itemValue={{ formatter: () => 323 }}
        itemName={{
          spacing: 10, // 文本同滑轨的距离
          style: {
            fill: "black",
            lineWidth: 2,
            fontWeight: "bold", // 文本粗细
            textBaseline: "top", // 文本基准线，可取 top middle bottom，默认为middle
          },
        }}
      /> */}
        </Chart>
      </div>
    </>
  );
};
export default DemoChartAreaBizChartProfile;
