import React from "react";
import {
  Chart,
  Interval,
  Axis,
  Tooltip,
  Coordinate,
  Legend,
} from "bizcharts";
import DataSet from "@antv/data-set";
import { Col } from "antd";

const data = [
  {
    label: "Monday",
    series1: 2800,
    series2: 2260,
  },
  {
    label: "Tuesday",
    series1: 1800,
    series2: 1300,
  },
  {
    label: "Wednesday",
    series1: 950,
    series2: 900,
  },
  {
    label: "Thursday",
    series1: 500,
    series2: 390,
  },
  {
    label: "Friday",
    series1: 170,
    series2: 100,
  },
];
const ds = new DataSet();
const dv = ds.createView().source(data);
dv.transform({
  type: "fold",
  fields: ["series1", "series2"],
  // 展开字段集
  key: "type",
  // key字段
  value: "value", // value字段
});

const DemoChartGroupedBizChart = () => (
  <>
    <Col span={24}>
      <Chart height={400} width={'auto'} data={dv} autoFit>
        <Legend />
        <Coordinate transpose scale={[1, -1]} />
        <Axis
          name="label"
          label={{
            offset: 12,
          }}
        />
        <Axis name="value" position={"right"} />
        <Tooltip />
        <Interval
          position="label*value"
          color={"type"}
          adjust={[
            {
              type: "dodge",
              marginRatio: 1 / 32,
            },
          ]}
        />
      </Chart>
    </Col>
  </>
);

export default DemoChartGroupedBizChart;
