import React from "react";
import { Chart, Tooltip, Interval, Annotation, Legend, Axis } from "bizcharts";

const data = [
  { name: "Hanoi", nation: "Jan", coin: 48.9 },
  { name: "Hanoi", nation: "Feb", coin: 65.8 },
  { name: "Hanoi", nation: "Mar", coin: 39.3 },
  { name: "Hanoi", nation: "Apr", coin: 81.4 },
  { name: "Hanoi", nation: "May", coin: 47 },
  { name: "Hanoi", nation: "Jun", coin: 20.3 },
  { name: "Hanoi", nation: "Jul", coin: 24 },
  { name: "Hanoi", nation: "Aug", coin: 35.6 },
  { name: "Hanoi", nation: "Sep", coin: 65.6 },
  { name: "Hanoi", nation: "Oct", coin: 48.7 },
  { name: "Hanoi", nation: "Now", coin: 38.6 },
  { name: "Hanoi", nation: "Dec", coin: 34.6 },
  { name: "Ho Chi Minh", nation: "Jan", coin: 37.4 },
  { name: "Ho Chi Minh", nation: "Feb", coin: 48.2 },
  { name: "Ho Chi Minh", nation: "Mar", coin: 34.5 },
  { name: "Ho Chi Minh", nation: "Apr", coin: 99.7 },
  { name: "Ho Chi Minh", nation: "May", coin: 52.6 },
  { name: "Ho Chi Minh", nation: "Jun", coin: 35.5 },
  { name: "Ho Chi Minh", nation: "Jul", coin: 37.4 },
  { name: "Ho Chi Minh", nation: "Aug", coin: 42.4 },
  { name: "Ho Chi Minh", nation: "Sep", coin: 14.2 },
  { name: "Ho Chi Minh", nation: "Oct", coin: 62.6 },
  { name: "Ho Chi Minh", nation: "Now", coin: 56.1 },
  { name: "Ho Chi Minh", nation: "Dec", coin: 42.9 },
];


const axisConfig = {
  label: {
    style: {
      textAlign: "center",
    }, // 设置坐标轴文本样式
  },
  line: {
    style: {
      stroke: "#ffffff",
      fill: "#ffffff",
      lineWidth: 1,
      // lineDash: [3, 3],
    }, // 设置坐标轴线样式
  },
  grid: {
    line: {
      style: {
        stroke: "#edf5ff",
        lineWidth: 1,
        fill: "#ffffff",
        // lineDash: [3, 3],
      },
    }, // 设置坐标系栅格样式
  },
};



const DemoChartColumBizChart = () => (
  <div className="item-chart-total-data-mqn">
    <Chart appendPadding={[30, 0, 0, 0]} data={data} autoFit>
      <Interval
        adjust={[
          {
            type: "dodge",
            marginRatio: 0,
          },
        ]}
        color={["name", ["#3B36DC", "#DB5AEE"]]}
        position="nation*coin"
      />
      <Tooltip shared />

      <Axis
          name="coin"
          {...axisConfig}
          label={{ offset: 10, formatter: (val) => `${val}M` }}
        />

      <Annotation.Text
        position={["min", "max"]}
        // content="Sale report"
        offsetX={0}
        offsetY={0}
        color="#292930"
        style={{
          fontSize: 22,
          fontWeight: "bold",
          fontFamily: "Nunito",
          fill: "#292930",
        }}
      />

      {/* <Tooltip shared={true} showMarkers={true} /> */}

      <Legend
        position="top-right"
        // itemName={{ formatter: () => 'custname' }} itemValue={{ formatter: () => 323 }}
        itemName={{
          spacing: 10, // 文本同滑轨的距离
          style: {
            fill: "#6E88A9",
            fontFamily: "Nunito",
            fontSize: 16,
          },
        }}
      />

    </Chart>
  </div>
);

export default DemoChartColumBizChart;
