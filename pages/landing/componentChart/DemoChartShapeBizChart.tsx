import React from 'react';
import {
	Chart,
	Interval,
	Tooltip,
	Axis,
	Coordinate,
	getTheme,
} from "bizcharts";


const data = [
	{ item: "Việt Nam", percent: 0.4 },
	{ item: "Thái Land", percent: 0.21 },
	{ item: "Singapore", percent: 0.17 },
	{ item: "Malaisya", percent: 0.13 },
	{ item: "Pháp", percent: 0.09 },
];



const DemoChartShapeBizChart = () => (
  <>
  <Chart height={400} data={data} interactions={['element-active']} autoFit>
			<Coordinate type="theta" radius={0.75} />
			<Tooltip showTitle={false} />
			<Axis visible={false} />
			<Interval
				position="percent"
				adjust="stack"
				color={['item', ['#5b8ff9', '#5ad8a6', '#5d7092', '#f6bd16', '#dd62ef']]}
				style={{
					lineWidth: 1,
					stroke: "#fff",
				}}
				element-highlight
				label={[
					"item",
					(item) => {
						return {
							offset: -20,
							content: (data) => {
								return `${data.item}\n ${data.percent * 100}%`;
							},
						};
					},
				]}
			/>
		</Chart>
  </>
);

export default DemoChartShapeBizChart;