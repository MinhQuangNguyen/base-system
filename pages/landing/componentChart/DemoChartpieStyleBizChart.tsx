import React from "react";
import {
  Chart,
  Axis,
  Tooltip,
  Interval,
  Interaction,
  Coordinate,
  Legend,
} from "bizcharts";
import useBaseHooks from 'themes/hooks/BaseHooks';

const DemoChartpieStyleBizChart = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  const data = [
    {
      type: t("Component.Documents"),
      value: 27,
    },
    {
      type: t("Component.MediaFile"),
      value: 25,
    },
    {
      type: t("Component.OrtherFile"),
      value: 18,
    },
    {
      type: t("Component.DesignFile"),
      value: 10,
    },
    {
      type: t("Component.Unknow"),
      value: 15,
    },
    {
      type: t("Component.CodeFile"),
      value: 5,
    },
  ];

  return (
    <>
      <div className="chart-pie-style">
        <Chart data={data} height={300} autoFit>
          <Coordinate type="theta" radius={0.8} innerRadius={0.75} />
          <Axis visible={false} />
          <Tooltip showTitle={false} />
          <Interval
            adjust="stack"
            position="value"
            color={[
              "type",
              [
                "#3AA0FF",
                "#36CBCB",
                "#4ECB73",
                "#FAD337",
                "#F2637B",
                "#975FE4",
              ],
            ]}
            shape="sliceShape"
            style={{ stroke: "white", lineWidth: 3 }}
          />
          <Interaction type="element-single-selected" />
          <Legend visible={false} />
        </Chart>
        <div className="total-size-file">
          <div className="title">{t("Component.Capacity")}</div>
          <div className="total">88.28GB</div>
        </div>
      </div>
    </>
  );
};

export default DemoChartpieStyleBizChart;
