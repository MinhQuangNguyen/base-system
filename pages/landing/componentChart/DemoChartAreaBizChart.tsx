import React from "react";
import {
  Chart,
  LineAdvance,
  Legend,
  Annotation,
  Axis,
  Tooltip,
} from "bizcharts";
import useBaseHooks from "themes/hooks/BaseHooks";

const data = [
  {
    month: "Jan",
    city: "Facebook",
    temperature: 7,
  },
  {
    month: "Jan",
    city: "Google",
    temperature: 3.9,
  },
  {
    month: "Feb",
    city: "Facebook",
    temperature: 13,
  },
  {
    month: "Feb",
    city: "Google",
    temperature: 4.2,
  },
  {
    month: "Mar",
    city: "Facebook",
    temperature: 16.5,
  },
  {
    month: "Mar",
    city: "Google",
    temperature: 5.7,
  },
  {
    month: "Apr",
    city: "Facebook",
    temperature: 14.5,
  },
  {
    month: "Apr",
    city: "Google",
    temperature: 8.5,
  },
  {
    month: "May",
    city: "Facebook",
    temperature: 10,
  },
  {
    month: "May",
    city: "Google",
    temperature: 11.9,
  },
  {
    month: "Jun",
    city: "Facebook",
    temperature: 7.5,
  },
  {
    month: "Jun",
    city: "Google",
    temperature: 15.2,
  },
  {
    month: "Jul",
    city: "Facebook",
    temperature: 9.2,
  },
  {
    month: "Jul",
    city: "Google",
    temperature: 17,
  },
  {
    month: "Aug",
    city: "Facebook",
    temperature: 14.5,
  },
  {
    month: "Aug",
    city: "Google",
    temperature: 16.6,
  },
  {
    month: "Sep",
    city: "Facebook",
    temperature: 9.3,
  },
  {
    month: "Sep",
    city: "Google",
    temperature: 14.2,
  },
  {
    month: "Oct",
    city: "Facebook",
    temperature: 8.3,
  },
  {
    month: "Oct",
    city: "Google",
    temperature: 10.3,
  },
  {
    month: "Nov",
    city: "Facebook",
    temperature: 8.9,
  },
  {
    month: "Nov",
    city: "Google",
    temperature: 5.6,
  },
  {
    month: "Dec",
    city: "Facebook",
    temperature: 5.6,
  },
  {
    month: "Dec",
    city: "Google",
    temperature: 9.8,
  },
];

const axisConfig = {
  label: {
    style: {
      textAlign: "center",
    }, // 设置坐标轴文本样式
  },
  line: {
    style: {
      stroke: "#6E88A9",
      fill: "#6E88A9",
      lineWidth: 0.1,
      // lineDash: [3, 3],
    }, // 设置坐标轴线样式
  },
  grid: {
    line: {
      style: {
        stroke: "#6E88A9",
        lineWidth:  0.1,
        fill: "#6E88A9",
        // lineDash: [3, 3],
      },
    }, // 设置坐标系栅格样式
  },
};

const scale = {
  temperature: { min: 0 },
};

const DemoChartAreaBizChart = () => {
  const { t } = useBaseHooks({ lang: ["common"] });
  return (
    <>
      <div className="titleChartArea">{t("title.SaleReport")}</div>
      <div className="chartAreaMqn">
        <Chart appendPadding={[50, 0, 0, 0]} autoFit data={data} scale={scale}>
          <LineAdvance
            shape="smooth"
            point
            area
            position="month*temperature"
            color={["city", ["#0075FF", "#24d299"]]}
            size={3}
            // label="temperature"
            // size={['count', [1, 10]]}
          />

          <Axis
            name="temperature"
            // visible={false} //xóa trục tung và trục hoành
            {...axisConfig}
            label={{ offset: 10, formatter: (val) => `${val}M` }}
          />
          {/* <Axis
            name="month"
            visible={false}
            {...axisConfig}
            label={{ offset: 10, formatter: (val) => `${val}M` }}
          /> */}

          <Annotation.Text
            position={["min", "max"]}
            // content="Sale report"
            offsetX={0}
            offsetY={-25}
            color="#292930"
            style={{
              fontSize: 22,
              fontWeight: "bold",
              fontFamily: "Nunito",
              fill: "#292930",
            }}
          />
          <Tooltip shared={true} showMarkers={true} />

          <Legend
            position="top-right"
            marker={{
              symbol: "circle",
              style: {
                fill: "white",
                lineWidth: 2,
                fontWeight: "bold", // 文本粗细
              },
            }}
            itemName={{
              spacing: 5, // 文本同滑轨的距离
              style: {
                // stroke: 'blue',
                fill: "#6E88A9",
                fontFamily: "Nunito",
                fontSize: 16,
              },
            }}
          />
          {/* <Legend 
        position="top-left"
        // itemName={{ formatter: () => 'custname' }} itemValue={{ formatter: () => 323 }}
        itemName={{
          spacing: 10, // 文本同滑轨的距离
          style: {
            fill: "black",
            lineWidth: 2,
            fontWeight: "bold", // 文本粗细
            textBaseline: "top", // 文本基准线，可取 top middle bottom，默认为middle
          },
        }}
      /> */}
        </Chart>
      </div>
    </>
  );
};

export default DemoChartAreaBizChart;
