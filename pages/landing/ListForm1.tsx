import Layout from "themes/layouts/Home";
import DemoForm1 from "./componentForm/DemoForm1";


const ListForm = () => {
  return (
    <>
      <Layout>
        <div className="site-layout-background">
          <DemoForm1/>
        </div>
      </Layout>
    </>
  );
};

export default ListForm;
