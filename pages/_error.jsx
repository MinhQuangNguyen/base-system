import React from 'react'
import { withTranslation } from 'themes/modules/I18n'
import Layout from 'themes/layouts/Home'

const Error = ({ statusCode, t }) => {
  if (statusCode == 404) {
    return (
      <Layout>
        <div className="error-page">
          <div className="content-error">
            <img src="/static/img/404.png" alt="" />
            <div className="title-error">Không thể tìm thấy trang bạn đang tìm kiếm.</div>
            <div className="decs-error">Trang không tồn tại hoặc một số lỗi khác đã xảy ra. </div>
          </div>
        </div>
      </Layout>
    )
  }
}

Error.getInitialProps = async ({ res, err }) => {
  let statusCode = null
  if (res) {
    ({ statusCode } = res)
  } else if (err) {
    ({ statusCode } = err)
  }
  return {
    namespacesRequired: ['common'],
    statusCode,
  }
}

Error.defaultProps = {
  statusCode: null,
}


export default withTranslation('common')(Error)